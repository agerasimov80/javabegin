package goldman.gameobjects;

public class UserScore {

    User user;
    int score;
    long play_date;
    int play_count;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getPlay_date() {
        return play_date;
    }

    public void setPlay_date(long play_date) {
        this.play_date = play_date;
    }

    public int getPlay_count() {
        return play_count;
    }

    public void setPlay_count(int play_count) {
        this.play_count = play_count;
    }

}
