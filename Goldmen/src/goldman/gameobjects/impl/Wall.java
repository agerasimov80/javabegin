package goldman.gameobjects.impl;

import goldman.enums.E_GameObjectType;
import goldman.gameobjects.abstracts.AbstractGameObject;
import java.net.URL;
import javax.swing.ImageIcon;

public class Wall extends AbstractGameObject {

    private final String iconPath = "/goldman/images/wall.png";
    URL urlIcon = this.getClass().getResource(iconPath);

    public Wall(Coordinate coordinate) {
        super();
        super.setCoordinate(coordinate);
        super.setIcon(new ImageIcon(urlIcon));
        super.setType(E_GameObjectType.WALL);
    }

}
