
package goldman.gameobjects.impl;

import goldman.enums.E_GameObjectType;
import goldman.gameobjects.abstracts.AbstractGameObject;

public class Nothing extends AbstractGameObject {
    
  
   
    public Nothing(Coordinate coordinate) {
        super ();
        super.setCoordinate( coordinate);
        super.setIcon(null);
        super.setType(E_GameObjectType.NOTHING);
    }
}
