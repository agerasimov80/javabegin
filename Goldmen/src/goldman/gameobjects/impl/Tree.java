
package goldman.gameobjects.impl;

import goldman.enums.E_GameObjectType;
import goldman.gameobjects.abstracts.AbstractGameObject;
import javax.swing.ImageIcon;


public class Tree extends AbstractGameObject{
    
    private final String iconPath = "/goldman/images/tree.jpg";
    ImageIcon icon = new ImageIcon(this.getClass().getResource(iconPath));
    
    public Tree(Coordinate coordinate) {
        super ();
        super.setIcon(icon);
        super.setType(E_GameObjectType.TREE);
        super.setCoordinate(coordinate);
        
    }
    
    
}
