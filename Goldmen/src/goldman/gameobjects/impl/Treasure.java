
package goldman.gameobjects.impl;

import goldman.enums.E_GameObjectType;
import goldman.gameobjects.abstracts.AbstractGameObject;
import java.net.URL;

import javax.swing.ImageIcon;

public class Treasure extends AbstractGameObject {
    
    private final int cost = 5;

    private final String iconPath = "/goldman/images/gold.png" ; 
    URL urlIcon = this.getClass().getResource(iconPath);
    
    public Treasure(Coordinate coordinate) {
        super ();
        super.setCoordinate(coordinate);
        super.setIcon(new ImageIcon(urlIcon));
        super.setType(E_GameObjectType.TREASURE);
    }
    
    
    
    public int getCost() {
        return cost;
    }
}
