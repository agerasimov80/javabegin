package goldman.gameobjects.impl;

import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_ActionResult;
import javax.swing.ImageIcon;

public class Monster extends AbstractGameObjectMovable {

    
    
    public Monster(Coordinate coordinate) {
        super();
        super.setCoordinate(coordinate);
        super.setType(E_GameObjectType.MONSTER);
        super.setIcon(new ImageIcon(this.getClass().getResource("/goldman/images/monster_up.jpg")));

        super.icons.put(E_Direction.Up, new ImageIcon(this.getClass().getResource("/goldman/images/monster_up.jpg")));
        super.icons.put(E_Direction.Down, new ImageIcon(this.getClass().getResource("/goldman/images/monster_down.jpg")));
        super.icons.put(E_Direction.Left, new ImageIcon(this.getClass().getResource("/goldman/images/monster_left.jpg")));
        super.icons.put(E_Direction.Right, new ImageIcon(this.getClass().getResource("/goldman/images/monster_right.jpg")));

    }

    @Override
    public E_ActionResult move(E_Direction direction, AbstractGameObject objectInNewCoordinate) {

        switch (objectInNewCoordinate.getType()) {

            case EXIT:
                return E_ActionResult.MOVE;
            case MONSTER:
            case TREASURE:
            case TREE:
                return E_ActionResult.NOACTION;

            case GOLDMAN:
                return E_ActionResult.DIE;

            default:
                return super.move(direction, objectInNewCoordinate);
        }
    }
}
