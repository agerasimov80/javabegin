
package goldman.gameobjects.impl;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_GameObjectType;
import java.net.URL;
import javax.swing.ImageIcon;


public class Exit extends AbstractGameObject{
    
    private final int cost = 5;
    private final String iconPath = "/goldman/images/exit.png";
    URL url = this.getClass().getResource(iconPath);
    
    public Exit(Coordinate coordinate) {
        super ();
        super.setCoordinate(coordinate);
        super.setIcon(new ImageIcon(url));
        super.setType(E_GameObjectType.EXIT);
    }
    
}
