package goldman.gameobjects.impl;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.enums.E_ActionResult;
import goldman.gameobjects.abstracts.AbstractSoundObject;
import javax.swing.ImageIcon;

public class Goldman extends AbstractSoundObject {


    private final ImageIcon iconUpImg = new ImageIcon(this.getClass().getResource("/goldman/images/goldman_up.png"));
    private final ImageIcon iconDownImg = new ImageIcon(this.getClass().getResource("/goldman/images/goldman_down.png"));
    private final ImageIcon iconLeftImg = new ImageIcon(this.getClass().getResource("/goldman/images/goldman_left.png"));
    private final ImageIcon iconRightImg = new ImageIcon(this.getClass().getResource("/goldman/images/goldman_right.png"));

    private int score = 0;

    public Goldman(Coordinate coordinate) {
        super();
        super.setCoordinate(coordinate);
        super.setType(E_GameObjectType.GOLDMAN);
        super.setIcon(iconUpImg);

        super.icons.put(E_Direction.Up, iconUpImg);
        super.icons.put(E_Direction.Down, iconDownImg);
        super.icons.put(E_Direction.Left, iconLeftImg);
        super.icons.put(E_Direction.Right, iconRightImg);
    }

    public void addScore(int score) {
        this.score += score;
    }
    

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTurnsCount() {
        return turnsCount;
    }

    public void setTurnsCount(int turnsCount) {
        this.turnsCount = turnsCount;
    }

    @Override
    public E_ActionResult move(E_Direction direction, AbstractGameObject objectInNewCoordinate) {

        
        // край карты
        if (objectInNewCoordinate == null) {
            return super.move(direction, objectInNewCoordinate);
        }
        
       

        switch (objectInNewCoordinate.getType()) {
            case TREASURE:
                turnsCount++;
                super.beforeMove(direction);
                this.addScore(((Treasure) objectInNewCoordinate).getCost());
                return E_ActionResult.CONSUME;

            case MONSTER:
                super.beforeMove(direction);
                return E_ActionResult.DIE;

            case EXIT:
                super.beforeMove(direction);
                return E_ActionResult.WIN;

            case TREE:
               turnsCount++;
                super.beforeMove(direction);
                return E_ActionResult.HIDE_IN_TREE;
        }

        return super.move(direction, objectInNewCoordinate);
    }

}
