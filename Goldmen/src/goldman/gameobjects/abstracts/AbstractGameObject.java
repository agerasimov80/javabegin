package goldman.gameobjects.abstracts;

import goldman.enums.E_GameObjectType;
import goldman.gameobjects.impl.Coordinate;
import goldman.gameobjects.interfaces.I_StaticObject;
import java.io.Serializable;
import java.util.Objects;
import javax.swing.ImageIcon;

public abstract class AbstractGameObject implements I_StaticObject, Serializable {

    // координата
    private Coordinate coordinate;

    // пиктограмма по умолчанию
    private ImageIcon icon = getImageIcon("C:\\Users\\gerasal3\\Projects\\javabegin\\Goldmen\\src\\goldman\\images\\noicon.png");
    
    // тип объекта
    private E_GameObjectType type;
    

    @Override
    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public ImageIcon getIcon () {
        return icon;
    }
    
    public ImageIcon getImageIcon(String path) {
        return new ImageIcon(path);
    }

    @Override
    public E_GameObjectType getType() {
        return type;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public void setType(E_GameObjectType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.coordinate);
        hash = 89 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractGameObject other = (AbstractGameObject) obj;
        if(this.type != other.type)
            return false;
        if (!this.coordinate.equals(other.coordinate))
            return false;
        return true;
    }
    
    
    
    
}
