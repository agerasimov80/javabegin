package goldman.gameobjects.abstracts;

import goldman.enums.E_ActionResult;
import goldman.sounds.impl.WavPlayer;
import goldman.sounds.interfaces.I_SoundObject;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public abstract class AbstractSoundObject extends AbstractGameObjectMovable implements I_SoundObject, Serializable {

    private transient Clip dieClip;
    private transient Clip consumeClip;
    private transient  Clip winClip;

    Clip openClip(String soundName) {

        Clip clip = null;
        try {
            clip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(this.getClass().getResource(WavPlayer.SOUND_PATH + soundName));
            clip.open(ais);
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
            Logger.getLogger(AbstractSoundObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clip;
    }

    @Override
    public Clip getSoundClip(E_ActionResult result) {
        if (dieClip == null) {
            dieClip = openClip(WavPlayer.WAV_DIE);
        }
        if (consumeClip == null) {
            consumeClip = openClip(WavPlayer.WAV_CONSUME);
        }
        if (winClip == null) {
            winClip = openClip(WavPlayer.WAV_WIN);
        }
        switch (result) {
            case CONSUME:
                return consumeClip;
            case DIE:
                return dieClip;
            case WIN:
                return winClip;
        }
        return null;
    }

}
