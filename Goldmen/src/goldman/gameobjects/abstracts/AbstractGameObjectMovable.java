package goldman.gameobjects.abstracts;

import goldman.enums.E_ActionResult;
import goldman.enums.E_Direction;
import goldman.gameobjects.interfaces.I_MovableObject;
import java.io.Serializable;
import java.util.EnumMap;
import javax.swing.ImageIcon;

public abstract class AbstractGameObjectMovable extends AbstractGameObject implements I_MovableObject, Serializable {

    // по умолчанию = 1
    private int steps = 1;
    protected int turnsCount;

    protected EnumMap<E_Direction, ImageIcon> icons = new EnumMap(E_Direction.class);

    
    public void setSteps(int steps) {
        this.steps = steps;
    }

    @Override
    public int getSteps() {
        return steps;
    }

    protected void beforeMove(E_Direction direction) {
        setIcon(icons.get(direction));
    }

    @Override
    public E_ActionResult move(E_Direction direction, AbstractGameObject objectInNewCoordinate) {

        if (objectInNewCoordinate == null) {        // край карты.
            return E_ActionResult.NOACTION;
        }

        // действия для всех движущихся объектов
        switch (objectInNewCoordinate.getType()) {
            case WALL:
                this.beforeMove(direction);
                return E_ActionResult.NOACTION;

            case NOTHING:
                turnsCount++;
                this.beforeMove(direction);
                return E_ActionResult.MOVE;

            default:
                return E_ActionResult.NOACTION;
        }
    }

}
