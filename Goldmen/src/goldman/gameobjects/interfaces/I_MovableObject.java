package goldman.gameobjects.interfaces;

import goldman.enums.E_Direction;
import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_ActionResult;

public interface I_MovableObject extends I_StaticObject {

    E_ActionResult move(E_Direction direction,AbstractGameObject objectInNewCoordinate);
    
    int getSteps();
    
}
