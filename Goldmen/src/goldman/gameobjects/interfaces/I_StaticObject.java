
package goldman.gameobjects.interfaces;

import goldman.gameobjects.impl.Coordinate;

import javax.swing.ImageIcon;
import goldman.enums.E_GameObjectType;

public interface I_StaticObject {
   
    E_GameObjectType getType();
    Coordinate getCoordinate ();
    ImageIcon getIcon ();
}


