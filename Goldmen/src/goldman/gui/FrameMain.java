package goldman.gui;

import goldman.collections.impl.MapGameCollection;
import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.facade.GameFacade;
import goldman.gamemap.impl.JTableGameMap;
import goldman.gamemap.loader.adapter.MapLoaderAdapter;
import goldman.scores.impl.DbScoreSaver;
import goldman.scores.interfaces.I_ScoreManager;
import goldman.gameobjects.User;
import goldman.sounds.impl.WavPlayer;
import goldman.sounds.interfaces.I_SoundPlayer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;

public class FrameMain extends javax.swing.JFrame {

    private User user;
    private JDialog splashDialog;
    private FrameGame frameGame;
    private FrameStats frameStats;
    private FrameSavedGames frameSavedGames;

    private CustomDialog usernameDialog = new CustomDialog(this, "Имя пользователя", "Введите имя:", true);

    // если карту загружать из файла.
    //private static final String MAP_FS = "/goldman/game.map";
    //private File file = new File(this.getClass().getResource(MAP_FS).getFile());
    private AbstractGameMap map = new JTableGameMap(new MapGameCollection());
    private MapLoaderAdapter mapLoaderAdapter = new MapLoaderAdapter(map);
    private I_SoundPlayer soundPlayer = new WavPlayer();
    private I_ScoreManager scoreSaver = new DbScoreSaver();
    SwingWorker s = null;

    GameFacade gameFacade = new GameFacade(mapLoaderAdapter, soundPlayer, scoreSaver);

    private static final int MAP_LEVEL_ONE = 1;

    public FrameMain() {

        initComponents();

        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(FrameMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        SwingUtilities.updateComponentTreeUI(this);

        super.setLayout(new BorderLayout());
        super.setResizable(false);
        super.setLocationRelativeTo(null);
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void createFrameGame() {

        if (frameGame == null) {
            frameGame = new FrameGame(gameFacade);
        }
    }

    private void createFrameStats() {
        if (frameStats == null) {
            frameStats = new FrameStats();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNewGame = new javax.swing.JButton();
        btnStats = new javax.swing.JButton();
        btnSavedGames = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(300, 350));
        setMinimumSize(new java.awt.Dimension(300, 350));
        setPreferredSize(new java.awt.Dimension(300, 350));
        setResizable(false);
        getContentPane().setLayout(new java.awt.FlowLayout());

        btnNewGame.setFont(new java.awt.Font("Euphemia", 0, 14)); // NOI18N
        btnNewGame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/new_game.png"))); // NOI18N
        btnNewGame.setText("New Game");
        btnNewGame.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnNewGame.setPreferredSize(new java.awt.Dimension(250, 75));
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });
        getContentPane().add(btnNewGame);

        btnStats.setFont(new java.awt.Font("Euphemia", 0, 14)); // NOI18N
        btnStats.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/oie_22142451WLRhcTLw.png"))); // NOI18N
        btnStats.setText("Statistics");
        btnStats.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnStats.setPreferredSize(new java.awt.Dimension(250, 75));
        btnStats.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStatsActionPerformed(evt);
            }
        });
        getContentPane().add(btnStats);

        btnSavedGames.setFont(new java.awt.Font("Euphemia", 0, 14)); // NOI18N
        btnSavedGames.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/floppy.png"))); // NOI18N
        btnSavedGames.setText("Saved Games");
        btnSavedGames.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnSavedGames.setPreferredSize(new java.awt.Dimension(250, 75));
        btnSavedGames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSavedGamesActionPerformed(evt);
            }
        });
        getContentPane().add(btnSavedGames);

        btnExit.setFont(new java.awt.Font("Euphemia", 0, 14)); // NOI18N
        btnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/exit.gif"))); // NOI18N
        btnExit.setText("Exit");
        btnExit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnExit.setPreferredSize(new java.awt.Dimension(250, 75));
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        getContentPane().add(btnExit);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed

        MapInfo mapInfo = new MapInfo();
        mapInfo.setLevelId(MAP_LEVEL_ONE);
        map.setMapInfo(mapInfo);

        if (!saveUser()) {
            return;
        }
        mapInfo.setUser(user);

        s = new SwingWorker() {

            @Override
            protected Object doInBackground() throws Exception {

                showSplash();
                if (!mapLoaderAdapter.loadMap(mapInfo, E_SourceType.Database)) {
                    throw new Exception("Map is not loaded.");
                }
                Thread.sleep(3000);

                return null;
            }

            @Override
            protected void done() {
                hideSplash();
                createFrameGame();
                frameGame.showFrame(FrameMain.this);
            }

            @Override
            protected void process(List list) {

            }
        };

        s.execute();


    }//GEN-LAST:event_btnNewGameActionPerformed


    private void btnStatsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStatsActionPerformed

        createFrameStats();

        frameStats.setList(scoreSaver.getScoreTable());
        frameStats.showFrame(this);
    }//GEN-LAST:event_btnStatsActionPerformed


    private void btnSavedGamesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSavedGamesActionPerformed

        MapInfo mapInfo = new MapInfo();
        map.setMapInfo(mapInfo);

        if (!saveUser()) {
            return;
        }
        mapInfo.setUser(user);

        createFrameGame();

        if (frameSavedGames == null) {
            frameSavedGames = new FrameSavedGames(mapLoaderAdapter, frameGame);
        }

        frameSavedGames.showFrame(this);
    }//GEN-LAST:event_btnSavedGamesActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnExitActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnSavedGames;
    private javax.swing.JButton btnStats;
    // End of variables declaration//GEN-END:variables

    private void showSplash() {

        if (splashDialog == null) {
            splashDialog = new JDialog(this);

            splashDialog.setUndecorated(true);
            splashDialog.setSize(200, 100);
            splashDialog.setLocationRelativeTo(this);
            splashDialog.setModal(false);
            splashDialog.getParent().setEnabled(false);

            JPanel panel = new JPanel();
            panel.setBorder(new LineBorder(Color.RED));

            JLabel label = new JLabel("Loading, please wait...");
            panel.add(label);
            splashDialog.add(panel);
        }

        splashDialog.getParent().setEnabled(false);
        splashDialog.setVisible(true);

    }

    private void hideSplash() {

        splashDialog.setVisible(false);

        splashDialog.getParent().setEnabled(true);
    }

    private String getUserNameDialog() {

        if (user != null && user.getName() != null) {
            usernameDialog.setName(user.getName());
        }

        usernameDialog.setVisible(true);

        return usernameDialog.getValidatedText();
    }

    private boolean saveUser() {// сохранить пользователя, получить его id

        // запрашиваем имя в диалоге
        String username = getUserNameDialog();

        // если ввели имя, тогда...
        if (username != null && !username.trim().equals("")) {

            if (user != null && user.getName().equals(username)) {// если ввел того же пользователя (т.е. ничего не менял)
                return true;
            }

            // создаем пользователя 
            user = new User();
            user.setName(username);
            user.setId(mapLoaderAdapter.getPlayerId(username, E_SourceType.Database));

            // .. и записываем в MapInfo
            map.getMapInfo().setUser(user);

            return true;
        }

        return false;
    }

}
