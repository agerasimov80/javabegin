package goldman.gui;

import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.enums.E_ActionResult;
import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.facade.GameFacade;
import goldman.listeners.interfaces.I_MoveResultListener;
import goldman.utils.E_MessageType;
import static goldman.utils.E_MessageType.MESSAGE_SAVED_SUCCESS;
import goldman.utils.MessageManager;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class FrameGame extends ConfirmCloseFrame implements I_MoveResultListener, Serializable {

    GameFacade gameFacade;
    private AbstractGameMap gameMap;

    private static final int FRAME_HEIGHT = 350;
    private static final int FRAME_WIDTH = 450;

    public FrameGame(GameFacade gameFacade) {

        this.gameFacade = gameFacade;

        initComponents();

        btnUp.setMnemonic(KeyEvent.VK_UP);
        btnDown.setMnemonic(KeyEvent.VK_DOWN);
        btnRight.setMnemonic(KeyEvent.VK_RIGHT);
        btnLeft.setMnemonic(KeyEvent.VK_LEFT);

        super.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        super.setLocationRelativeTo(null);

    }

    @Override
    protected boolean confirmCloseFrame() {

        gameFacade.stopGame();
        int result = MessageManager.showYesNoCancelMessage(this, E_MessageType.CONFIRM_SAVE_GAME);
        switch (result) {
            case JOptionPane.CANCEL_OPTION:
                gameFacade.startGame();
                return false;
            case JOptionPane.NO_OPTION:
                closeFrame();
                break;
            case JOptionPane.YES_OPTION:
                saveMap();
                break;
        }
        return true;
    }

    private void saveMap() {

        gameFacade.saveMap();
        MessageManager.showInformMessage(this, MESSAGE_SAVED_SUCCESS);
    }

//    public AbstractGameMap getMap() {
//        return gameMap;
//    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpanelArrows = new javax.swing.JPanel();
        btnRight = new javax.swing.JButton();
        btnUp = new javax.swing.JButton();
        btnLeft = new javax.swing.JButton();
        btnDown = new javax.swing.JButton();
        jpanelGameField = new javax.swing.JPanel();
        jpanelSaveExitButtons = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jpanelScores = new javax.swing.JPanel();
        jlabelScore = new javax.swing.JLabel();
        jlabelTurnsLeft = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuItmSave = new javax.swing.JMenuItem();
        menuItmExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        menuItemBackGroundMusic = new javax.swing.JCheckBoxMenuItem();
        menuItmStats = new javax.swing.JMenuItem();

        setMinimumSize(new java.awt.Dimension(470, 370));
        setResizable(false);

        jpanelArrows.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jpanelArrows.setPreferredSize(new java.awt.Dimension(130, 150));

        btnRight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/right.png"))); // NOI18N
        btnRight.setMaximumSize(new java.awt.Dimension(45, 45));
        btnRight.setMinimumSize(new java.awt.Dimension(45, 45));
        btnRight.setPreferredSize(new java.awt.Dimension(45, 45));
        btnRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRightActionPerformed(evt);
            }
        });

        btnUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/up.png"))); // NOI18N
        btnUp.setMaximumSize(new java.awt.Dimension(45, 45));
        btnUp.setMinimumSize(new java.awt.Dimension(45, 45));
        btnUp.setPreferredSize(new java.awt.Dimension(45, 45));
        btnUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpActionPerformed(evt);
            }
        });
        btnUp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                handler(evt);
            }
        });

        btnLeft.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/left.png"))); // NOI18N
        btnLeft.setName("btnLeft"); // NOI18N
        btnLeft.setPreferredSize(new java.awt.Dimension(45, 45));
        btnLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLeftActionPerformed(evt);
            }
        });

        btnDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/down.png"))); // NOI18N
        btnDown.setPreferredSize(new java.awt.Dimension(45, 45));
        btnDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpanelArrowsLayout = new javax.swing.GroupLayout(jpanelArrows);
        jpanelArrows.setLayout(jpanelArrowsLayout);
        jpanelArrowsLayout.setHorizontalGroup(
            jpanelArrowsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelArrowsLayout.createSequentialGroup()
                .addComponent(btnLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelArrowsLayout.createSequentialGroup()
                .addContainerGap(42, Short.MAX_VALUE)
                .addGroup(jpanelArrowsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44))
        );
        jpanelArrowsLayout.setVerticalGroup(
            jpanelArrowsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelArrowsLayout.createSequentialGroup()
                .addComponent(btnUp, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jpanelArrowsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jpanelGameField.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jpanelGameField.setLayout(new java.awt.BorderLayout());

        jpanelSaveExitButtons.setLayout(new java.awt.BorderLayout());

        btnExit.setText("Exit game");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        jpanelSaveExitButtons.add(btnExit, java.awt.BorderLayout.SOUTH);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jpanelSaveExitButtons.add(btnSave, java.awt.BorderLayout.PAGE_START);

        jlabelScore.setText("Кол-во очков: 0");

        jlabelTurnsLeft.setText("Кол-во ходов: 0");

        javax.swing.GroupLayout jpanelScoresLayout = new javax.swing.GroupLayout(jpanelScores);
        jpanelScores.setLayout(jpanelScoresLayout);
        jpanelScoresLayout.setHorizontalGroup(
            jpanelScoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlabelTurnsLeft, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jlabelScore, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpanelScoresLayout.setVerticalGroup(
            jpanelScoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanelScoresLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlabelScore)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jlabelTurnsLeft)
                .addContainerGap())
        );

        jMenu1.setText("Game");

        menuItmSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        menuItmSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/save.png"))); // NOI18N
        menuItmSave.setText("Save");
        menuItmSave.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
                menuItmSaveMenuKeyPressed(evt);
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
            }
        });
        menuItmSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItmSaveActionPerformed(evt);
            }
        });
        jMenu1.add(menuItmSave);

        menuItmExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        menuItmExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/goldman/images/exit.png"))); // NOI18N
        menuItmExit.setText("Exit");
        menuItmExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItmExitActionPerformed(evt);
            }
        });
        jMenu1.add(menuItmExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Service");

        menuItemBackGroundMusic.setSelected(true);
        menuItemBackGroundMusic.setText("BackgroundMusic");
        menuItemBackGroundMusic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemBackGroundMusicActionPerformed(evt);
            }
        });
        jMenu2.add(menuItemBackGroundMusic);

        menuItmStats.setText("Statistics");
        menuItmStats.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItmStatsActionPerformed(evt);
            }
        });
        jMenu2.add(menuItmStats);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpanelGameField, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jpanelScores, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpanelArrows, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(jpanelSaveExitButtons, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jpanelArrows, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jpanelScores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jpanelSaveExitButtons, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jpanelGameField, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpanelArrows.getAccessibleContext().setAccessibleName("jPanel3");
        jpanelGameField.getAccessibleContext().setAccessibleName("jPanel1");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRightActionPerformed
        moveObject(E_GameObjectType.GOLDMAN, E_Direction.Right);
    }//GEN-LAST:event_btnRightActionPerformed

    private void btnUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpActionPerformed
        moveObject(E_GameObjectType.GOLDMAN, E_Direction.Up);
    }//GEN-LAST:event_btnUpActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        saveMap();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownActionPerformed
        moveObject(E_GameObjectType.GOLDMAN, E_Direction.Down);
    }//GEN-LAST:event_btnDownActionPerformed

    private void btnLeftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLeftActionPerformed
        moveObject(E_GameObjectType.GOLDMAN, E_Direction.Left);
    }//GEN-LAST:event_btnLeftActionPerformed

    private void handler(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_handler


    }//GEN-LAST:event_handler

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        closeFrame();
    }//GEN-LAST:event_btnExitActionPerformed

    private void menuItmSaveMenuKeyPressed(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_menuItmSaveMenuKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuItmSaveMenuKeyPressed

    private void menuItmSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItmSaveActionPerformed
        saveMap();
    }//GEN-LAST:event_menuItmSaveActionPerformed

    private void menuItmExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItmExitActionPerformed
        closeFrame();
    }//GEN-LAST:event_menuItmExitActionPerformed

    private void menuItmStatsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItmStatsActionPerformed
        closeFrame();

    }//GEN-LAST:event_menuItmStatsActionPerformed

    private void menuItemBackGroundMusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemBackGroundMusicActionPerformed
        if (menuItemBackGroundMusic.getState())
            gameFacade.startBackgroundMusic();
        else {
            gameFacade.stopBackgroundMusic();
        }
        
    }//GEN-LAST:event_menuItemBackGroundMusicActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDown;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLeft;
    private javax.swing.JButton btnRight;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUp;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel jlabelScore;
    private javax.swing.JLabel jlabelTurnsLeft;
    private javax.swing.JPanel jpanelArrows;
    private javax.swing.JPanel jpanelGameField;
    private javax.swing.JPanel jpanelSaveExitButtons;
    private javax.swing.JPanel jpanelScores;
    private javax.swing.JCheckBoxMenuItem menuItemBackGroundMusic;
    private javax.swing.JMenuItem menuItmExit;
    private javax.swing.JMenuItem menuItmSave;
    private javax.swing.JMenuItem menuItmStats;
    // End of variables declaration//GEN-END:variables

    private void moveObject(E_GameObjectType type, E_Direction movingDirection) {
        gameFacade.moveObject(null, movingDirection, type);
    }

    @Override
    public void moveEventHandler(E_ActionResult actionResult, AbstractGameObjectMovable abstractGameObjectMovable) {

        checkGoldmanActions(abstractGameObjectMovable, actionResult);

        switch (actionResult) {
            case DIE:
                closeFrame(E_MessageType.LOOSE);
                break;
            case WIN:
                gameFacade.saveScore();
                closeFrame(E_MessageType.WIN);
                break;

        }

        gameFacade.drawMap();
    }

    private void checkGoldmanActions(AbstractGameObjectMovable abstractGameObjectMovable, E_ActionResult actionResult) {

        if (abstractGameObjectMovable.getType().equals(E_GameObjectType.GOLDMAN)) {
            switch (actionResult) {

                case CONSUME:
                    setLabelTextScore();
                    setLabelTextTurns();
                    break;

                case MOVE:
                case HIDE_IN_TREE:
                    if (gameFacade.getTurnsLeftCount() <= 0) {
                        closeFrame(E_MessageType.LOOSE);
                    }
                    setLabelTextTurns();
                    break;
            }
        }
    }

    private void initMap() {

        jpanelGameField.removeAll();
        jpanelGameField.add(gameFacade.getMapComponent(), java.awt.BorderLayout.CENTER);

        gameFacade.addMoveListener(this);

        setLabelTextTurns();
        setLabelTextScore();

        gameFacade.startGame();
    }

    @Override
    public void showFrame(JFrame parent) {
        initMap();
        super.showFrame(parent);
    }

    @Override
    public void closeFrame() {

        gameFacade.stopGame();
        super.closeFrame();
    }

    public void closeFrame(E_MessageType messageType) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                MessageManager.showInformMessage(null, messageType);
            }
        });

        closeFrame();
    }

    void setLabelTextTurns() {
        jlabelTurnsLeft.setText("Кол-во ходов: " + String.valueOf(gameFacade.getTurnsLeftCount()));
    }

    void setLabelTextScore() {
        jlabelScore.setText("Кол-во очков: " + String.valueOf(gameFacade.getTotalScore()));
    }
}
