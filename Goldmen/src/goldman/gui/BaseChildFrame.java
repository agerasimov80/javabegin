package goldman.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class BaseChildFrame extends JFrame {

    JFrame parent = null;

    public BaseChildFrame() {
        super();
        setCloseOperation();
    }

    public JFrame getParentFrame() {
        return parent;
    }

    protected void setCloseOperation() {
        super.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        super.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                closeFrame();
            }
        });
    }

    public void showFrame(JFrame parent) {

        this.parent = parent;
        parent.setVisible(false);
        super.setVisible(true);
    }

    public void closeFrame() {
        super.setVisible(false);
        parent.setVisible(true);
    }
    
    

}
