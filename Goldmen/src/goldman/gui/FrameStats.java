package goldman.gui;

import goldman.models.impl.ScoreTableModel;
import goldman.gameobjects.UserScore;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;


public class FrameStats extends BaseChildFrame {

    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_WIDTH = 550;

    ArrayList<UserScore> list;
    JTable jTableScore = new JTable();
    JScrollPane scrollPane = new JScrollPane();
    

    public FrameStats() {

        initComponents();
        super.setLayout(new BorderLayout());
        scrollPane.setViewportView(jTableScore);
        jTableScore.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        super.add(scrollPane, BorderLayout.CENTER);
        
        super.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        super.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        super.setLocationRelativeTo(null);
    }

    public void setList(ArrayList<UserScore> list) {
        this.list = list;
    }

    @Override
    public void showFrame(JFrame parent) {

        jTableScore.setModel(new ScoreTableModel(list));
        super.showFrame(parent); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jScoreTable = new javax.swing.JTable();

        jScoreTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jScoreTable);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 435, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 303, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable jScoreTable;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
