package goldman.gui;

public enum FrameType {

    NewGame,
    SavedGames,
    Statistics

}
