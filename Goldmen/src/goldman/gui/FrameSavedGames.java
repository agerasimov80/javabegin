package goldman.gui;

import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.loader.abstracts.AbstractMapLoader;
import goldman.gamemap.loader.adapter.MapLoaderAdapter;
import goldman.gameobjects.SavedMapInfo;
import goldman.models.impl.SavedGamesTableModel;
import goldman.utils.E_MessageType;
import goldman.utils.MessageManager;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

public class FrameSavedGames extends BaseChildFrame {

    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_WIDTH = 500;

    private MapLoaderAdapter mapLoaderAdapter;
    private SavedGamesTableModel model;
    private FrameGame frameGame;
    ArrayList<SavedMapInfo> list;
    JTable jTableMaps = new JTable();


    public FrameSavedGames(MapLoaderAdapter mapLoaderAdapter, FrameGame frameGame) {
        initComponents();

        //jScrollPaneSaves.setLayout(new BorderLayout());
        jScrollPaneSaves.setViewportView(jTableMaps);
        jTableMaps.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        //super.add(scrollPane, BorderLayout.CENTER);

        super.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        super.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        super.setLocationRelativeTo(null);

        this.mapLoaderAdapter = mapLoaderAdapter;
        this.frameGame = frameGame;
    }

    public void setList(ArrayList<SavedMapInfo> list) {
        this.list = list;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPaneSaves = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        btnLoadGame = new javax.swing.JButton();
        btnDeleteGame = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPaneSaves.setPreferredSize(new java.awt.Dimension(2, 20));
        getContentPane().add(jScrollPaneSaves, java.awt.BorderLayout.CENTER);

        btnLoadGame.setText("Load Game");
        btnLoadGame.setMaximumSize(new java.awt.Dimension(150, 23));
        btnLoadGame.setMinimumSize(new java.awt.Dimension(100, 23));
        btnLoadGame.setPreferredSize(new java.awt.Dimension(100, 23));
        btnLoadGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadGameActionPerformed(evt);
            }
        });

        btnDeleteGame.setText("Delete Game");
        btnDeleteGame.setMaximumSize(new java.awt.Dimension(150, 23));
        btnDeleteGame.setMinimumSize(new java.awt.Dimension(100, 23));
        btnDeleteGame.setPreferredSize(new java.awt.Dimension(100, 23));
        btnDeleteGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteGameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(216, Short.MAX_VALUE)
                .addComponent(btnLoadGame, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeleteGame, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDeleteGame, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLoadGame, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoadGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadGameActionPerformed
   int index = jTableMaps.getSelectedRow();

        if (index < 0) {
            return;
        }

        MapInfo mapInfo = model.getMapInfo(index);

        mapLoaderAdapter.loadMap(mapInfo, E_SourceType.Database);

        closeFrame();

        frameGame.showFrame(getParentFrame());
    }//GEN-LAST:event_btnLoadGameActionPerformed

    private void btnDeleteGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteGameActionPerformed
             int index = jTableMaps.getSelectedRow();

        if (index < 0) {
            return;
        }

        int result = MessageManager.showYesNoCancelMessage(this, E_MessageType.CONFIRM_DELETION);
        switch (result) {
            case JOptionPane.YES_OPTION: {



                MapInfo mapInfo = model.getMapInfo(index);

                mapLoaderAdapter.deleteSavedMap(mapInfo,  E_SourceType.Database);

                model.deleteMapInfo(index);
                model.refresh();


                break;
            }
            case JOptionPane.NO_OPTION:
            case JOptionPane.CANCEL_OPTION:

        }
    }//GEN-LAST:event_btnDeleteGameActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteGame;
    private javax.swing.JButton btnLoadGame;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPaneSaves;
    // End of variables declaration//GEN-END:variables


    @Override
    public void showFrame(JFrame parent) {

        list = mapLoaderAdapter.getSavedMapList(mapLoaderAdapter.getGameMap().getMapInfo().getUser(),  E_SourceType.Database);

        model = new SavedGamesTableModel(list);

        jTableMaps.setModel(model);

        jTableMaps.setRowHeight(35);

        super.showFrame(parent);

        if (list.isEmpty()) {
            MessageManager.showInformMessage(this, E_MessageType.ERROR_NO_SAVED_GAMES);
        }

    }
}
