package goldman.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class ConfirmCloseFrame extends BaseChildFrame {

    
    protected abstract boolean  confirmCloseFrame ();

    
    @Override
    protected void setCloseOperation() {
       
        super.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we) {
               if (confirmCloseFrame())
                  closeFrame();
            }
            
        });
        
    }

    
    
}
