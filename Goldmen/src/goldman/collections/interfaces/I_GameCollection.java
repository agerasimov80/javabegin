package goldman.collections.interfaces;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.movingalgorithm.interfaces.I_MovingAlgorithm;
import goldman.gameobjects.impl.Coordinate;
import goldman.listeners.interfaces.I_MoveResultNotifier;
import java.util.ArrayList;
import java.util.List;

public interface I_GameCollection extends I_MoveResultNotifier {

    void addGameObject(AbstractGameObject object);

    AbstractGameObject getObjectByCoordinate(int x, int y);

    AbstractGameObject getObjectByCoordinate(Coordinate coordinate);

    AbstractGameObject getPriorityObject(AbstractGameObject firstObject, AbstractGameObject secondObject);

    List  <AbstractGameObject> getAllGameObjects();

    ArrayList<AbstractGameObject> getGameObjects(E_GameObjectType type);
    
    void moveObject (I_MovingAlgorithm algorithm, E_Direction direction, E_GameObjectType type);
    //void moveObjectRandom ( E_ObjType type);

    void clear() ;


}
