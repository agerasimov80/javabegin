package goldman.collections.impl;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.enums.E_ActionResult;
import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.movingalgorithm.interfaces.I_MovingAlgorithm;
import goldman.gameobjects.impl.Coordinate;
import goldman.gameobjects.impl.Goldman;
import goldman.gameobjects.impl.Nothing;
import goldman.listeners.interfaces.I_MoveResultListener;
import goldman.listeners.abstracts.MoveListenerRegistrator;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;

public class MapGameCollection extends MoveListenerRegistrator {

    private final HashMap<Coordinate, List<AbstractGameObject>> gameObjects = new HashMap<>();
    private final EnumMap<E_GameObjectType, ArrayList<AbstractGameObject>> typeObjects = new EnumMap<>(E_GameObjectType.class);
    private Coordinate coord = null;

    @Override
    public void addGameObject(AbstractGameObject object) {

        if (typeObjects.get(object.getType()) == null) {
            typeObjects.put(object.getType(), new ArrayList<>());
        }
        typeObjects.get(object.getType()).add(object);

        if (gameObjects.get(object.getCoordinate()) == null) {
            gameObjects.put(object.getCoordinate(), new ArrayList<>());
        }
        gameObjects.get(object.getCoordinate()).add(object);
    }

    @Override
    public AbstractGameObject getPriorityObject(AbstractGameObject firstObject, AbstractGameObject secondObject) {
        return (firstObject.getType().getPriority() > secondObject.getType().getPriority()) ? firstObject : secondObject;
    }

    @Override
    public ArrayList<AbstractGameObject> getGameObjects(E_GameObjectType type) {
        return typeObjects.get(type);
    }

    @Override
    public ArrayList<AbstractGameObject> getAllGameObjects() {

        ArrayList<AbstractGameObject> fullList = new ArrayList<>();

        for (List<AbstractGameObject> listOfObjects : gameObjects.values()) {
            for (AbstractGameObject object : listOfObjects) {
                fullList.add(object);
            }
        }

        return fullList;
    }

    @Override
    public AbstractGameObject getObjectByCoordinate(Coordinate coordinate) {

        List list = gameObjects.get(coordinate);
        if (list == null) {
            return null;
        }
        return getTopPrioObject(list);
    }

    @Override
    public AbstractGameObject getObjectByCoordinate(int x, int y) {

        if (coord == null) {
            coord = new Coordinate(x, y);
        }
        coord.setX(x);
        coord.setY(y);
        return getObjectByCoordinate(coord);
    }

    public AbstractGameObject getTopPrioObject(List<AbstractGameObject> list) {

        AbstractGameObject abstractGameObject = null;
        for (AbstractGameObject obj : list) {

            if (abstractGameObject == null) {
                abstractGameObject = obj;
            }

            AbstractGameObject prioObject = getPriorityObject(abstractGameObject, obj);
            abstractGameObject = prioObject;
        }
        return abstractGameObject;
    }

    @Override
    public void moveObject(I_MovingAlgorithm algorithm, E_Direction direction, E_GameObjectType type) {

        Goldman goldman = (Goldman) this.getGameObjects(E_GameObjectType.GOLDMAN).get(0);
        E_Direction directionPassedToMethod = direction;

        E_ActionResult result = null;
        f:
        for (AbstractGameObject object : this.getGameObjects(type)) {

            if (object instanceof AbstractGameObjectMovable) {
                AbstractGameObjectMovable movingObject = (AbstractGameObjectMovable) object;

                if (directionPassedToMethod == null) {
                    direction = algorithm.getDirection(object, goldman, this);
                }

                Coordinate coordinateNew = getNewCoordinate(direction, movingObject);

                AbstractGameObject objectInNewCoordinate = this.getObjectByCoordinate(coordinateNew);
                
                if (objectInNewCoordinate != null) {
                    
                    result = movingObject.move(direction, objectInNewCoordinate);

                    // делаем дополнительные действия в коллекции перед тем, как передать резальтат движения фрейму
                    switch (result) {

                        case MOVE:
                            swapObjects(movingObject, objectInNewCoordinate);
                            break;

                        case CONSUME:
                            swapObjects(movingObject, new Nothing(coordinateNew));
                            removeObject(objectInNewCoordinate);

                            break;

                        case HIDE_IN_TREE:
                            objectInNewCoordinate = new Nothing(coordinateNew);
                            swapObjects(movingObject, objectInNewCoordinate);
                            break;

                        case NOACTION:
                            break;

                        case WIN:
                        case DIE:
                            break;
                    }
                    notifyMoveListeners(result, movingObject);
                }

            }
        }
    }

    void removeObject(AbstractGameObject abstractGameObject) {
        typeObjects.get(abstractGameObject.getType()).remove(abstractGameObject);
        gameObjects.get(abstractGameObject.getCoordinate()).remove(abstractGameObject);
    }

    private void swapObjects(AbstractGameObject obj1, AbstractGameObject obj2) {

        //gameObjects.put(obj1.getCoordinate(), obj1);
        //gameObjects.put(obj2.getCoordinate(), obj2);
        List list1 = gameObjects.get(obj1.getCoordinate());
        List list2 = gameObjects.get(obj2.getCoordinate());

        list1.remove(obj1);
        list2.remove(obj2);

        list1.add(obj2);
        list2.add(obj1);

        swapCoordinates(obj1, obj2);

    }

    private void swapCoordinates(AbstractGameObject obj1, AbstractGameObject obj2) {
        Coordinate tmpCoordinate = obj1.getCoordinate();
        obj1.setCoordinate(obj2.getCoordinate());
        obj2.setCoordinate(tmpCoordinate);
    }

    public int getTotalScore() {
        return ((Goldman) getGameObjects(E_GameObjectType.GOLDMAN).get(0)).getScore();
    }

    public int getTotalTurns() {
        return ((Goldman) getGameObjects(E_GameObjectType.GOLDMAN).get(0)).getTurnsCount();
    }

    private Coordinate getNewCoordinate(E_Direction direction, AbstractGameObjectMovable object) {

        int x = object.getCoordinate().getX();
        int y = object.getCoordinate().getY();

        switch (direction) {
            case Up:
                y -= object.getSteps();
                break;
            case Down:
                y += object.getSteps();
                break;
            case Left:
                x -= object.getSteps();
                break;
            case Right:
                x += object.getSteps();
                break;
            default:
        }
        return new Coordinate(x, y);
    }

    @Override
    public void notifyMoveListeners(E_ActionResult actionResult, AbstractGameObjectMovable abstractGameObjectMovable) {
        for (I_MoveResultListener listener : this.getMoveListeners()) {
            listener.moveEventHandler(actionResult, abstractGameObjectMovable);
        }
    }

    @Override
    public void clear() {
        this.gameObjects.clear();
        this.typeObjects.clear();
    }
}
