package goldman.scores.impl;

import goldman.database.SQLiteDBConnector;
import goldman.scores.interfaces.I_ScoreManager;
import goldman.gameobjects.User;
import goldman.gameobjects.UserScore;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbScoreSaver implements I_ScoreManager {

    SQLiteDBConnector dBConnector = SQLiteDBConnector.getInstance();
    private static Connection connection = null;
    private static Statement st = null;
    private static PreparedStatement pst = null;
    private static ResultSet rs = null;

    @Override
    public void saveResult(UserScore userScore) {

        try {

            // соединяемся
            connection = dBConnector.getConnection();
            connection.setAutoCommit(false);

            Date date = new Date();
            int[] cols = {1, 2, 3, 4};

            pst = connection.prepareStatement("Insert into score  "
                    + "(id, player_id, score, play_date) "
                    + "values (? ,? ,? ,?)",
                    cols);
            pst.setString(1, null);
            pst.setInt(2, userScore.getUser().getId());
            pst.setInt(3, userScore.getScore());
            pst.setLong(4, date.getTime());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException ex) {
            Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }

                if (pst != null) {
                    pst.close();
                }

                rs = null;
                pst = null;
                connection = null;

                dBConnector.closeConnection();

            } catch (SQLException ex) {
                Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    public ArrayList getScoreTable() {
        ArrayList<UserScore> list = new ArrayList<>();

        PreparedStatement selectStmt = null;
        ResultSet rs = null;

        try {

            selectStmt = dBConnector.getConnection().prepareStatement("select "
                    + "count(s.score) as play_count, "
                    + "s.score, "
                    + "s.play_date, "
                    + "p.name "
                    + "from score s inner join player p on p.id = s.player_id where s.score>0 "
                    + "group by p.name order by s.score desc, play_count asc limit 10");

            rs = selectStmt.executeQuery();

            while (rs.next()) {
                UserScore userScore = new UserScore();

                userScore.setUser(new User(rs.getString("name")));
                userScore.setScore(rs.getInt("score"));
                userScore.setPlay_date(rs.getLong("play_date"));
                userScore.setPlay_count(rs.getInt("play_count"));

                list.add(userScore);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (selectStmt != null) {
                try {
                    if (selectStmt != null) {
                        selectStmt.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        dBConnector.closeConnection();

        return list;
    }
}
