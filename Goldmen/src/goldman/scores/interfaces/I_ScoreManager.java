package goldman.scores.interfaces;

import goldman.gameobjects.UserScore;
import java.util.ArrayList;

public interface I_ScoreManager {

    void saveResult(UserScore userScore);
    
    ArrayList  getScoreTable ();
    
}
