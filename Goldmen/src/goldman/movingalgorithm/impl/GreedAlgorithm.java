
package goldman.movingalgorithm.impl;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_Direction;
import goldman.collections.interfaces.I_GameCollection;
import goldman.gameobjects.impl.Coordinate;
import goldman.movingalgorithm.interfaces.I_MovingAlgorithm;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GreedAlgorithm implements I_MovingAlgorithm {

    I_GameCollection collection;

    public GreedAlgorithm(I_GameCollection collection) {
        this.collection = collection;
    }
    
    
    
    @Override
    public E_Direction getDirection(AbstractGameObject object, AbstractGameObject goldman, I_GameCollection collection) {

        int goldmanX = goldman.getCoordinate().getX();
        int goldmanY = goldman.getCoordinate().getY();
        int objectX = object.getCoordinate().getX();
        int objectY = object.getCoordinate().getY();

        // сразу ест если рядом
        if ((goldmanX - objectX) == 1 && (goldmanY - objectY) == 0) {
            return E_Direction.Right;
        }
        if ((goldmanX - objectX) == -1 && (goldmanY - objectY) == 0) {
            return E_Direction.Left;
        }
        if ((goldmanX - objectX) == 0 && (goldmanY - objectY) == 1) {
            return E_Direction.Down;
        }
        if ((goldmanX - objectX) == 0 && (goldmanY - objectY) == -1) {
            return E_Direction.Up;
        }

        // если находим доступное для движения направление - то возвращаем первое случайное из них
        ArrayList<E_Direction> directions = getSurroundingMovablePositions(object.getCoordinate());
        
        if (!directions.isEmpty()) {
            return directions.get(getRandomInt(directions.size()));
        }

        // если предыдущий метод результата не дал (например доступных нправлений нет), 
        // то просто берем кратчайшее направление к Goldman'у
        return getPreferableDirection(object.getCoordinate(), goldman.getCoordinate());
    }

    int getRandomInt(int bound) {
        Random i = new Random();
        int r = i.nextInt(bound);
        return r;
    }
        
        
    E_Direction getPreferableDirection(Coordinate coordinate1, Coordinate coordinate2) {

        if (Math.abs(coordinate1.getX() - coordinate2.getX()) <= Math.abs(coordinate1.getY() - coordinate2.getY())) {

            if (coordinate1.getX() > coordinate2.getX()) {
                return E_Direction.Left;
            } else {
                return E_Direction.Right;
            }
        } else {
            if (coordinate1.getY() > coordinate2.getY()) {
                return E_Direction.Up;
            } else {
                return E_Direction.Down;
            }
        }
    }
    
    boolean checkIfMovable(Coordinate coordinate) {

        if (collection.getObjectByCoordinate(coordinate) == null) {
            return false;
        }


        switch (collection.getObjectByCoordinate(coordinate).getType()) {
            case NOTHING:
            case GOLDMAN:
                return true;
        }
        return false;
    }

    ArrayList<E_Direction> getSurroundingMovablePositions(Coordinate coordinate) {
        ArrayList<E_Direction> directions = new ArrayList<>();

        Coordinate coord;

        coord = new Coordinate(coordinate.getX() + 1, coordinate.getY());
        if (checkIfMovable(coord)) {
            directions.add(E_Direction.Right);
        }
        coord = new Coordinate(coordinate.getX() - 1, coordinate.getY());
        if (checkIfMovable(coord)) {
            directions.add(E_Direction.Left);
        }
        coord = new Coordinate(coordinate.getX(), coordinate.getY() + 1);
        if (checkIfMovable(coord)) {
            directions.add(E_Direction.Down);
        }
        coord = new Coordinate(coordinate.getX(), coordinate.getY() - 1);
        if (checkIfMovable(coord)) {
            directions.add(E_Direction.Up);
        }

        return directions;
    }
    
    
}
