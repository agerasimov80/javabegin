
package goldman.movingalgorithm.interfaces;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_Direction;
import goldman.collections.interfaces.I_GameCollection;


public interface I_MovingAlgorithm {

    E_Direction getDirection (AbstractGameObject movingObject, AbstractGameObject targetObject, I_GameCollection collection);
    
}
