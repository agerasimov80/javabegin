
package goldman.enums;

public enum E_Direction {
    Up,
    Down,
    Left,
    Right,
    Wait
}
