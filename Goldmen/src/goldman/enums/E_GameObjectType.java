
package goldman.enums;

public enum E_GameObjectType {
    
    TREE (6),
    MONSTER(5),
    TREASURE(4),
    EXIT(3),
    WALL(2),
    GOLDMAN(1),
    NOTHING(-1);

    
    private int priority;

    private E_GameObjectType(int priority) {
        this.priority = priority;
    }
    
    public int getPriority (){
        return this.priority;
    }
   
}
