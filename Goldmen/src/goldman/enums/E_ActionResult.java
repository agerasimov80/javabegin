
package goldman.enums;

public enum E_ActionResult {
    
WIN,
DIE,
MOVE,
NOACTION,
CONSUME,
HIDE_IN_TREE

}
