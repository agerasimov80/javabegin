package goldman.gamemap.facade;

import goldman.enums.E_Direction;
import goldman.enums.E_GameObjectType;
import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.loader.adapter.MapLoaderAdapter;
import goldman.gameobjects.SavedMapInfo;
import goldman.gameobjects.UserScore;
import goldman.gameobjects.impl.Goldman;
import goldman.listeners.interfaces.I_MoveResultListener;
import goldman.movingalgorithm.interfaces.I_MovingAlgorithm;
import goldman.scores.interfaces.I_ScoreManager;
import goldman.sounds.interfaces.I_SoundPlayer;
import java.awt.Component;

public class GameFacade {

    private MapLoaderAdapter mapLoader;
    private I_SoundPlayer soundPlayer;
    private I_ScoreManager scoreSaver;
    private MapInfo mapInfo;
    private AbstractGameMap gameMap;

    public GameFacade(MapLoaderAdapter mapLoader, I_SoundPlayer soundPlayer, I_ScoreManager scoreSaver) {
        this.mapLoader = mapLoader;
        this.soundPlayer = soundPlayer;
        this.scoreSaver = scoreSaver;
    }

    public void startBackgroundMusic() {
        soundPlayer.startBackgroundMusic();
    }

    public void stopBackgroundMusic() {
        soundPlayer.stopBackgroundMusic();
    }

    public void addMoveListener(I_MoveResultListener listener) {
        gameMap.getGameCollection().addMoveListener(listener);
    }

    public void moveObject(I_MovingAlgorithm algorithm, E_Direction movingDirection, E_GameObjectType gameObjectType) {
        gameMap.getGameCollection().moveObject(algorithm, movingDirection, gameObjectType);
    }

    public void gameInit() {
        gameMap = mapLoader.getGameMap();

        mapLoader.getGameMap().getGameCollection().getMoveListeners().clear();
        // слушатели для звуков должны идти в первую очередь, т.к. они запускаются в отдельном потоке и не мешают выполняться следующим слушателям
        if (soundPlayer instanceof I_MoveResultListener) {
            gameMap.getGameCollection().addMoveListener((I_MoveResultListener) soundPlayer);
        }
    }

    public Component getMapComponent() {
        gameInit();
        gameMap.drawMap();
        return mapLoader.getGameMap().getMapComponent();
    }

    public int getTurnsLeftCount() {
        return gameMap.getMapInfo().getTurnsLimit() - getGoldMan().getTurnsCount();
    }

    public int getTotalScore() {
        return getGoldMan().getScore();
    }

    public void saveScore() {
        UserScore userScore = new UserScore();
        userScore.setUser(gameMap.getMapInfo().getUser());
        userScore.setScore(getGoldMan().getScore());
        scoreSaver.saveResult(userScore);
    }

    public Goldman getGoldMan() {
        return (Goldman) mapLoader.getGameMap().getGameCollection().getGameObjects(E_GameObjectType.GOLDMAN).get(0);
    }

    public void stopGame() {
        soundPlayer.stopBackgroundMusic();
        gameMap.stopGame();
    }

    public void startGame() {
        soundPlayer.startBackgroundMusic();
        gameMap.startGame();
    }

    public void saveMap() {

        SavedMapInfo savedMapInfo = new SavedMapInfo();
        savedMapInfo.setId(gameMap.getMapInfo().getId());
        savedMapInfo.setUser(gameMap.getMapInfo().getUser());
        savedMapInfo.setTotalScore(getGoldMan().getScore());
        savedMapInfo.setTurnsCount(getGoldMan().getTurnsCount());

        mapLoader.saveMap(savedMapInfo, E_SourceType.Database);

    }

    public void drawMap() {
        gameMap.drawMap();
    }

}
