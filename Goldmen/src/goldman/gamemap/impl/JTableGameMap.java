package goldman.gamemap.impl;

import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_GameObjectType;
import goldman.collections.interfaces.I_GameCollection;
import goldman.movingalgorithm.impl.GreedAlgorithm;
import goldman.movingalgorithm.interfaces.I_MovingAlgorithm;
import goldman.gameobjects.impl.Coordinate;
import goldman.gameobjects.impl.Nothing;
import goldman.gameobjects.impl.Wall;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

public class JTableGameMap extends AbstractGameMap {

    private transient String columns[];
    private transient AbstractGameObject[][] mapObjects;
    private transient final JTable jTableMap = new JTable();
    private transient TimeMover timeMover;

    public JTableGameMap(I_GameCollection collection) {

        super(collection);
        jTableMap.setEnabled(false);
        jTableMap.setSize(300, 300);
        jTableMap.setRowHeight(25);
        jTableMap.setRowSelectionAllowed(false);
        jTableMap.setShowHorizontalLines(false);
        jTableMap.setShowVerticalLines(false);
        jTableMap.setTableHeader(null);
        jTableMap.setUpdateSelectionOnSort(false);
        jTableMap.setVerifyInputWhenFocusTarget(false);

        timeMover = new TimeMover(new GreedAlgorithm(collection));

    }

    @Override
    public Component getMapComponent() {
        return jTableMap;
    }

    @Override
    public boolean drawMap() {

        updateMapObjects();

        columns = new String[mapObjects[0].length];
        for (int i = 0; i < columns.length; i++) {
            columns[i] = "";
        }

        DefaultTableModel dtm = new DefaultTableModel(mapObjects, columns);
        jTableMap.setModel(dtm);
        for (int i = 0; i < jTableMap.getColumnModel().getColumnCount(); i++) {
            jTableMap.getColumnModel().getColumn(i).setCellRenderer(new ImageRenderer());
        }

        return true;
    }

    private void fillEmptyMap(int width, int height) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                mapObjects[y][x] = new Nothing(new Coordinate(x, y));
            }
        }
    }

    private void updateMapObjects() {

        mapObjects = new AbstractGameObject[super.getMapInfo().getHeight()][super.getMapInfo().getWidth()];

        fillEmptyMap(super.getMapInfo().getWidth(), super.getMapInfo().getHeight());

        for (AbstractGameObject gameObject : super.getGameCollection().getAllGameObjects()) {

            int y = gameObject.getCoordinate().getY();
            int x = gameObject.getCoordinate().getX();

//            if (gameObject instanceof AbstractGameObject) {
//                System.out.println("x = " + x);
//                System.out.println("y = " + y);
//            }
            if (!(mapObjects[y][x] instanceof Nothing || mapObjects[y][x] instanceof Wall)) {

                AbstractGameObject tmpObj = mapObjects[y][x];
                mapObjects[y][x] = super.getGameCollection().getPriorityObject(tmpObj, gameObject);
            } else {
                mapObjects[y][x] = gameObject;
            }
        }
    }

    @Override
    public void startGame() {
        this.timeMover.start();
    }

    @Override
    public void stopGame() {
        this.timeMover.stop();
    }

    private class TimeMover implements ActionListener {

        private static final int DELAY = 1000;                  //millisec 
        private static final int INITIAL_DELAY = 2000;          //millisec 
        private final Timer timer;
        I_MovingAlgorithm movingAlgorithm;

        public TimeMover(I_MovingAlgorithm algorithm) {

            this.timer = new Timer(DELAY, this);
            this.timer.setInitialDelay(INITIAL_DELAY);

            this.movingAlgorithm = algorithm;
        }

        public void start() {
            //this.soundPlayer.startBackgroundMusic();
            this.timer.start();
        }

        public void stop() {
            //this.soundPlayer.stopBackgroundMusic();
            this.timer.stop();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // если монстры есть - то их запускаем
            if (getGameCollection().getGameObjects(E_GameObjectType.MONSTER) != null) {
                getGameCollection().moveObject(movingAlgorithm, null, E_GameObjectType.MONSTER);
                drawMap();
            }
        }
    }
}
