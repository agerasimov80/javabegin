package goldman.gamemap.impl;

import goldman.gameobjects.abstracts.AbstractGameObject;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ImageRenderer extends DefaultTableCellRenderer {
//private static int t =0;    

    JLabel l = new JLabel();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        //System.out.println( ((AbstractGameObject )value).getIcon());
        try {
            l.setText("");
            l.setIcon(((AbstractGameObject) value).getIcon());
        } catch (Exception e) {
            System.out.println("No icon");
        }

        return l;
    }

}
