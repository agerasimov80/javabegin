package goldman.gamemap.loader.adapter;

import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.loader.impl.DBMapLoader;
import goldman.gamemap.loader.impl.FSMapLoader;
import goldman.gameobjects.SavedMapInfo;
import goldman.gameobjects.User;
import java.util.ArrayList;

public class MapLoaderAdapter {

    FSMapLoader fSMapLoader;
    DBMapLoader dBMapLoader;
    AbstractGameMap gameMap;
   

    public MapLoaderAdapter(AbstractGameMap abstractGameMap) {
        fSMapLoader = new FSMapLoader(abstractGameMap);
        dBMapLoader = new DBMapLoader(abstractGameMap);
    }

    public boolean loadMap(MapInfo mapInfo, E_SourceType type) {
        boolean result = false;
        switch (type) {
            case Database:
                result = dBMapLoader.loadMap(mapInfo);
                gameMap = dBMapLoader.getGameMap();
                break;
            case FileSystem:
                result = fSMapLoader.loadMap(mapInfo);
                gameMap = fSMapLoader.getGameMap();
                break;
            default:
                result = false;
                break;
        }
        return result;
    }


    public boolean saveMap(SavedMapInfo mapInfo, E_SourceType type) {
        boolean result = false;
        switch (type) {
            case Database:
                result = dBMapLoader.saveMap(mapInfo);
                break;
            case FileSystem:
                result = fSMapLoader.saveMap(mapInfo);
                break;
            default:
                result = false;
                break;
        }
        return result;
    }


    public ArrayList<SavedMapInfo> getSavedMapList(User user, E_SourceType type) {
        ArrayList<SavedMapInfo> result = null;
        switch (type) {
            case Database:
                result = dBMapLoader.getSavedMapList(user);
                break;
            case FileSystem:
                result = fSMapLoader.getSavedMapList(user);
                break;
        }
        return result;
    }


    public AbstractGameMap getGameMap() {
        return gameMap;
    }


    public boolean deleteSavedMap(MapInfo mapInfo, E_SourceType type) {
        boolean result = false;
        switch (type) {
            case Database:
                result = dBMapLoader.deleteSavedMap(mapInfo);
                break;
            case FileSystem:
                result = fSMapLoader.deleteSavedMap(mapInfo);
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    
    public int  getPlayerId(String username, E_SourceType type) {
       int result = 0;
        switch (type) {
            case Database:
                result = dBMapLoader.getPlayerId(username);
                break;
            case FileSystem:
                result = fSMapLoader.getPlayerId(username);
                break;
            default:
                result = 0;
                break;
        }
        return result;

        
    }
    
    
}
