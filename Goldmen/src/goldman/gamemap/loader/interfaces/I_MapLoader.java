
package goldman.gamemap.loader.interfaces;

import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gameobjects.SavedMapInfo;
import goldman.gameobjects.User;
import java.util.ArrayList;

public interface I_MapLoader {

    boolean loadMap(MapInfo mapInfo);

    boolean saveMap(SavedMapInfo mapInfo);
    
    public ArrayList<SavedMapInfo> getSavedMapList(User user);
    
    public AbstractGameMap getGameMap();
    
    boolean deleteSavedMap(MapInfo mapInfo);
    
    int getPlayerId (String name);
    
}
