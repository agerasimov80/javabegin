package goldman.gamemap.loader.abstracts;

import goldman.creators.GameObjectsFactory;
import goldman.database.SQLiteDBConnector;
import goldman.enums.E_GameObjectType;
import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.loader.interfaces.I_MapLoader;
import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.gameobjects.impl.Coordinate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractMapLoader implements I_MapLoader {

    private AbstractGameMap abstractGameMap;

    public AbstractMapLoader(AbstractGameMap abstractGameMap) {
        this.abstractGameMap = abstractGameMap;
    }


    @Override
    public AbstractGameMap getGameMap() {
        return abstractGameMap;
    }
    


    public void setGameMap(AbstractGameMap abstractGameMap) {
        this.abstractGameMap = abstractGameMap;
    }

    protected void createGameObject(String str, Coordinate coordinate) {

        E_GameObjectType type = E_GameObjectType.valueOf(str.toUpperCase());

        AbstractGameObject newObj = GameObjectsFactory.getInstance().getObject(type, coordinate);

        abstractGameMap.getGameCollection().addGameObject(newObj);

        if (newObj.getType() == E_GameObjectType.EXIT) {
            abstractGameMap.getMapInfo().setExitExist(true);
        } else if (newObj.getType() == E_GameObjectType.GOLDMAN) {
            abstractGameMap.getMapInfo().setGoldManExist(true);
        }

    }
    
    public int getPlayerId(String username) {

        PreparedStatement selectStmt = null;
        PreparedStatement insertStmt = null;
        ResultSet rs = null;

        try {

            // если есть уже такой пользователь - получить его id
            selectStmt = SQLiteDBConnector.getInstance().getConnection().prepareStatement("select id from player where name = ?");
            selectStmt.setString(1, username);

            rs = selectStmt.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            selectStmt.close();


            // если нет такого пользователя - создать его и вернуть id
            insertStmt = SQLiteDBConnector.getInstance().getConnection().prepareStatement("insert into player(name) values(?)");
            insertStmt.setString(1, username);
            insertStmt.executeUpdate();

            // получить id созданного пользователя
            selectStmt = SQLiteDBConnector.getInstance().getConnection().prepareStatement("select last_insert_rowid()");
            return selectStmt.executeQuery().getInt(1);

        } catch (SQLException ex) {
            Logger.getLogger(AbstractMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {

                if (selectStmt != null) {
                    selectStmt.close();
                }
                if (insertStmt != null) {
                    insertStmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AbstractMapLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return 0;
        
    }
}