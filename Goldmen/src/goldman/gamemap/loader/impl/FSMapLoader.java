package goldman.gamemap.loader.impl;

import goldman.enums.E_SourceType;
import goldman.gameobjects.impl.Coordinate;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.loader.abstracts.AbstractMapLoader;
import goldman.gameobjects.SavedMapInfo;
import goldman.gameobjects.User;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FSMapLoader extends AbstractMapLoader {

    @Override
    public boolean deleteSavedMap(MapInfo mapInfo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    File file;


    public FSMapLoader(File file, AbstractGameMap abstractGameMap) {
        super(abstractGameMap);
        this.file = file;
    }

    
    
    public FSMapLoader(AbstractGameMap abstractGameMap) {
        super(abstractGameMap);
    }

    @Override
    public boolean loadMap(MapInfo mapInfo ) {

        try {
            getGameMap().getGameCollection().clear();
            getGameMap().getMapInfo().setExitExist(false);
            getGameMap().getMapInfo().setGoldManExist(false);

            BufferedReader reader = (new BufferedReader(new FileReader(file)));

            // считываем первую строку для определения параметров карты
            String str = reader.readLine().trim();
            getGameMap().getMapInfo().setMapName(str.split(",")[0]);

            getGameMap().getMapInfo().setHeight(getLineCount(file));
            getGameMap().getMapInfo().setWidth(Integer.valueOf(str.split(",")[2]).intValue());

            getGameMap().getMapInfo().setTurnsLimit(Integer.valueOf(str.split(",")[1]).intValue());

            int x = 0;
            int y = 0;

            String strLine = null;
            while ((strLine = reader.readLine()) != null) {
                x = 0;
                for (String s : strLine.split(",")) {
                    super.createGameObject(s, new Coordinate(x, y));
                    x++;
                }

                y++;
            }

            if (!getGameMap().isMapValid()) {
                throw new Exception("The map is not valid!");
            }

        } catch (IOException ex) {
            Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return getGameMap().isMapValid();
    }

    @Override
    public boolean saveMap(SavedMapInfo savedMapInfo ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }

    private int getLineCount(File file) {
        BufferedReader reader = null;
        int lineCount = 0;
        try {
            reader = new BufferedReader(new FileReader(file));

            while (reader.readLine() != null) {
                lineCount++;
            }
            lineCount = lineCount - 1;// lineNumber-1 потому что первая строка из файла не входит в карту
        } catch (IOException ex) {
            Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return lineCount;

    }

    @Override
    public ArrayList<SavedMapInfo> getSavedMapList(User user ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
