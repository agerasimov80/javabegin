package goldman.gamemap.loader.impl;

import goldman.collections.interfaces.I_GameCollection;
import goldman.database.SQLiteDBConnector;
import goldman.enums.E_GameObjectType;
import goldman.enums.E_SourceType;
import goldman.gamemap.abstracts.AbstractGameMap;
import goldman.gamemap.abstracts.MapInfo;
import goldman.gamemap.loader.abstracts.AbstractMapLoader;
import goldman.gameobjects.SavedMapInfo;
import goldman.gameobjects.User;
import goldman.gameobjects.impl.Coordinate;
import goldman.gameobjects.impl.Goldman;
import goldman.scores.impl.DbScoreSaver;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.javabegin.training.goldman.utils.ObjectByteCreator;

public class DBMapLoader extends AbstractMapLoader {

    Statement loadStatement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    public final String DELIMITER = ",";

    public DBMapLoader(AbstractGameMap gameMap) {
        super(gameMap);
    }

    @Override
    public boolean loadMap(MapInfo mapInfo) {

        if (mapInfo.getLevelId() > 0) {
            return createNewMap(mapInfo.getLevelId());
        }

        if (mapInfo.getId() > 0) {
            return loadMap(mapInfo.getId());
        }

        return false;

    }

    private boolean createNewMap(int level_id) {

        getGameMap().getGameCollection().clear();
        getGameMap().getMapInfo().setExitExist(false);
        getGameMap().getMapInfo().setGoldManExist(false);

        try {

            connection = SQLiteDBConnector.getInstance().getConnection();
            loadStatement = connection.createStatement();
            resultSet = loadStatement.executeQuery(
                    "Select id, map_name, height, width, value, level_id, turns_limit "
                    + "from map where level_id =" + level_id );

            getGameMap().getMapInfo().setId(resultSet.getInt("id"));
            getGameMap().getMapInfo().setMapName(resultSet.getString("map_name"));
            getGameMap().getMapInfo().setHeight(resultSet.getInt("height"));
            getGameMap().getMapInfo().setWidth(resultSet.getInt("width"));
            getGameMap().getMapInfo().setTurnsLimit(resultSet.getInt("turns_limit"));

            String mapObjects = resultSet.getString("value");

            int x = 0, y = 0;

            for (String line : mapObjects.split(System.getProperty("line.separator"))) {
                x = 0;
                for (String object : line.split(DELIMITER)) {
                    super.createGameObject(object, new Coordinate(x, y));
                    x++;
                }
                y++;
            }

            if (!getGameMap().isMapValid()) {
                throw new Exception("The map is not valid!");
            }

        } catch (IOException ex) {
            Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FSMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                resultSet.close();
                loadStatement.close();
                SQLiteDBConnector.getInstance().closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(DBMapLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return getGameMap().isMapValid();
    }

    @Override
    public boolean saveMap(SavedMapInfo saveMapInfo) {
        PreparedStatement insertStmt = null;
        connection = SQLiteDBConnector.getInstance().getConnection();

        try {

            insertStmt = connection.prepareStatement("insert into saved_game(player_id, saved_date, collection, score, map_id, turns_count) values(?,?,?,?,?,?)");
            insertStmt.setInt(1, saveMapInfo.getUser().getId());
            insertStmt.setLong(2, new Date().getTime());
            getGameMap().getGameCollection().getMoveListeners().clear();
            insertStmt.setBytes(3, ObjectByteCreator.getBytes(getGameMap().getGameCollection()));
            insertStmt.setInt(4, saveMapInfo.getTotalScore());
            insertStmt.setInt(5, saveMapInfo.getId());
            insertStmt.setInt(6, saveMapInfo.getTurnsCount());

            insertStmt.executeUpdate();

            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DBMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            if (insertStmt != null) {
                try {
                    insertStmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBMapLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SQLiteDBConnector.getInstance().closeConnection();

        }

        return false;
    }
    
    
    @Override
    public boolean deleteSavedMap(MapInfo mapInfo) {
        PreparedStatement deleteStmt = null;
        SQLiteDBConnector sqliteDB = SQLiteDBConnector.getInstance();

        try {

            deleteStmt = sqliteDB.getConnection().prepareStatement("delete from saved_game where id=?");
            deleteStmt.setInt(1, mapInfo.getId());

            int result = deleteStmt.executeUpdate();

            if (result == 1) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBMapLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            if (deleteStmt != null) {
                try {
                    deleteStmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBMapLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

        return false;
    }

    private boolean loadMap(int id) {
        PreparedStatement selectStmt = null;

        ResultSet rs = null;
        connection = null;

        try {
            connection = SQLiteDBConnector.getInstance().getConnection();
            selectStmt = connection.prepareStatement("select "
                    + " g.player_id,"
                    + " g.saved_date,"
                    + " g.collection,"
                    + " g.score,"
                    + " g.turns_count,"
                    + " m.id,"
                    + " m.height,"
                    + " m.width,"
                    + " m.turns_limit,"
                    + " m.map_name,"
                    + " m.level_id"
                    + " from saved_game g inner join map m on m.id=g.map_id where g.id = ?");

            selectStmt.setInt(1, id);

            rs = selectStmt.executeQuery();

            while (rs.next()) {
                getGameMap().setGameCollection((I_GameCollection) ObjectByteCreator.getObject(rs.getBytes("collection")));
                
                getGameMap().getMapInfo().setId(rs.getInt("id"));
                getGameMap().getMapInfo().setHeight(rs.getInt("height"));
                getGameMap().getMapInfo().setWidth(rs.getInt("width"));
                getGameMap().getMapInfo().setMapName(rs.getString("map_name"));
                getGameMap().getMapInfo().setTurnsLimit(rs.getInt("turns_limit"));

                Goldman goldMan = (Goldman) getGameMap().getGameCollection().getGameObjects(E_GameObjectType.GOLDMAN).get(0);
                goldMan.setTurnsCount(rs.getInt("turns_count"));
                goldMan.setScore(rs.getInt("score"));
            }

            getGameMap().getMapInfo().setExitExist(false);
            getGameMap().getMapInfo().setGoldManExist(false);

        } catch (SQLException ex) {
            Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (selectStmt != null) {
                try {
                    if (selectStmt != null) {
                        selectStmt.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SQLiteDBConnector.getInstance().closeConnection();
        }

        return true;
    }

    @Override
    public ArrayList<SavedMapInfo> getSavedMapList(User user ) {

        ArrayList<SavedMapInfo> list = new ArrayList<>();
        PreparedStatement selectStmt = null;

        ResultSet rs = null;

        SQLiteDBConnector sqliteDB = SQLiteDBConnector.getInstance();

        try {

            selectStmt = sqliteDB.getConnection().prepareStatement("select * from saved_game where player_id = ? order by score desc");

            selectStmt.setInt(1, user.getId());

            rs = selectStmt.executeQuery();

            while (rs.next()) {
                SavedMapInfo mapInfo = new SavedMapInfo();
                mapInfo.setId(rs.getInt("id"));
                mapInfo.setSaveDate(rs.getLong("saved_date"));
                mapInfo.setUser(user);
                mapInfo.setTotalScore(rs.getInt("score"));
                mapInfo.setTurnsCount(rs.getInt("turns_count"));
                list.add(mapInfo);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            try {
                if (selectStmt != null) {
                    selectStmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return list;
    }
}