package goldman.gamemap.abstracts;

import goldman.collections.interfaces.I_GameCollection;
import goldman.gamemap.interfaces.I_TimeMap;
import java.io.Serializable;

public abstract class AbstractGameMap implements I_TimeMap, Serializable {

    private static final long serialVersionUID = 1L;
    private I_GameCollection collection;

    private MapInfo mapInfo;

    public AbstractGameMap(I_GameCollection collection) {
        this.collection = collection;
    }

    public boolean isMapValid() {
        return mapInfo.isExitExist() && mapInfo.isGoldManExist();
    }

    @Override
    public MapInfo getMapInfo() {
        return mapInfo;
    }

    public void setMapInfo(MapInfo mapInfo) {
        this.mapInfo = mapInfo;
    }

    @Override
    public I_GameCollection getGameCollection() {
        return collection;
    }

    public void setGameCollection(I_GameCollection coll) {
        this.collection = coll;
    }

}
