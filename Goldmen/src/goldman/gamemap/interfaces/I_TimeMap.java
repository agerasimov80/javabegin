
package goldman.gamemap.interfaces;


public interface I_TimeMap extends I_DrawableMap {
    
    void startGame ();
    void stopGame ();
    
}
