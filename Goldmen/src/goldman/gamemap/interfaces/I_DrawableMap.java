
package goldman.gamemap.interfaces;


import java.awt.Component;

public interface I_DrawableMap  extends I_MainMap {
    
    Component getMapComponent ();
    boolean drawMap ();

}
