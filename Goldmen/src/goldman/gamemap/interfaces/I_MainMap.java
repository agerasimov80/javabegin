
package goldman.gamemap.interfaces;

import goldman.collections.interfaces.I_GameCollection;
import goldman.gamemap.abstracts.MapInfo;

/**
 *
 * @author Alex
 */
interface I_MainMap {
 
    MapInfo getMapInfo();
    
    I_GameCollection getGameCollection();// все карты должн хранить коллекцию объектов
   
}
