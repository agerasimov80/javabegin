
package goldman.listeners.interfaces;

import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.enums.E_ActionResult;

public interface I_MoveResultListener {
    
    public void moveEventHandler (E_ActionResult actionResult, AbstractGameObjectMovable abstractGameObjectMovable); 
}
