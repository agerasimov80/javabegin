
package goldman.listeners.interfaces;

import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.enums.E_ActionResult;
import java.util.List;

public interface I_MoveResultNotifier {
 
    List <I_MoveResultListener> getMoveListeners ();
    
    void addMoveListener (I_MoveResultListener listener);
    
    void removeMoveListener (I_MoveResultListener listener);
    
    void removeAllMoveListeners ();

    void notifyMoveListeners (E_ActionResult actionResult, AbstractGameObjectMovable abstractGameObjectMovable);
    
}
