
package goldman.listeners.abstracts;

import goldman.collections.interfaces.I_GameCollection;
import goldman.listeners.interfaces.I_MoveResultListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public abstract class MoveListenerRegistrator  implements I_GameCollection, Serializable {
    
    

    ArrayList <I_MoveResultListener> listeners = new ArrayList<> ();
    
    @Override
    public List<I_MoveResultListener> getMoveListeners() {
        return listeners;
    }

    @Override
    public void addMoveListener(I_MoveResultListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeMoveListener(I_MoveResultListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void removeAllMoveListeners() {
        listeners.clear();
    }
  
}
