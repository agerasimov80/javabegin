package goldman.utils;

public interface StringValidator {

    boolean isValid(String text);

}
