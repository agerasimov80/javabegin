package goldman.utils;

import java.awt.Component;
import javax.swing.JOptionPane;

public class MessageManager {

    public static void showInformMessage(Component comp, E_MessageType messageType) {
        JOptionPane.showMessageDialog(comp, messageType.getMessage(), "Message Dialog", JOptionPane.PLAIN_MESSAGE);
    }

    public static int showYesNoCancelMessage(Component comp, E_MessageType messageType) {
        return JOptionPane.showConfirmDialog(comp, messageType.getMessage(), "Подтверждение", JOptionPane.YES_NO_CANCEL_OPTION);
    }

}
