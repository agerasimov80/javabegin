
package goldman.utils;

public enum E_MessageType {
    WIN ("Вы выиграли!"),
    LOOSE ("Вы проиграли!"),
    CONFIRM_SAVE_GAME ("Сохранить игру?"),
    ERROR_NO_SAVED_GAMES ("Нет сохраненных игр"),
    CONFIRM_DELETION ("Подтверждаете удаление?"),
    MESSAGE_SAVED_SUCCESS ("Игра сохранена");
    
   String message;

    private E_MessageType(String message) {
        this.message= message;
    
    } 
    
    public String getMessage (){
        return message;
    }
   
}
