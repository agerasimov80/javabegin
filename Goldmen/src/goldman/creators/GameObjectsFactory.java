package goldman.creators;

import goldman.gameobjects.abstracts.AbstractGameObject;
import goldman.enums.E_GameObjectType;
import goldman.gameobjects.impl.Coordinate;
import goldman.gameobjects.impl.Goldman;
import goldman.gameobjects.impl.Monster;
import goldman.gameobjects.impl.Nothing;
import goldman.gameobjects.impl.Exit;
import goldman.gameobjects.impl.Treasure;
import goldman.gameobjects.impl.Tree;
import goldman.gameobjects.impl.Wall;

public class GameObjectsFactory {

    private static GameObjectsFactory factory = null;

    private GameObjectsFactory() {
    }

    public static GameObjectsFactory getInstance() {
        if (factory == null) {
            factory = new GameObjectsFactory();
        }
        return factory;
    }

    public AbstractGameObject getObject(E_GameObjectType type, Coordinate coordinate) {

        switch (type) {
            case GOLDMAN:
                return new Goldman(coordinate);
            case MONSTER:
                return new Monster(coordinate);
            case TREASURE:
                return new Treasure(coordinate);
            case WALL:
                return new Wall(coordinate);
            case EXIT:
                return new Exit(coordinate);
            case NOTHING:
                return new Nothing(coordinate);
            case TREE:
                return new Tree(coordinate);
            default:
                throw new IllegalArgumentException("Неверный тип объекта:" + type);
        }
    }
}
