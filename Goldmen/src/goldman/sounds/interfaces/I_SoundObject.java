package goldman.sounds.interfaces;

import goldman.enums.E_ActionResult;
import javax.sound.sampled.Clip;

public interface I_SoundObject {
    
    Clip getSoundClip(E_ActionResult result);

}
