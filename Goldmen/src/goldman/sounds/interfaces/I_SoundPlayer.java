package goldman.sounds.interfaces;

import goldman.listeners.interfaces.I_MoveResultListener;
import javax.sound.sampled.Clip;

public interface I_SoundPlayer extends I_MoveResultListener{
    
    void startBackgroundMusic ();
    void stopBackgroundMusic ();
    void playSound (Clip clip, final boolean loop);
    
}
