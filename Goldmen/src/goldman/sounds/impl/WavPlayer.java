package goldman.sounds.impl;

import goldman.gameobjects.abstracts.AbstractGameObjectMovable;
import goldman.enums.E_ActionResult;
import goldman.sounds.interfaces.I_SoundObject;
import goldman.sounds.interfaces.I_SoundPlayer;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class WavPlayer implements I_SoundPlayer, Serializable {

    public static final String WAV_DIE = "die.wav";
    public static final String WAV_WIN = "win.wav";
    public static final String WAV_CONSUME = "treasure.wav";
    private static final String BACKGROUNDWAV = "background.wav";

    public static final String SOUND_PATH = "/goldman/sounds/files/";

    private transient Clip backGroundClip;

    public WavPlayer() {
        try {
            backGroundClip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(this.getClass().getResource(SOUND_PATH + BACKGROUNDWAV));
            backGroundClip.open(ais);
        } catch (LineUnavailableException | UnsupportedAudioFileException | IOException ex) {
            Logger.getLogger(WavPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void startBackgroundMusic() {
        playSound(backGroundClip, true);
    }

    @Override
    public void stopBackgroundMusic() {
        if (backGroundClip.isRunning()) {
            backGroundClip.stop();
        }
    }

    @Override
    public void moveEventHandler(E_ActionResult actionResult, AbstractGameObjectMovable abstractGameObjectMovable) {

        if (!(abstractGameObjectMovable instanceof I_SoundObject)) {
            return;
        }

        if (actionResult.equals(E_ActionResult.DIE) ||  actionResult.equals(E_ActionResult.WIN )) {
            stopBackgroundMusic();
        }


        I_SoundObject soundObject = (I_SoundObject) abstractGameObjectMovable;

        this.playSound(soundObject.getSoundClip(actionResult), false);

    }

    @Override
    public void playSound(Clip clip, final boolean loop) {

        if (clip == null) {
            return;
        }

        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {

                if (clip == null) {
                    return;
                }

                clip.setFramePosition(0);

                if (loop) {
                    clip.loop(Clip.LOOP_CONTINUOUSLY);
                } else {
                    if (clip.isRunning()) {
                        clip.stop();
                    }

                    clip.start();
                }

            }
        });
        t.setDaemon(true);
        t.start();
    }
}
