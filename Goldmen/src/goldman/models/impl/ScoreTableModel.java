package goldman.models.impl;

import goldman.gameobjects.UserScore;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.AbstractTableModel;

public class ScoreTableModel extends AbstractTableModel {

    // коллекция для хранения данных.
    ArrayList<UserScore> userScoreList;

    public ScoreTableModel(ArrayList<UserScore> userScoreList) {
        this.userScoreList = userScoreList;
    }

    @Override
    public int getRowCount() {
        return userScoreList.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public Object getValueAt(int rowIndex, int colIndex) {
        Object value = null;
        switch (colIndex) {
            case 0:
                value = rowIndex + 1;
                break;
            case 1:
                value = userScoreList.get(rowIndex).getUser().getName();
                break;
            case 2:
                Date date = new Date(userScoreList.get(rowIndex).getPlay_date());
                value = dateFormat.format(date);
                break;
            case 3:
                value = userScoreList.get(rowIndex).getScore();
                break;
            case 4:
                value = userScoreList.get(rowIndex).getPlay_count();
                break;
            default:
                throw new IndexOutOfBoundsException("Column index out of bounds: " + // NOI18N
                        colIndex);
        }

        return value;
    }

    @Override
    public Class<?> getColumnClass(int colIndex) {

        Class<?> clazz;
        switch (colIndex) {
            case 0:
            case 3:
            case 4:
                clazz = Integer.class;
                break;
            case 1:
            case 2:
                clazz = String.class;
                break;
            default:
                throw new IndexOutOfBoundsException("Column index out of bounds: " + colIndex);

        }
        return clazz;
    }

    @Override
    public String getColumnName(int colIndex) {
        String columnName;
        switch (colIndex) {
            case 0:
                columnName = "№";
                break;
            case 1:
                columnName = "Имя игрока";
                break;
            case 2:
                columnName = "Дата игры";
                break;
            case 3:
                columnName = "Кол-во очков";
                break;
            case 4:
                columnName = "Кол-во игр";
                break;
            default:
                throw new IndexOutOfBoundsException("Column index out of bounds: " + colIndex);
        }
        return columnName;
    }

}
