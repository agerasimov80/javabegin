package goldman.database;

import goldman.scores.impl.DbScoreSaver;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLiteDBConnector {

    private static Connection connection;
    private static final String DB_PATH = "db/goldman.db";
    
    private SQLiteDBConnector (){
        
    }
    
    private static SQLiteDBConnector instance ;
    
    public static SQLiteDBConnector getInstance () {
        if (instance == null)
            instance = new SQLiteDBConnector();
        return instance;
    }
   
    
    public Connection getConnection() {

        if (connection == null) {

            
            try {
                
                // динамическая регистрация драйвера
                Driver d = (Driver) Class.forName("org.sqlite.JDBC").newInstance();
                
                // создание подключения по пути указанному в URL
                connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
            
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
                Logger.getLogger(DbScoreSaver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return connection;
    }
    
    public void closeConnection () {
        if (connection != null )
            try {
                connection.close();
                connection= null;
        } catch (SQLException ex) {
            Logger.getLogger(SQLiteDBConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

 
}