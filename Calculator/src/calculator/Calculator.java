
package calculator;

import calculator.components.Button;
import calculator.components.Field;
import calculator.components.Panel;
import calculator.listeners.MyMouseListener;
import calculator.listeners.MySkinChangeActionListener;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Calculator extends JApplet {

    public static final String DEFAULT_TEXT =  "введите число";

    JFrame frame;    
    Field field1;
    Field field2;
    Field field3;

    Button btnPlus;
    Button btnMinus;
    Button btnDivide;
    Button btnMultiply;


    Button btnSkin1;
    Button btnSkin2;
    Button btnSkin3;

    JPanel jp1; // fields
    JPanel jp2; // buttons + - * /
    JPanel jp3; // buttons skins

    static Calculator calculator;

    @Override
    public void init() {
        
        try {
             UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Calculator.class.getName()).log(Level.SEVERE, null, ex);
        }           
        
        createFields (50, 30);
        createButtons(90, 30);
        //calculator.createPanels ();
        createPanels2 ();
        createFrame("Calculator", 220,270);     
        addBtnListeners();
    }
        

    public static void main(String[] args) {
        calculator = new Calculator();
        calculator.init();
       
    }

    private  void createFrame(String title, int width, int height)  {
        frame = new JFrame(title);
        frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(width,height);
        frame.setLocationRelativeTo(null);
       
        frame.setLayout(new BorderLayout());
        frame.setMinimumSize(new Dimension(210,270));
       // frame.setMaximumSize(new Dimension(310,320));
        
        frame.getContentPane().add(jp1, BorderLayout.NORTH);
        frame.getContentPane().add(jp2, BorderLayout.CENTER);
        frame.getContentPane().add(jp3, BorderLayout.SOUTH);
        
        frame.setVisible(true);
    }
    
    private  void createFields(int width, int height) {
        
        field1 = new  Field( DEFAULT_TEXT   , width, height);        
        field2 = new  Field( DEFAULT_TEXT   , width, height);
        field3 = new  Field( ""             , width, height);
        field3.setEditable(false);
    }
    
  
    private void createButtons (int width, int height) {        
        
        btnPlus = new Button("+", width, height);        
        btnMinus = new Button("-", width, height);
        btnMultiply = new Button("x", width, height);
        btnDivide = new Button("/", width, height);
        
        btnSkin1 = new Button("skin1", width, height);
        btnSkin2 = new Button("skin2", width, height);
        btnSkin3 = new Button("skin3", width, height);

        
    }

    private void createPanels() {
       
        jp1 = new Panel(180,90);
        BoxLayout bl1 = new BoxLayout (jp1, BoxLayout.Y_AXIS);
        jp1.setLayout(bl1);

        jp2 = new Panel(180, 80, new FlowLayout(FlowLayout.CENTER));
        jp2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        jp2.add(btnPlus);
        jp2.add(btnMinus);
        jp2.add(btnMultiply);
        jp2.add(btnDivide);
        
        jp1.add(field1);
        jp1.add(field2);
        jp1.add(field3);   
    }
    
    private void createPanels2() {
       
        jp1 = new Panel(180,90);
        BorderLayout bl1 = new BorderLayout();        
        jp1.setLayout(bl1);

        jp2 = new Panel(180, 80, new GridLayout(2,2));
        jp2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        jp3 = new Panel(180, 80, new FlowLayout());
        jp3.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        
        jp2.add(btnPlus);
        jp2.add(btnMinus);
        jp2.add(btnMultiply);
        jp2.add(btnDivide);
        
        jp3.add(btnSkin1);
        jp3.add(btnSkin2);
        jp3.add(btnSkin3);
        
        jp1.add(field1, BorderLayout.NORTH);
        jp1.add(field2, BorderLayout.CENTER);
        jp1.add(field3, BorderLayout.SOUTH);   
    }

    private void addBtnListeners() {
        btnPlus.addMouseListener(MyMouseListener.getInstance(field1,field2,field3));
        btnMultiply.addMouseListener(MyMouseListener.getInstance(field1,field2,field3));
        btnDivide.addMouseListener(MyMouseListener.getInstance(field1,field2,field3));
        btnMinus.addMouseListener(MyMouseListener.getInstance(field1,field2,field3));
        
        btnSkin1.addMouseListener(new MySkinChangeActionListener(frame));
        btnSkin2.addMouseListener(new MySkinChangeActionListener(frame));
        btnSkin3.addMouseListener(new MySkinChangeActionListener(frame));
    }
    
}
