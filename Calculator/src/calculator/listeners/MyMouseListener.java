package calculator.listeners;

import calculator.components.Button;
import calculator.components.Field;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class MyMouseListener extends MouseAdapter {

   private static MyMouseListener ml= null;

   private Field number1;
   private Field number2; 
   private Field result;

    public static MyMouseListener getInstance (Field number1, Field number2, Field result) {
        
        if (ml==null)          
            ml=new MyMouseListener(number1, number2, result);
       
        return ml;             
    }

    private MyMouseListener(Field number1, Field number2, Field result) {
        this.number1 = number1;
        this.number2 = number2;
        this.result = result;
        
        
    }
       
    
    
    @Override
    public void mouseClicked(MouseEvent e) {
            
        final double a = Double.parseDouble(number1.getText());
        final double b = Double.parseDouble(number2.getText());
        
            if (e.getSource() instanceof JButton)

           
            
            switch (((Button)(e.getSource())).getText()) {
                case "+" :
                    System.out.println("сложение");  
                    result.setText(String.valueOf(a+b));
                    break;
                case "-" :
                    System.out.println("вычитание");  
                    result.setText(String.valueOf(a-b));
                    break;
                case "x" :
                    System.out.println("умножение");  
                    result.setText(String.valueOf(a*b));
                    break;
                case "/" :
                    System.out.println("деление"); 
                    result.setText(String.valueOf(a/b));
                    break;
            }
    }
}
