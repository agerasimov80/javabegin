package calculator.listeners;

import calculator.Calculator;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTextField;

public class MyFocusListener extends FocusAdapter {

    private static final String DEFAULT_TEXT =  "введите число";
    JTextField jtf ;
    
    @Override
    public void focusLost(FocusEvent e) {
       if (e.getSource() instanceof JTextField) {
            jtf = (JTextField)e.getSource();
            if (jtf.isEditable())
                if (jtf.getText().trim().equals("")) {
                   jtf.setText(Calculator.DEFAULT_TEXT);
                   jtf.setForeground(Color.GRAY);
                }
        }     
    }

    
    @Override
    public void focusGained(FocusEvent e) {
        jtf = (JTextField)e.getSource();
            if ((jtf.getText().trim().equals(Calculator.DEFAULT_TEXT)))
                   jtf.setText("");
                   jtf.setForeground(Color.BLACK);
     }     
        
        
 }   

