package calculator.listeners;

import calculator.components.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class MySkinChangeActionListener extends MouseAdapter {

    private JFrame frame;
    
    public MySkinChangeActionListener(JFrame frame) {
        this.frame = frame;
    }

     
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JButton) {
        switch (((Button)(e.getSource())).getText()) {
        case "skin1" :
            {
                try {
                    UIManager.setLookAndFeel(new NimbusLookAndFeel());
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(MyMouseListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SwingUtilities.updateComponentTreeUI(this.frame);
            break;  
        case "skin2" :
            {
                try {
                    UIManager.setLookAndFeel(new MetalLookAndFeel());
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(MyMouseListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SwingUtilities.updateComponentTreeUI(this.frame);
            break; 
        case "skin3" :
            {
                try {
                    UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(MyMouseListener.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MySkinChangeActionListener.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(MySkinChangeActionListener.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(MySkinChangeActionListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SwingUtilities.updateComponentTreeUI(this.frame);
            break;
    }
   }
  }
}