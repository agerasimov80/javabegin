package calculator.components;

import calculator.listeners.MyFocusListener;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import javax.swing.JTextField;

public class Field extends JTextField {

    public Field(String text, int width, int height) {
        super();
        super.setPreferredSize(new Dimension(width, height));
        super.setText(text);
        super.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        super.setEditable(true);
        super.setForeground(Color.GRAY);
        super.addFocusListener(new MyFocusListener());
        
    }

    
    
}
