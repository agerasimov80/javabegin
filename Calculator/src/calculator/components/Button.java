package calculator.components;

import java.awt.Dimension;
import javax.swing.JButton;

public class Button extends JButton{

    public Button(String text, int width, int height ) {
        super(text);
        super.setSize(width, height);
        super.setPreferredSize(new Dimension(width, height));
    }
    
    
    
}
