package calculator.components;

import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Panel extends JPanel {

    public Panel(int width, int height, LayoutManager layout) {
        super(layout);
        super.setPreferredSize(new Dimension(width, height));
        super.setBorder(BorderFactory.createEtchedBorder());  
    }
    
    
    public Panel(int width, int height) {
        super();
        super.setPreferredSize(new Dimension(width, height));
        super.setBorder(BorderFactory.createEtchedBorder());  
    }
    
}
