
package JustPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {
    
    private static String bdPath;
    private static Connection connection;
    private static ArrayList <String> strings = new ArrayList();
    private static String [ ] sqlReq;
    
    
    public static void readFile    (String fileName){
           
        try  (BufferedReader reader = new BufferedReader(new FileReader(fileName));){
           
            String s ;
            while ( ( s = reader.readLine())!=null)
                 strings.add(s);
            
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void parseStrings () {
        bdPath= strings.get(0);
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i< strings.size(); i++)
            builder.append(strings.get(i));
        String str =  builder.toString();
        sqlReq = str.split(";");
        
    }
    
    
    public static void writeToFile  (String filePath, String str )  {
    
        try {
            Writer writer = new BufferedWriter(new FileWriter(filePath));
            writer.flush();
            writer.write(str);
        
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    public static  void openConnection ()  {
    
        try {
            Driver d  =  (Driver) Class.forName("org.sqlite.JDBC").newInstance();
            connection = DriverManager.getConnection("jdbc:sqlite:" + bdPath);
        
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void queryingDB ()  {
        try {
            
            Statement  statement =  connection.createStatement();
             
            StringBuilder resultBuilder = new StringBuilder();
            for (String sql : sqlReq) {
                resultBuilder.append("Запрос: " + sql + "\n");
                resultBuilder.append("Результат:" + "\n");
                
                ResultSet  resultSet =  statement.executeQuery(sql);
                
                ArrayList<ObjModel> list = new ArrayList<> () ;
                while (resultSet.next()){  
                    
                    ObjModel model = new ObjModel();
                    model.setId(resultSet.getInt("model_id"));
                    model.setName_en(resultSet.getString("model_en"));
                    model.setName_ru(resultSet.getString("model_ru"));
                    list.add(model);
                
                }
                for (ObjModel obj : list) {
                    resultBuilder.append(obj.getId() + "," + obj.getName_ru() + "," + obj.getName_en() + "\n");
                }
                resultBuilder.append("\n");
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    
    
    }
    
}
