/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JustPackage;

/**
 *
 * @author gerasal3
 */
public class ObjModel {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ru() {
        return name_ru;
    }

    public void setName_ru(String name_ru) {
        this.name_ru = name_ru;
    }
    Integer id;
    String name_en;
    String name_ru;

    @Override
    public String toString() {
        return getId() + " " + getName_en() + " " + name_ru ;
    }
    
         
    
}
