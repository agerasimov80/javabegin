
package carshop;

import AnotherPackage.JTableExample;
import AnotherPackage.MyModel;
import AnotherPackage.MyRenderer;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Start {

    private static final String url = "jdbc:sqlite:C:\\Users\\gerasal3\\Projects\\javabegin\\CarShop\\CarShop.db";
    
    
    public static void main(String[] args) {
        
        // первое задание
//         dbConnection_FirstTask();
        
        
        // второе задание 
//        String FILE_NAME = "C:\\Users\\gerasal3\\Projects\\javabegin\\CarShop\\requests.txt" ;   
//        Utils.readFile(FILE_NAME);
//        Utils.parseStrings();
//        Utils.openConnection();
//        Utils.queryingDB();   
        
        
        // третье задание JTable 
        JTableExample ("spr_car_Model") ;
        
        
        // четвертое задание
        //JTableExample2();
        
        

    }

    
        
    
    
    private static void dbConnection_FirstTask() {
        try {
            //        Connection con= null ;Statement stmt= null ;  ResultSet res = null;

            Driver d = (Driver) Class.forName("org.sqlite.JDBC").newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String sql = "Select * from spr_car_Model";
        sql = "Select * from spr_car_Model where model_en = ?";
       
        ResultSet res = null;
        String [] str = {"model_en"}; 
        
        try (Connection con = DriverManager.getConnection(url); 
             PreparedStatement stmt = con.prepareStatement(sql, str);  )
        {
       
            stmt.setString(1, "Corolla");
            // выполнение запроса
            //res = stmt.executeQuery(sql);
            res = stmt.executeQuery();
            
            // обработка результата
            while (res.next())
            {
                System.out.println(res.getString("model_en") + "-" + res.getObject("model_en"));
            }
            
            
        } catch ( SQLException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    

    private static void JTableExample(String tableName) {
       
        
        try {
            Connection con = JTableExample.getConnection(url);
            MyModel model = new MyModel (con, tableName) ;
            JTable table = new JTable();           
            table.setModel(model);
            table.setDefaultRenderer(Object.class, new  MyRenderer ());
      
            
            // добавление треугольничков для сортировки
            table.setAutoCreateRowSorter(true);
           

//            table.getModel().addTableModelListener(new TableModelListener() {
//
//                @Override
//                public void tableChanged(TableModelEvent e) {
//                                      int row = e.getFirstRow();
//                    
//                    int column = e.getColumn();
//                    
//                    System.out.println(row+" "+column);
//                    
//                    TableModel model = (TableModel) e.getSource();
//                    String columnName = model.getColumnName(column);
//                    Object data = model.getValueAt(row, column);
//                }
//            });
            
            // программная сортировка
            TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
            table.setRowSorter(sorter);
            ArrayList <RowSorter.SortKey> sortKeys = new ArrayList<>();
            int columnIndexToSort = 0;
            sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.ASCENDING));
            columnIndexToSort =1;
            sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.DESCENDING));
            sorter.setSortKeys(sortKeys);
            sorter.sort();

            
            JFrame frame = new JFrame();
            frame.setLayout(new BorderLayout());
            frame.setSize(300, 300);
            JScrollPane scroller = new JScrollPane(table,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED ,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            JButton b = new JButton("Обновить");
            b.setSize(100, 100);
            
            
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                       
                        model.updateSQL();
                    } catch (SQLException ex) {
                        Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            frame.add(b, BorderLayout.SOUTH);
            frame.add(scroller);
            frame.pack();
            frame.setVisible(true);
            
        
        } catch (SQLException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    
    
    
    private static void JTableExample2() {
       
            Object [] columnClass = {String.class,String.class, String.class , String.class};
            Object [] columnHeaders = {"Фамилия", "Имя", "Отчество","email"};
            Object [] []  tableData = {
                    {"Иванов", "Иван", "Иванович" ,"ivanov@a.ru"},
                    {"Сидоров", "Сидор", "Сидорович","sidorov@a.ru" }, 
                    {"Петров", "Петр", "Петрович","petrov@a.ru" } 
            };
             
            
            
            JTable table = new JTable(tableData, columnHeaders);           
            table.setDefaultRenderer(Object.class, new  MyRenderer ());

            table.getModel().addTableModelListener(new TableModelListener() {

                @Override
                public void tableChanged(TableModelEvent e) {
                                      int row = e.getFirstRow();
                    
                    int column = e.getColumn();
                    
                    System.out.println(row+" "+column);
                    
                    TableModel model = (TableModel) e.getSource();
                    String columnName = model.getColumnName(column);
                    Object data = model.getValueAt(row, column);
                }
            });
            
            
            table.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getSource() instanceof JTable)
                    {
                        JTable t = (JTable)e.getSource();
                        String col = table.getColumnName(t.getSelectedColumn());
                        String email= "";
                        if (col.equals("email")){
                           email = (String) table.getModel().getValueAt(t.getSelectedRow(), t.getSelectedColumn());
                            try {
                                Desktop desktop = Desktop.getDesktop();
                                String message = "mailto:username@domain.com?subject=New_Profile&body=seeAttachment&attachment=c:/Update8.txt";
                                URI uri = URI.create(message);
                                desktop.mail(uri);
                                //Desktop.getDesktop().mail(new URI("mailto:" + value + "?SUBJECT=Служебное%20письмо&body=Текст%20письма"));
                            } catch (IOException ex) {
                                Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                            System.out.println("email= " + email);
                        
                    }
                  
                }

            });
            
            // добавление треугольничков для сортировки
            table.setAutoCreateRowSorter(true);

            
            JFrame frame = new JFrame();
            frame.setLocationRelativeTo(null);
            frame.setSize(300, 300);
            JScrollPane scroller = new JScrollPane(table,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED ,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.add(scroller);
            frame.pack();
            frame.setVisible(true);
  
    }
}
