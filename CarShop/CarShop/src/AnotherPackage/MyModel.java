
package AnotherPackage;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Statement;
import javax.swing.table.AbstractTableModel;


public class MyModel extends AbstractTableModel{

    String [] columns;
    Object [] types;
    Object [][] contents;
    Connection con = null;
    String tableName= "";
    
    public MyModel(Connection con, String tableName) throws SQLException {
        
        super ();
        this.con = con;
        this.tableName= tableName;
        getTableConents (con,tableName);
        
    }   
    
    private void getTableConents(Connection con, String tableName) throws SQLException {
       
        
        DatabaseMetaData meta = con.getMetaData();
        ResultSet columnsMetaData = meta.getColumns(null, null, tableName, null);
        
        ArrayList <String> columnNames = new ArrayList();
        ArrayList  columnTypes = new ArrayList();
        
        while (columnsMetaData.next()){
            columnNames.add(columnsMetaData.getString("COLUMN_NAME"));
            
            switch (columnsMetaData.getInt("DATA_TYPE")) {
                case Types.NCHAR:
                case Types.NVARCHAR:
                    columnTypes.add(String.class);
                    break;
                case Types.INTEGER:
                    columnTypes.add(Integer.class);
                    break;
                case Types.FLOAT:
                    columnTypes.add(Float.class);
                    break;
                case Types.DOUBLE:
                case Types.REAL:
                    columnTypes.add(Double.class);
                    break;                               
                case Types.DATE:
                case Types.DATALINK:
                case Types.TIME:
                case Types.TIMESTAMP:
                    columnTypes.add(Date.class);
                    break;                
                default:
                    columnTypes.add(String.class);
                    break;  
            }   // switch
        }  // while  
        
        
        columns = new String  [columnNames.size()];
        columnNames.toArray(columns);
        types = columnTypes.toArray();
                
        
        Statement statement = con.createStatement();
        ResultSet res = statement.executeQuery("Select * from " + tableName);
        
        ArrayList rowList = new ArrayList ();
        
        while (res.next()){     // для каждой записи из таблицы
            
            
            ArrayList cells = new ArrayList (); // список значений каждой ячейки в строке
            Object    cellValue = null;
            for (int i = 0; i < types.length; i++) {
                Object type = types[i];
                if (type == String.class)        cellValue=res.getString(columns[i]);
                else if (type == Integer.class)  cellValue=res.getInt(columns[i]);
                else if (type == Float.class)    cellValue=res.getFloat(columns[i]);
                else if (type == Double.class)   cellValue=res.getDouble(columns[i]);
                else if (type == Date.class)     cellValue=res.getDate(columns[i]);
                
                cells.add(cellValue);   // добавляем значение ячейки в строке
            }
            
            rowList.add(cells.toArray()); // добавляем строку.
        } // while 
        
        contents = new Object [rowList.size()][];
        for (int i = 0; i < contents.length; i++) 
            contents[i]=(Object [])rowList.get(i); 
        
        res.close();
        statement.close();
        
    }
    
    @Override
    public int getRowCount() {
        return contents.length;
    }

    @Override
    public int getColumnCount() {
         if (contents.length == 0) {
            return 0;
        } else {
            return contents[0].length;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return  contents [rowIndex][columnIndex];
    }
  
   
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
       return true;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
        contents[rowIndex][columnIndex] = aValue;
        fireTableCellUpdated(rowIndex, columnIndex);
    }
 
    
    @Override
    public Class getColumnClass(int col) {
       return  (Class)types[col];
    }

    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    
     
    public void updateSQL () throws SQLException {
        
        Statement st = con.createStatement();
        ArrayList <String> updateSQLs = new ArrayList();
               
        for (int i=0; i<contents.length; i ++)
        {
           
           StringBuilder builder = new StringBuilder();
           builder.append("Update " + tableName  + " set ");
           int counter = 0;
           for (int j= 0; j< columns.length; j++)
            {
                
                if (!columns [j].contains("id")) {
                    if (counter==0) builder.append( columns[j]  + "= ")  ;
                    else            builder.append(", " + columns[j]   + "= ");                           
                    if (types [j] ==String.class)  builder.append ("'" );
                    builder.append( getValueAt (i,j) );
                    if (types [j] ==String.class)  builder.append ("'" );   
                    counter ++;
                }   
            }
           builder.append (" where model_id = " + getValueAt (i,0) + ";");
           updateSQLs.add(builder.toString());
        }
        
        for (String requestUpdate : updateSQLs)
            st.executeUpdate(requestUpdate);
      
        st.close();
    }

    
}
