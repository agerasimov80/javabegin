
package AnotherPackage;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


public class MyRenderer extends DefaultTableCellRenderer{

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
            
        setText (value.toString());
       
        if (column ==3){
               setBackground(Color.CYAN);
               setForeground(Color.BLACK);
        }
        else
        {
               setBackground(Color.GREEN);
               setForeground(Color.BLACK);
        }
        
         if (value.toString().contains("@")) setValue("<html><a href=\"#\">"+value+"</href></html>");
         
        return this;
    }
}
