
package AnotherPackage;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JTableExample {
    
    private static Connection con;
    
   public static Connection getConnection (String url) {
      
      if (con == null){ 
          try {
              Driver driver = (Driver) Class.forName("org.sqlite.JDBC").newInstance();
              con =DriverManager.getConnection(url);
          } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
              Logger.getLogger(JTableExample.class.getName()).log(Level.SEVERE, null, ex);
          }
          
      }   
      return con ;
   }
    
    
    
    
    
    
    
}
