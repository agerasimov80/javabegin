
package filesexceptions;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FilesDemo {

    
    public static void readTextFromFile  (String fileName){
        
        
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            
            String str;
            while ( (str = reader.readLine())!= null)
                System.out.println(str);
           
        } catch (IOException ex) {
            Logger.getLogger(FilesDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }
    
    
    
    
    public static  void readBytesFromFile (String fileName)   {
       
        
        byte [] buffer = new byte [10];
        
        try (BufferedInputStream bos= new BufferedInputStream (new FileInputStream(fileName) )){
            
            while (bos.read(buffer) !=-1){
            
                String str = new String(buffer);
                System.out.println(str);
            }
            
        }
            
        catch (IOException exception){
            System.out.println("Ошибка либо с доступностью ресурса, либо с операцией чтения/записи.");
            System.out.println(exception.getClass().getCanonicalName());
        }    
    
    
    }
    
    
    
    public static void writeTextToFile  (String fileName) {
        
        try (Writer writer = new BufferedWriter(new FileWriter (fileName), 10)){

            writer.write("Строка 1");
            writer.write("Строка 2");
            writer.flush();
        } catch (IOException ex) {
           
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Записали строки в файл.");
    }
    
    public static String [] getTxtFilesList (String path) {
        String [] fileNames= null;
        Map <String, Integer> files= new HashMap ();
        
        File file = new File (path);
        if (file.isDirectory()) {
            fileNames= file.list (new Filter () );
            StringBuilder builder;
            for (String fileName: fileNames ){
                builder = new StringBuilder (fileName);
                
                StringBuilder reversed =builder.reverse();
                int startingPos = builder.length() - builder.indexOf(".");
                String extention = builder.substring(startingPos);
                if (!files.containsKey(extention))
                {
                    files.put(extention, 1);
                }
                else
                {
                    int val = (int) files.get(extention);
                    val++;
                    files.put(extention, val);
                }
                
            }
        
        }
        return fileNames;
    }
    

    public static Map <String, Integer> getStatictics (String path) {
        File [] fileNames= null;
        Map <String, Integer> files= new HashMap ();
        
        File file = new File (path);
        if (file.isDirectory()) {
            fileNames= file.listFiles();
            StringBuilder builder;
            for (File file1: fileNames ){
                if (file1.isFile())  {
                    builder = new StringBuilder (file1.getName());                
                    builder.reverse();

                    int startingPos = builder.length() - builder.indexOf(".")-1;
                    String extention = builder.reverse().substring(startingPos+1);
                    if (!files.containsKey(extention))
                    {
                        files.put(extention, 1);
                    }
                    else
                    {
                        int val = (int) files.get(extention);
                        val++;
                        files.put(extention, val);
                    }
                }
            }
        
        }
        return files;
    }
    
    
    
    
    static class Filter implements FilenameFilter {
        
        @Override
        public boolean accept(File dir, String name) {
                  if (name.endsWith(".txt"))
                                return true;
                            else
                                return false;
        }
    }
                                
     
  
    
}
