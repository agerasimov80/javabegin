
package filesexceptions;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Start {
    
    public static void main(String[] args) {
        
        test();
 
        Exceptions.IndexOutOfBound();
        System.out.println("");
 
        Exceptions.ClassCast();
        System.out.println("");
        
        try {
        TrainingException.division();
        } catch (TrainingException te){
            if (te instanceof TrainingException)
                System.out.println("Поймали TrainingException");
        }
        System.out.println("");

        
        String fileName = "C:\\Users\\gerasal3\\test1.txt";
        FilesDemo.writeTextToFile(fileName);
        System.out.println("");
        

        FilesDemo.readBytesFromFile(fileName);
        System.out.println("");
        
        FilesDemo.readTextFromFile(fileName);
        System.out.println("");
        
        
        String path = "C:\\Users\\gerasal3\\";
        String [] fileList = FilesDemo.getTxtFilesList(path);
        if (fileList.length >0)
            for (String fileList1 : fileList) {
                System.out.println(fileList1);
            }
        System.out.println("");
        
        
        Map <String, Integer> m = FilesDemo.getStatictics(path);
        Set  <Entry <String, Integer>>  set = m.entrySet();
        for (Entry<String, Integer> entrySet : m.entrySet()) {
            String key = entrySet.getKey();
            Integer value = entrySet.getValue();
            System.out.println(key + ": " + value);
            
        }
    }

    private static void test() {
        test();
    }
    
    
}
