package filesexceptions;

import java.util.ArrayList;

public class Exceptions {

        private static Class1 class1 = new Class1();
        private static Class2 class2 = new Class2();
        private static ArrayList ar = new ArrayList();
    
    

    private static void ErrHandler(Throwable e) {
        
        //Exceptions
        if (e instanceof Exception) {
            System.out.println("Поймали Exception");
            if (e instanceof RuntimeException) {
                System.out.println("Поймали RuntimeException");
                if (e instanceof IndexOutOfBoundsException)
                    System.out.println("Поймали " + e.getClass().getCanonicalName());
            }
        }
        
        //Errors
        if (e instanceof Error) {
            System.out.println("Поймали Error");
        }
    }

    public static void IndexOutOfBound() {
        
        try {

            int[] arrInt;
            arrInt = new int[10];
            arrInt[10] = 1;                   // RuntimeException
            
        } catch (Throwable e) {
            System.out.println("Поймали Throwable object");
            ErrHandler(e);
        }
        
    }

    public static void ClassCast() {
      

       try {

            ar.add(class1);
            ar.add(class2);
            Class1 c = (Class1) ar.get(1);   // RuntimeException
            
        } catch (Throwable e) {
            System.out.println("Поймали Throwable object");
            ErrHandler(e);
        }
    }
    
    
    public static class Class1 {
    
    }
    
    public static class Class2 {
    
    }
    

}
