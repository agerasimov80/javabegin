
package arrayscollections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ArraysCollections {

    private static int arraySize;
    private static int [] arr ;

    
    public static void main(String[] args) {
     
        // создаем Scanner 
        Scanner  scanner = new Scanner (System.in);

        
        proceedWithArrayOfDigits(scanner);
        //proceedWithArrayOfChars (scanner);
    }
    
    
    
    

    private static void proceedWithArrayOfDigits(Scanner scanner) {
        
        // запрашиваем размер массива
        System.out.println("Please input array size. ");
        arraySize = getNumber(scanner);
        
        
        // создаем массив 
        arr  = new int [arraySize] ;
        int t=0;
        while(t<arr.length){
            System.out.println("Please enter an element. : ");
            arr [t++] = getNumber(scanner);    
        }
        printAr(arr);
        
        System.out.println("sum: " + sum(arr));
        System.out.println("average: " + average(arr));
        
        System.out.println("Сортируем по возрастанию.");
        sortAcsending(arr);
        printAr(arr);
        
        System.out.println("Сортируем по убыванию.");
        sortDescending(arr);
        printAr(arr);       
        
        System.out.println("Получаем только четные числа");
        int [] oddAr= getOddsOnly(arr);
        printAr(oddAr);
        
        System.out.println("Получаем только нечетные числа");
        int [] notOddAr= getNonOddsOnly(arr);
        printAr(notOddAr);
        
        System.out.println("Сортируем по возрастанию.");
        Arrays.sort(arr);
        
        
        
        
        printAr(arr);
        
        // элементы нужно удалять в обратном порядке
        ArrayList <Integer> a = new ArrayList ();              
        for (int i : arr)
            a.add((Integer)i);
        a.sort(Comparator.reverseOrder());
        
        Integer [] arrInt = new Integer[arr.length];
        a.toArray(arrInt);
    }
    
    
    

    private static void printAr(int [] arr) {
        // выводим содержимое массива
        System.out.println("Содержимое массива: ");
        for (int i: arr)
            System.out.println(i);
    }

    
    
    
    private static int getNumber(Scanner scanner) {
       int i ;
       while (true) {
           System.out.print("Please input a number: ");
           try{ 
               i = scanner.nextInt();
               break;
           }
           catch (InputMismatchException e ){
               System.out.println("It's not a number!");
               scanner.next();
           }
       }
       return i;
    }
    
    private static String getChar(Scanner scanner) {
              
       System.out.print("Please input a char. (The first char only will be considered) : ");           
       return scanner.next();
    }
    
    
    private static float average (int[]ar){
 
        return sum (ar)/ar.length;
    }

    private static int sum ( int [] ar){
        int sum =0;
        for (int i: ar)
            sum+=i;       
        return sum;
    }
    
    private static void sortAcsending (int [] ar){
        int elem;
        
        for (int i=0; i<ar.length; i++)
        {
            elem= ar[i];
            for (int j=i+1; j<arr.length; j++)
                if (elem > ar[j])
                {
                    elem += ar[j];
                    ar[j]=elem-ar[j];
                    elem -=ar[j];
                }
            ar[i]= elem;
        }
        
    }
 
    private static void sortDescending (int [] ar){
        int elem;
        
        for (int i=0; i<ar.length; i++)
        {
            elem= ar[i];
            for (int j=i+1; j<arr.length; j++)
                if (elem < ar[j])
                {
                    elem += ar[j];
                    ar[j]=elem-ar[j];
                    elem -=ar[j];
                }
            ar[i]= elem;
        }
       
    }
    
    private static int [] getOddsOnly (int [] ar){
        int [] array = new int [ar.length];
        int t=0;
        for (int i: ar)
            if (i%2 ==0)
                array [t++]=i;
        
        int [] array1 = new int [t];
        for (int i =0; i<t;i++)
            array1[i]= array[i];
            
        return array1;
    }
    
    private static int [] getNonOddsOnly (int [] ar){
        int [] array = new int [ar.length];
        int t=0;
        for (int i: ar)
            if (i%2 ==1)
                array [t++]=i;
        
        int [] array1 = new int [t];
        for (int i =0; i<t;i++)
            array1[i]= array[i];
            
        return array1;
    }

    private static void proceedWithArrayOfChars(Scanner scanner) {
        
        // задаем размер
        int count = getNumber(scanner);
        
        // получаем символ и создаем массив 
        String s = getChar (scanner);
        char c= (s.trim()).charAt(0); 
        //char [][] array =  new char [count] [count] ;
        
        char [][] array;
        array= getPatternA (count, c);
        System.out.println("Pattern A");
        System.out.println("");
        printArray (array);

        array= getPatternB (count, c);
        System.out.println("Pattern B");
        System.out.println("");
        printArray (array);

        array= getPatternC (count, c);
        System.out.println("Pattern C");
        System.out.println("");
        printArray (array);        
        
        array= getPatternD (count, c);
        System.out.println("Pattern D");
        System.out.println("");
        printArray (array);        
        
        
        
    }

    
    
    private static void printArray (char [][] array) {
         System.out.println("");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) 
                System.out.print(array[i][j]);             
            System.out.println("");
       }
        System.out.println("");
    }

    private static char [] []  getPatternA(int count, char c) {
        
        char [] [] ar =  new char [count][count];
        
        for (int i = 0; i < count; i++) {
            for  (int j =0 ; j <= i; j++) {
                ar[i][j] =  c;   
            }            
        }

        return ar;
    }
    
    
    private static char[][] getPatternB(int count, char c) {
        char [] [] ar =  new char [count][count];
        
        for (int i = 0; i < count; i++) {
            for  (int j = 0; j < count-i; j++) {
                ar[i][j] =  c;   
            }            
        }

        return ar;
    }

    private static char[][] getPatternC(int count, char c) {
        char [] [] ar =  new char [count][count];
        
        for (int i = 0; i < count; i++) {
            for  (int j = count-1; j>= count-i-1; j--) {
                ar[i][j] =  c;   
            }            
        }

        return ar;
    }

    private static char[][] getPatternD(int count, char c) {
         char [] [] ar =  new char [count][count];
        
        for (int i = 0; i < count; i++) {
            for  (int j = i; j < count; j++) {
                ar[i][j] =  c;   
            }            
        }

        return ar;
    }
    
}
