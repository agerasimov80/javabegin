package ru.javabegin.training.fastjava2.objects;

/**
 * Created by Alex on 15.04.2015.
 */
public class Door {


    private String color = "red";
    private int height = 2;
    private int width = 1;
    private boolean locked;

    private static boolean a, b;


    public void open() {
        System.out.println("The door is being opened.");
    }

    private void close(boolean usingKey) {
        System.out.println("The door is beeing closed");
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
