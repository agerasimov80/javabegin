package ru.javabegin.training.fastjava2.start;

import ru.javabegin.training.fastjava2.objects.Door;

public class Main {



    public static void main(String[] args) {

        Door d = new Door();

        d.open();

        d.setColor("blue");
        System.out.println("d.getColor() = " + d.getColor());

        d.setLocked(true);
        System.out.println("d.isLocked = " + d.isLocked());

        d.setHeight(4);
        d.setWidth(3);

        System.out.println("d.getHeight() = " + d.getHeight());
        System.out.println("d.getWidth() = " + d.getWidth());
    }
}
