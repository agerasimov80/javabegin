package ru.javabegin.fastjava2.objects;

/**
 * Created by Alex on 19.04.2015.
 */
public class Main {

    public static void main(String[] args) {
        Phone phone1= new Phone("G1000",1024,"Sumsung","GSM");
        phone1.setName("IPhone");
        System.out.println("phone1.getType() = " + phone1.getType());
        System.out.println("phone1.getName() = " + phone1.getName());

        Phone phone2 = new Phone("T34",100,"Samsung","GSM");
        Phone phone3 = new Phone("T34",100,"slider",1024,"Samsung","GSM");

        System.out.println("phone3.getName() = " + phone3.getName());
        System.out.println("phone3.getType() + \" \" + phone3.getName() + \" \" + phone3.getModel() + \" \" + phone3.getRam() = " + phone3.getType() + " " + phone3.getName() + " " + phone3.getModel() + " " + phone3.getRam());
        System.out.println("phone3.getWeight() + \" \" + phone3.getFormat() = " + phone3.getWeight() + " " + phone3.getFormat());
    }
}
