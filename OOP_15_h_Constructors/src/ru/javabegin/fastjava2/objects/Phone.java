package ru.javabegin.fastjava2.objects;

/**
 * Created by Alex on 19.04.2015.
 */
public class Phone {
    private String model;
    private String color;
    private int weight;
    private String format;

    private int ram;
    private String name;
    private String type;

    public Phone() {
    }


    public Phone(String model, int ram, String name, String type) {
        this.setModel(model);
        this.setRam(ram);
        this.setName(name);
        this.setType(type);
    }

    public Phone(String model, int weight, String format, int ram, String name, String type) {
        this(model, ram, name, type);
        this.weight = weight;
        this.format = format;
    }

    public Phone(String model, String color, int weight, String format, int ram, String name, String type) {
        this.model = model;
        this.color = color;
        this.weight = weight;
        this.format = format;
        this.ram = ram;
        this.name = name;
        this.type = type;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
