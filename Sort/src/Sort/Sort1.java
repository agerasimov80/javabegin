package Sort;

/**
 * Created by Alex on 01.05.2015.
 */
public class Sort1 implements ISort {

    @Override
    public void sort(int[] arr) {


        for (int i = 0; i < arr.length; i++) {

            // searching the minimum element for position number i
            int temp = arr[i];
            // checking the rest of the array starting with index j=i
            for (int j = i; j < arr.length; j++) {
                // in case the element is lower than we have - we change this element with element in the position i
                if (temp > arr[j]) {
                    temp += arr[j];
                    arr[j] = temp - arr[j];
                    temp -= arr[j];
                }
                arr[i] = temp;
            }
        }
    }
}
