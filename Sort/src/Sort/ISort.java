package Sort;

/**
 * Created by Alex on 01.05.2015.
 */
public interface ISort {

    static void printArray (int i []) {
        for (int j = 0; j <i.length ; j++) {
            System.out.println(i[j]);
        }
    };

    void sort(int[] arr) ;
}
