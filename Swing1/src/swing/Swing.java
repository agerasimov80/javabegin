
package swing;

import com.jtattoo.plaf.JTattooUtilities;
import java.awt.FlowLayout;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;

public class Swing { 

    public static void main(String[] args) throws ClassNotFoundException {
        
        try {
            // внешний вид для компонентов
            UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");  
                // com.sun.java.swing.plaf.motif.MotifLookAndFeel
                //new MetalLookAndFeel()
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Swing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Swing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Swing.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // внешний вид для всего контейнера, то есть применять ли скин также к главному окну.
        JFrame.setDefaultLookAndFeelDecorated(true);
        
        JB jb = new JB(200, 100, "Кнопка");
        JP jp = new JP(250, 350);
        JF jf = new JF(300, 400, "GUItest");

  
        Image imageIcon = new ImageIcon("C:\\Users\\gerasal3\\Projects\\javabegin\\Swing1\\src\\images\\icon.jpg").getImage();
        jf.setIconImage(imageIcon);
        

        // менеджер расположения
        FlowLayout fl2 = new FlowLayout();
        jp.setLayout (fl2);

        jp.add(jb);
        jf.getContentPane().add(jp);
        jf.setVisible(true);   
    }
    
}
