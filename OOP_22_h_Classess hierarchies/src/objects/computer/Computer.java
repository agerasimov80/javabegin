package objects.computer;

/**
 * Created by Alex on 20.04.2015.
 */
public abstract class Computer {

    String type;
    int ram;
    int processorFrequency;
    String processorModel;
    int weight;
    String producer;

    public void switchOn() {

    }

    ;

    public void switchOff() {

    }

    ;

    public Computer(int ram, int processorFrequency, String processorModel, int weight, String producer, String type) {
        this.ram = ram;
        this.processorFrequency = processorFrequency;
        this.processorModel = processorModel;
        this.weight = weight;
        this.producer = producer;
        this.type = type;
    }
}
