package objects.computer.apple.mainframe;

import objects.computer.lenovo.notebook.Notebook;

/**
 * Created by Alex on 20.04.2015.
 */
public class Model1 extends Notebook {

    private static int ram=4000;
    private static int processorFrequency= 2600;
    private static String processorModel = "Intel_i5";
    private static int weight = 2500;


    public Model1() {
        super(ram, processorFrequency, processorModel, weight);
    }


}
