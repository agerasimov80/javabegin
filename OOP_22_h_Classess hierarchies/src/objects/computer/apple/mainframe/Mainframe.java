package objects.computer.apple.mainframe;

import objects.computer.Computer;

/**
 * Created by Alex on 20.04.2015.
 */
public abstract class Mainframe extends Computer{


    public Mainframe(int ram, int processorFrequency, String processorModel, int weight) {
        super(ram, processorFrequency, processorModel, weight, "apple", "mainframe");
    }


}
