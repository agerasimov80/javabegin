package objects.computer.apple.notebook;

import objects.computer.lenovo.notebook.Notebook;

/**
 * Created by Alex on 20.04.2015.
 */
public class Pro extends Notebook {

    private static int ram=8000;
    private static int processorFrequency= 2600;
    private static String processorModel = "Intel_i7";
    private static int weight = 2500;


    public Pro() {
        super(ram, processorFrequency, processorModel, weight);
    }


}
