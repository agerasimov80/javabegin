package objects.computer.apple.notebook;

import objects.computer.Computer;

/**
 * Created by Alex on 20.04.2015.
 */
public abstract class Notebook extends Computer{


    public Notebook(int ram, int processorFrequency, String processorModel, int weight) {
        super(ram, processorFrequency, processorModel, weight, "apple", "notebook");
    }


}
