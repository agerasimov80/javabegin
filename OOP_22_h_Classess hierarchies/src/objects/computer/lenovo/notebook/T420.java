package objects.computer.lenovo.notebook;

/**
 * Created by Alex on 20.04.2015.
 */
public class T420 extends  Notebook{

    private static int ram=8000;
    private static int processorFrequency= 2700;
    private static String processorModel = "Intel_i5";
    private static int weight = 2500;


    public T420() {
        super(ram, processorFrequency, processorModel, weight);
    }


}
