package objects.computer.lenovo.notebook;

/**
 * Created by Alex on 20.04.2015.
 */
public class T400 extends  Notebook{

    private static int ram=4000;
    private static int processorFrequency= 2600;
    private static String processorModel = "Intel_i5";
    private static int weight = 2500;


    public T400( ) {
        super(ram, processorFrequency, processorModel, weight);
    }


}
