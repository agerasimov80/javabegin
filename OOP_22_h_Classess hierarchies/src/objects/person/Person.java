package objects.person;

/**
 * Created by Alex on 20.04.2015.
 */
public abstract class Person {

    public Person(String name, String surname, int age, int race, int height, int weight) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.race = race;
        this.height = height;
        this.weight = weight;
    }

    String name;
    String surname;
    int age;
    int race;
    int height;
    int weight;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public int getRace() {
        return race;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }


}
