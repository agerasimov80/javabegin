package objects.person.employed;

import objects.person.Person;

/**
 * Created by Alex on 20.04.2015.
 */
public abstract class Employed extends Person {


    public Employed(String name, String surname, int age, int race, int height, int weight, Profession profession) {
        super(name, surname, age, race, height, weight);
        this.isEmployed = true;
        this.profession=profession;
    }

    private boolean isEmployed;
    private Profession profession;

    public boolean isEmployed() {
        return isEmployed;
    }


    public void work() {

    }


}
