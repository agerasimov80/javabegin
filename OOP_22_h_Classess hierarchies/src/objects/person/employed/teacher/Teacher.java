package objects.person.employed.teacher;

import objects.person.employed.Employed;
import objects.person.employed.Profession;

/**
 * Created by Alex on 20.04.2015.
 */
public class Teacher extends Employed {

    public Teacher(String name, String surname, int age, int race, int height, int weight) {
        super(name, surname, age, race, height, weight, Profession.Teacher);
    }

    @Override
    public void work() {

        //teaching

    }

}
