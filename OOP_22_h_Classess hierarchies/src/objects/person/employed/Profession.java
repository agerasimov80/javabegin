package objects.person.employed;

/**
 * Created by Alex on 20.04.2015.
 */
public enum Profession {
    Doctor ("doctor"),
    Teacher("teacher"),
    Worker ("worker");

    private String profession;

    Profession (String profession) {
        this.profession=profession;
    }

    }
