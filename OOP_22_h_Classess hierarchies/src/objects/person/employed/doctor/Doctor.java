package objects.person.employed.doctor;

import objects.person.employed.Employed;
import objects.person.employed.Profession;

/**
 * Created by Alex on 20.04.2015.
 */
public  class Doctor extends Employed {

    public Doctor(String name, String surname, int age, int race, int height, int weight) {
        super(name, surname, age, race, height, weight, Profession.Doctor );
    }


    @Override
    public void work() {

        // do treatment
    }
}
