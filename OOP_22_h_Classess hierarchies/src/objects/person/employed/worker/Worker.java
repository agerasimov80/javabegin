package objects.person.employed.worker;

import objects.person.employed.Employed;
import objects.person.employed.Profession;

/**
 * Created by Alex on 20.04.2015.
 */
public  class Worker extends Employed {

    public Worker(String name, String surname, int age, int race, int height, int weight) {
        super(name, surname, age, race, height, weight, Profession.Worker );
    }

    @Override
    public void work() {
        // work
    }
}
