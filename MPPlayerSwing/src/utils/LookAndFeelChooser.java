
package utils;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class LookAndFeelChooser   {

    
    public static void setLookAndFeel (String lookAndFeel, Component component){
    
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(LookAndFeelChooser.class.getName()).log(Level.SEVERE, null, ex);
        } 
        SwingUtilities.updateComponentTreeUI(component);
    }
    
    public static void setLookAndFeel (LookAndFeel lookAndFeel, Component component){
    
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(LookAndFeelChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
        SwingUtilities.updateComponentTreeUI(component);
    }
    
    
        
    
}
