/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;


/**
 *
 * @author gerasal3
 */
public class MP3FileFilter extends FileFilter {

    String extension = "";
    String description = "" ;

    public MP3FileFilter(String extension, String description) {
        this.extension=extension;
        this.description =description;
    
    }
    
    
    @Override
     public boolean accept(File pathname) {
         return pathname.isDirectory() || ( pathname.isFile() && pathname.getAbsolutePath().endsWith(extension));
     }

    @Override
    public String getDescription() {
        return description;
    }

}
