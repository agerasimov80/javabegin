
package utils;

import components.PlayList;
import components.PlayListJListImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

public class FileUtils {
    
    public static void addFileFilter (JFileChooser fileChooser, String extension, String description) {
     
        fileChooser.removeChoosableFileFilter(null);
        fileChooser.setFileFilter(new MP3FileFilter(extension, description));
       
    }

    public static void serializePlayList(PlayList playList, String f) throws FileNotFoundException, IOException {

            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream (fos);
            oos.writeObject(playList);
        
    }
    
    
     public static PlayListJListImpl deserializePlayList(PlayList playList, File f) throws FileNotFoundException, IOException, ClassNotFoundException {
        
            f= new File (f.getAbsolutePath());
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fis);
            PlayListJListImpl pl = (PlayListJListImpl)ois.readObject();
            return pl;
            
    }
    

}
