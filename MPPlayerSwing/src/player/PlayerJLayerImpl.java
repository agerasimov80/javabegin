package player;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;


public class  PlayerJLayerImpl  extends Thread implements player.Player {    
Thread t ;

   
    private static InputStream in = null;
    private  Player p = null ;
    private static PlayerJLayerImpl pl = null;
    
    
    
   public static PlayerJLayerImpl getInstance (InputStream in) {
       if (pl == null)
           pl =  new PlayerJLayerImpl (in);
        return pl;
        
   }
    
    private PlayerJLayerImpl(InputStream in) {
        PlayerJLayerImpl.in = in;
    }

    @Override
    public  void  play() {
    
        if( p == null ){ 
            
          try {
         
              p = new javazoom.jl.player.Player(in);
                   System.out.println(p.getPosition());
            } catch (JavaLayerException ex) {
                Logger.getLogger(PlayerJLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
            }   
        }
        try {
            synchronized (p){
                p.play();
            }
        } catch (JavaLayerException ex) {
            Logger.getLogger(PlayerJLayerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void runPlayer() {
        start();            
    }

    @Override
     public void run() {
       //t = new Thread ();
       //start();
       
       t = Thread.currentThread();
       System.out.println("ZoomPlayer is running in Thread : " + Thread.currentThread().getName());
       
       play () ; 
       System.out.println("Второй поток завершается");
       
    }

    @Override
    public  void pause() {
        //t = Thread.currentThread();
       System.out.println("ZoomPlayer is running in Thread : " + t.getName());
        t.interrupt();
    }

    @Override
    public int getPosition() {

        return p.getPosition();
    }
    
    

}
