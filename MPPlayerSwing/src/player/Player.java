package player;

public interface Player  {
    
    public void play ();
    public void stop ();
    public void pause ();
    public int getPosition  () ;
    public void runPlayer () ;
    //public void run ();
 
}
