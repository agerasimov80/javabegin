
package components;

import java.awt.Event;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

public final class PlayListJListImpl extends JList<Object> implements PlayList, Serializable {
    
    PlayListItem currentItem;
    private DefaultListModel dlm = new DefaultListModel();;
    private int width;
    private int height;
    private PlayerGUI parentFrame;
    BasicPlayer basicPlayer;

    
    // выделяет песни если они содержат в названии эти символы.
    @Override
    public void searchItems(String string) {
        
        ArrayList <Integer> indexes = new ArrayList ();
    
        for (int i = 0 ; i< dlm.getSize(); i++)
            if (((PlayListItem)(dlm.getElementAt(i))).getName().toUpperCase().contains(string.toUpperCase()))
                    indexes.add(i);
        indexes.trimToSize();
        int [] array = new int [indexes.size()] ;
        for (int i=0; i<array.length;i++)
            array[i]=indexes.get(i);
        this.setSelectedIndices(array);
    }

  

    @Override
    public DefaultListModel getItems() {
        return dlm;
    }

    @Override
    public void  setPopUpMenu (JPopupMenu menu) {
        this.setComponentPopupMenu(menu);
    }


       
    @Override
    public PlayListItem getCurrentItem() {
        if (currentItem == null)
            currentItem = (PlayListItem) dlm.get(0);
        return currentItem;
    }
    
    // Constructor
    public PlayListJListImpl  (int width, int height, BasicPlayer basicPlayer) {

       this.width= width;
       this.height= height;
       this.basicPlayer=basicPlayer;
       
       
       this.getComponentPopupMenu();
       addListeners();
       setModel(dlm);
       setDesignJList ();
    }

    @Override
    public void add(PlayListItem item) {
    
        if (!dlm.contains(item))        
            dlm.addElement(item);
    }

    @Override
    public void delete(int index) {
        dlm.removeElementAt(index);
    }

    @Override
    public void deleteAll() {
        dlm.removeAllElements();
    }
    
    private void setDesignJList (){
        
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setToolTipText("Добавляйте и удаляйте песни");
        super.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setDragEnabled(true);
        setMaximumSize(new java.awt.Dimension(60, 500));
        setMinimumSize(new java.awt.Dimension(60, 300));
        setPreferredSize(new java.awt.Dimension(width, height));
        setValueIsAdjusting(true);       
    }

    private void addListeners() {
        
        // слушатели событий клавиатуры
        addKeyListener(new java.awt.event.KeyAdapter() {
            
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jListKeyPressed(evt);
            }
        });
        
        // слушатели событий мыши
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                
                // изменение currentItem
                if ( e.getSource() instanceof  JList) {
                    JList list = (JList) e.getSource();
                    if (list.getModel().getSize()!=0)
                        if (list.getSelectedIndex()!= -1)
                            currentItem = (PlayListItem)dlm.get(((JList) e.getSource()).getSelectedIndex());       
                }
            
                if (e.getButton()==e.BUTTON1 && e.getClickCount()==2)
                    try {
                        basicPlayer.play();
                } catch (BasicPlayerException ex) {
                    Logger.getLogger(PlayListJListImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                        
            }
        });
    }
    
    @Override
    public void selectAll () {
        this.getSelectionModel().addSelectionInterval(0, getModel().getSize()-1);
    }
    
   
    
     private void jListKeyPressed(java.awt.event.KeyEvent evt) {                                  
           
         switch (evt.getKeyCode()){
            
             // удаление выделенного
             case Event.DELETE:
                delete( this.getSelectedIndices());  
                break;
             
             // проигрываение песни
             case Event.ENTER:
                 this.parentFrame.playCurrent();     
         }
    }
        
      @Override
    public void delete(int[] indexes)  {
       
        // элементы нужно удалять в обратном порядке
        ArrayList <Integer> a = new ArrayList ();              
        for (int i : indexes)
            a.add((Integer)i);
        a.sort(Comparator.reverseOrder());
        for (int i : a) 
            dlm.removeElementAt(i);  
    }
    
     @Override
    public void deleteSelected()  {
       
        // элементы нужно удалять в обратном порядке
        ArrayList <Integer> a = new ArrayList ();              
       
        for (int i : this.getSelectedIndices())
            a.add((Integer)i);
        a.trimToSize();
        
        a.sort(Comparator.reverseOrder());
        for (int i : a) 
            dlm.removeElementAt(i);  
    }


    @Override
    public List  getSelectedItems() {
      return  (this.getSelectedValuesList());
    }

 
}