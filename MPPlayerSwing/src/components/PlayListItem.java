
package components;

import java.io.File;
import java.io.Serializable;

public class PlayListItem implements Serializable {
    
    private File file;
    
    public String getName() {
        return name;
    }

    public PlayListItem(String name, String path)           {
        this.name = name;
        this.path = path;
        this.file = new File(path);
     
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

        private String name;
        private String path;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
       return name;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.getPath().equals(((PlayListItem)obj).getPath()));
    }   
}
