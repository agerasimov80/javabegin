
package components;

import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JPopupMenu;


public interface PlayList {

    void selectAll();
    void add (PlayListItem item);
    void delete (int index);
    void delete (int [] indexes);
    void deleteAll ();
    void deleteSelected () ;
    
    public void setPopUpMenu (JPopupMenu menu);
   
    void searchItems (String string);
    
    DefaultListModel getItems ();
    PlayListItem getCurrentItem ();
    List <PlayListItem> getSelectedItems();
    
    
}
