package components;

import java.awt.Event;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import utils.FileUtils;
import utils.LookAndFeelChooser;

public class PlayerGUI extends javax.swing.JFrame implements BasicPlayerListener {

    static PlayerGUI mainForm;
    private final BasicPlayer basicPlayer =new BasicPlayer();

    public BasicPlayer getBasicPlayer() {
        return basicPlayer;
    }
    private PlayList playList;
    // Thread t = null;
    private String state = "STOPPED";
    private String stateVolume = "IS_NOT_MUTE";
    static float currentVolume;
    private static final String PLAY_LIST_EXTENSION = "pls";
    private static final String MP3_FILE_EXTENSION = "mp3";
    private static final String PLS_PLAYLIST_FILTER_DIALOG_DESCRIPTION = "pls playlist";
    private static final String MP3_FILES_FILTER_DIALOG_DESCRIPTION = "mp3 files";
    private long duration;
    private double bytesLen;
    private double posValue;
    private boolean moveAutomatic;
    
    
    

    public PlayerGUI() throws Exception {
        
        initComponents();
        jPopupMenu1.setSize(300, 300);       
        LookAndFeelChooser.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel", this);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        addListeners();

    }

    public void setPlayList(PlayList playList) {
        this.playList = playList;
        jScrollPane2.setViewportView(((JList) this.playList));  
    }
    
    
    public void playCurrent ()  {
        this.playSong();
    }
    

    private void addListeners() {
        this.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                switch (e.getButton()) {
                    case MouseEvent.BUTTON3: {
                        jPopupMenu1.setVisible(true);
                        break;
                    }
                }
            }
        });
        
        this.jTextFieldSearch.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
            
                switch (e.getKeyCode())
                {
                    case Event.ENTER:
                        
                        if (jTextFieldSearch.isFocusOwner()){
                            searchSong();
                            return;
                        }
                        if( !playList.getSelectedItems().isEmpty())
                             playSong();
                    break;                       
                }              
            }         
        });
  
       basicPlayer.addBasicPlayerListener(this);
   
    }

    public JPopupMenu getjPopupMenu1() {
        return jPopupMenu1;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jFileChooser = new javax.swing.JFileChooser();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        menuItemSaveList = new javax.swing.JMenuItem();
        menuItemPopAdd = new javax.swing.JMenuItem();
        menuItemPopDel = new javax.swing.JMenuItem();
        menuItemOpenList = new javax.swing.JMenuItem();
        jFrame1 = new javax.swing.JFrame();
        panelMain1 = new javax.swing.JPanel();
        panelList1 = new javax.swing.JPanel();
        jPanelButtons1 = new javax.swing.JPanel();
        btnAddSong1 = new javax.swing.JButton();
        btnRemoveSong1 = new javax.swing.JButton();
        btnSelectAll1 = new javax.swing.JButton();
        jPanelSearch1 = new javax.swing.JPanel();
        jTextFieldSearch1 = new javax.swing.JTextField();
        btnSearch1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        panelPlayer1 = new javax.swing.JPanel();
        sliderSong1 = new javax.swing.JSlider();
        panelVolume1 = new javax.swing.JPanel();
        btnVolume1 = new javax.swing.JToggleButton();
        sliderVolume1 = new javax.swing.JSlider();
        panelButtons1 = new javax.swing.JPanel();
        btnNextSong1 = new javax.swing.JButton();
        btnPrevSong1 = new javax.swing.JButton();
        panel3buttons1 = new javax.swing.JPanel();
        panel2buttons1 = new javax.swing.JPanel();
        btnStop1 = new javax.swing.JButton();
        btnPlayPause1 = new javax.swing.JButton();
        btnShuffle1 = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuFile1 = new javax.swing.JMenu();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuitemExit1 = new javax.swing.JMenuItem();
        manuPLayList1 = new javax.swing.JMenu();
        menuitemOpenPlayList1 = new javax.swing.JMenuItem();
        menuitemSavePlayList1 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menuitemMultipleSelection1 = new javax.swing.JCheckBoxMenuItem();
        menuDesign1 = new javax.swing.JMenu();
        menuSkin1 = new javax.swing.JMenu();
        menuItemSkin3 = new javax.swing.JMenuItem();
        menuItemSkin4 = new javax.swing.JMenuItem();
        menuitemIsResizable1 = new javax.swing.JCheckBoxMenuItem();
        panelMain = new javax.swing.JPanel();
        panelList = new javax.swing.JPanel();
        jPanelButtons = new javax.swing.JPanel();
        btnAddSong = new javax.swing.JButton();
        btnRemoveSong = new javax.swing.JButton();
        btnSelectAll = new javax.swing.JButton();
        jPanelSearch = new javax.swing.JPanel();
        jTextFieldSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        panelPlayer = new javax.swing.JPanel();
        sliderSong = new javax.swing.JSlider();
        panelVolume = new javax.swing.JPanel();
        btnVolume = new javax.swing.JToggleButton();
        sliderVolume = new javax.swing.JSlider();
        panelButtons = new javax.swing.JPanel();
        btnNextSong = new javax.swing.JButton();
        btnPrevSong = new javax.swing.JButton();
        panel3buttons = new javax.swing.JPanel();
        panel2buttons = new javax.swing.JPanel();
        btnStop = new javax.swing.JButton();
        btnPlayPause = new javax.swing.JButton();
        btnShuffle = new javax.swing.JToggleButton();
        jMenuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuitemExit = new javax.swing.JMenuItem();
        manuPLayList = new javax.swing.JMenu();
        menuitemOpenPlayList = new javax.swing.JMenuItem();
        menuitemSavePlayList = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuitemMultipleSelection = new javax.swing.JCheckBoxMenuItem();
        menuDesign = new javax.swing.JMenu();
        menuSkin = new javax.swing.JMenu();
        menuItemSkin1 = new javax.swing.JMenuItem();
        menuItemSkin2 = new javax.swing.JMenuItem();
        menuitemIsResizable = new javax.swing.JCheckBoxMenuItem();

        jFileChooser.setMultiSelectionEnabled(true);
        jFileChooser.getAccessibleContext().setAccessibleName("");

        menuItemSaveList.setText("SaveList");
        menuItemSaveList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSaveListActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuItemSaveList);

        menuItemPopAdd.setText("Add song");
        menuItemPopAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemPopAddActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuItemPopAdd);

        menuItemPopDel.setText("Delete song");
        menuItemPopDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemPopDelActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuItemPopDel);

        menuItemOpenList.setText("Open list");
        menuItemOpenList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemOpenListActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuItemOpenList);

        jFrame1.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jFrame1.setForeground(java.awt.Color.gray);
        jFrame1.setMinimumSize(new java.awt.Dimension(520, 300));

        panelMain1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelMain1.setMaximumSize(new java.awt.Dimension(600, 300));
        panelMain1.setMinimumSize(new java.awt.Dimension(500, 250));
        panelMain1.setName(""); // NOI18N
        panelMain1.setPreferredSize(new java.awt.Dimension(600, 350));
        panelMain1.setLayout(new java.awt.BorderLayout(2, 5));

        panelList1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelList1.setMinimumSize(new java.awt.Dimension(250, 300));
        panelList1.setPreferredSize(new java.awt.Dimension(250, 300));
        panelList1.setLayout(new java.awt.BorderLayout());

        btnAddSong1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/plus_16.png"))); // NOI18N
        btnAddSong1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSong1ActionPerformed(evt);
            }
        });
        jPanelButtons1.add(btnAddSong1);

        btnRemoveSong1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/remove_icon.png"))); // NOI18N
        btnRemoveSong1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveSong1ActionPerformed(evt);
            }
        });
        jPanelButtons1.add(btnRemoveSong1);

        btnSelectAll1.setText("Select all");
        btnSelectAll1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectAll1ActionPerformed(evt);
            }
        });
        jPanelButtons1.add(btnSelectAll1);

        panelList1.add(jPanelButtons1, java.awt.BorderLayout.SOUTH);

        jTextFieldSearch1.setPreferredSize(new java.awt.Dimension(150, 20));
        jPanelSearch1.add(jTextFieldSearch1);

        btnSearch1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_16.png"))); // NOI18N
        btnSearch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch1ActionPerformed(evt);
            }
        });
        jPanelSearch1.add(btnSearch1);

        panelList1.add(jPanelSearch1, java.awt.BorderLayout.NORTH);

        jScrollPane3.setComponentPopupMenu(jPopupMenu1);
        panelList1.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        panelMain1.add(panelList1, java.awt.BorderLayout.CENTER);

        panelPlayer1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelPlayer1.setMaximumSize(new java.awt.Dimension(300, 250));
        panelPlayer1.setMinimumSize(new java.awt.Dimension(250, 300));
        panelPlayer1.setPreferredSize(new java.awt.Dimension(250, 300));
        panelPlayer1.setLayout(new java.awt.BorderLayout());

        sliderSong1.setPaintTicks(true);
        sliderSong1.setValue(0);
        sliderSong1.setInheritsPopupMenu(true);
        sliderSong1.setMaximumSize(new java.awt.Dimension(32767, 50));
        sliderSong1.setMinimumSize(new java.awt.Dimension(250, 30));
        sliderSong1.setName("sliderSong"); // NOI18N
        sliderSong1.setPreferredSize(new java.awt.Dimension(250, 50));
        sliderSong1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderSong1StateChanged(evt);
            }
        });
        panelPlayer1.add(sliderSong1, java.awt.BorderLayout.NORTH);

        btnVolume1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/speaker.png"))); // NOI18N
        btnVolume1.setPreferredSize(new java.awt.Dimension(30, 30));
        btnVolume1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mute.png"))); // NOI18N
        btnVolume1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolume1ActionPerformed(evt);
            }
        });
        panelVolume1.add(btnVolume1);

        sliderVolume1.setMaximum(200);
        sliderVolume1.setPaintTicks(true);
        sliderVolume1.setSnapToTicks(true);
        sliderVolume1.setToolTipText("");
        sliderVolume1.setValue(30);
        sliderVolume1.setName(""); // NOI18N
        sliderVolume1.setPreferredSize(new java.awt.Dimension(200, 50));
        sliderVolume1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderVolume1StateChanged(evt);
            }
        });
        panelVolume1.add(sliderVolume1);

        panelPlayer1.add(panelVolume1, java.awt.BorderLayout.SOUTH);

        panelButtons1.setMinimumSize(new java.awt.Dimension(250, 150));
        panelButtons1.setPreferredSize(new java.awt.Dimension(250, 150));
        panelButtons1.setLayout(new java.awt.BorderLayout());

        btnNextSong1.setText(">>");
        panelButtons1.add(btnNextSong1, java.awt.BorderLayout.LINE_END);

        btnPrevSong1.setText("<<");
        panelButtons1.add(btnPrevSong1, java.awt.BorderLayout.WEST);

        panel3buttons1.setLayout(new java.awt.BorderLayout());

        panel2buttons1.setPreferredSize(new java.awt.Dimension(70, 73));
        panel2buttons1.setLayout(new java.awt.BorderLayout());

        btnStop1.setText("stop");
        btnStop1.setMinimumSize(new java.awt.Dimension(50, 23));
        btnStop1.setPreferredSize(new java.awt.Dimension(70, 30));
        btnStop1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStop1ActionPerformed(evt);
            }
        });
        panel2buttons1.add(btnStop1, java.awt.BorderLayout.EAST);

        btnPlayPause1.setText("play");
        btnPlayPause1.setPreferredSize(new java.awt.Dimension(70, 50));
        btnPlayPause1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPlayPause1MouseClicked(evt);
            }
        });
        btnPlayPause1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayPause1ActionPerformed(evt);
            }
        });
        panel2buttons1.add(btnPlayPause1, java.awt.BorderLayout.CENTER);

        btnShuffle1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/repeat.png"))); // NOI18N
        btnShuffle1.setText("shuffle off");
        btnShuffle1.setToolTipText("");
        btnShuffle1.setName(""); // NOI18N
        btnShuffle1.setOpaque(true);
        btnShuffle1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/shuffle.png"))); // NOI18N
        btnShuffle1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShuffle1ActionPerformed(evt);
            }
        });
        panel2buttons1.add(btnShuffle1, java.awt.BorderLayout.SOUTH);

        panel3buttons1.add(panel2buttons1, java.awt.BorderLayout.CENTER);

        panelButtons1.add(panel3buttons1, java.awt.BorderLayout.CENTER);

        panelPlayer1.add(panelButtons1, java.awt.BorderLayout.CENTER);

        panelMain1.add(panelPlayer1, java.awt.BorderLayout.WEST);

        jFrame1.getContentPane().add(panelMain1, java.awt.BorderLayout.CENTER);

        jMenuBar1.setComponentPopupMenu(jPopupMenu1);

        menuFile1.setText("File");
        menuFile1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuFile1.add(jSeparator3);

        menuitemExit1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/exit.png"))); // NOI18N
        menuitemExit1.setText("Exit");
        menuitemExit1.setPreferredSize(new java.awt.Dimension(150, 22));
        menuitemExit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemExit1ActionPerformed(evt);
            }
        });
        menuFile1.add(menuitemExit1);

        jMenuBar1.add(menuFile1);

        manuPLayList1.setText("PlayList");

        menuitemOpenPlayList1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/open-icon.png"))); // NOI18N
        menuitemOpenPlayList1.setText("Open");
        menuitemOpenPlayList1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemOpenPlayList1ActionPerformed(evt);
            }
        });
        manuPLayList1.add(menuitemOpenPlayList1);

        menuitemSavePlayList1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save_16.png"))); // NOI18N
        menuitemSavePlayList1.setText("Save");
        menuitemSavePlayList1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemSavePlayList1ActionPerformed(evt);
            }
        });
        manuPLayList1.add(menuitemSavePlayList1);
        manuPLayList1.add(jSeparator4);

        menuitemMultipleSelection1.setSelected(true);
        menuitemMultipleSelection1.setText("Multiple song selection mode ");
        manuPLayList1.add(menuitemMultipleSelection1);

        jMenuBar1.add(manuPLayList1);

        menuDesign1.setText("Design");

        menuSkin1.setText("Skin");
        menuSkin1.setHideActionText(true);
        menuSkin1.setPreferredSize(new java.awt.Dimension(150, 22));

        menuItemSkin3.setText("skin1");
        menuItemSkin3.setToolTipText("Выбрать скин");
        menuItemSkin3.setActionCommand("");
        menuItemSkin3.setAutoscrolls(true);
        menuItemSkin3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        menuItemSkin3.setPreferredSize(new java.awt.Dimension(150, 22));
        menuItemSkin3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSkin3ActionPerformed(evt);
            }
        });
        menuSkin1.add(menuItemSkin3);

        menuItemSkin4.setText("skin2");
        menuItemSkin4.setPreferredSize(new java.awt.Dimension(150, 22));
        menuItemSkin4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSkin4ActionPerformed(evt);
            }
        });
        menuSkin1.add(menuItemSkin4);

        menuDesign1.add(menuSkin1);

        menuitemIsResizable1.setSelected(true);
        menuitemIsResizable1.setText("Window Resizable");
        menuDesign1.add(menuitemIsResizable1);

        jMenuBar1.add(menuDesign1);

        jFrame1.setJMenuBar(jMenuBar1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setForeground(java.awt.Color.gray);
        setMaximumSize(new java.awt.Dimension(600, 500));
        setMinimumSize(new java.awt.Dimension(520, 300));
        setPreferredSize(new java.awt.Dimension(520, 350));

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, menuitemIsResizable, org.jdesktop.beansbinding.ELProperty.create("${selected}"), this, org.jdesktop.beansbinding.BeanProperty.create("resizable"));
        bindingGroup.addBinding(binding);

        panelMain.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelMain.setMaximumSize(new java.awt.Dimension(600, 300));
        panelMain.setMinimumSize(new java.awt.Dimension(500, 250));
        panelMain.setName(""); // NOI18N
        panelMain.setPreferredSize(new java.awt.Dimension(600, 350));
        panelMain.setLayout(new java.awt.BorderLayout(2, 5));

        panelList.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelList.setMinimumSize(new java.awt.Dimension(250, 300));
        panelList.setPreferredSize(new java.awt.Dimension(250, 300));
        panelList.setLayout(new java.awt.BorderLayout());

        btnAddSong.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/plus_16.png"))); // NOI18N
        btnAddSong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSongActionPerformed(evt);
            }
        });
        jPanelButtons.add(btnAddSong);

        btnRemoveSong.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/remove_icon.png"))); // NOI18N
        btnRemoveSong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveSongActionPerformed(evt);
            }
        });
        jPanelButtons.add(btnRemoveSong);

        btnSelectAll.setText("Select all");
        btnSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectAllActionPerformed(evt);
            }
        });
        jPanelButtons.add(btnSelectAll);

        panelList.add(jPanelButtons, java.awt.BorderLayout.SOUTH);

        jTextFieldSearch.setPreferredSize(new java.awt.Dimension(150, 20));
        jPanelSearch.add(jTextFieldSearch);

        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_16.png"))); // NOI18N
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        jPanelSearch.add(btnSearch);

        panelList.add(jPanelSearch, java.awt.BorderLayout.NORTH);

        jScrollPane2.setComponentPopupMenu(jPopupMenu1);
        panelList.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        panelMain.add(panelList, java.awt.BorderLayout.CENTER);

        panelPlayer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelPlayer.setMaximumSize(new java.awt.Dimension(300, 250));
        panelPlayer.setMinimumSize(new java.awt.Dimension(250, 300));
        panelPlayer.setPreferredSize(new java.awt.Dimension(250, 300));
        panelPlayer.setLayout(new java.awt.BorderLayout());

        sliderSong.setPaintTicks(true);
        sliderSong.setValue(0);
        sliderSong.setInheritsPopupMenu(true);
        sliderSong.setMaximumSize(new java.awt.Dimension(32767, 50));
        sliderSong.setMinimumSize(new java.awt.Dimension(250, 30));
        sliderSong.setName("sliderSong"); // NOI18N
        sliderSong.setPreferredSize(new java.awt.Dimension(250, 50));
        sliderSong.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderSongStateChanged(evt);
            }
        });
        sliderSong.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sliderSongMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sliderSongMouseReleased(evt);
            }
        });
        panelPlayer.add(sliderSong, java.awt.BorderLayout.NORTH);

        btnVolume.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/speaker.png"))); // NOI18N
        btnVolume.setPreferredSize(new java.awt.Dimension(30, 30));
        btnVolume.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/mute.png"))); // NOI18N
        btnVolume.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolumeActionPerformed(evt);
            }
        });
        panelVolume.add(btnVolume);

        sliderVolume.setMaximum(200);
        sliderVolume.setPaintTicks(true);
        sliderVolume.setSnapToTicks(true);
        sliderVolume.setToolTipText("");
        sliderVolume.setValue(30);
        sliderVolume.setName(""); // NOI18N
        sliderVolume.setPreferredSize(new java.awt.Dimension(200, 50));
        sliderVolume.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderVolumeStateChanged(evt);
            }
        });
        panelVolume.add(sliderVolume);

        panelPlayer.add(panelVolume, java.awt.BorderLayout.SOUTH);

        panelButtons.setMinimumSize(new java.awt.Dimension(250, 150));
        panelButtons.setPreferredSize(new java.awt.Dimension(250, 150));
        panelButtons.setLayout(new java.awt.BorderLayout());

        btnNextSong.setText(">>");
        panelButtons.add(btnNextSong, java.awt.BorderLayout.LINE_END);

        btnPrevSong.setText("<<");
        panelButtons.add(btnPrevSong, java.awt.BorderLayout.WEST);

        panel3buttons.setLayout(new java.awt.BorderLayout());

        panel2buttons.setPreferredSize(new java.awt.Dimension(70, 73));
        panel2buttons.setLayout(new java.awt.BorderLayout());

        btnStop.setText("stop");
        btnStop.setMinimumSize(new java.awt.Dimension(50, 23));
        btnStop.setPreferredSize(new java.awt.Dimension(70, 30));
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });
        panel2buttons.add(btnStop, java.awt.BorderLayout.EAST);

        btnPlayPause.setText("play");
        btnPlayPause.setPreferredSize(new java.awt.Dimension(70, 50));
        btnPlayPause.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPlayPauseMouseClicked(evt);
            }
        });
        btnPlayPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayPauseActionPerformed(evt);
            }
        });
        panel2buttons.add(btnPlayPause, java.awt.BorderLayout.CENTER);

        btnShuffle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/repeat.png"))); // NOI18N
        btnShuffle.setText("shuffle off");
        btnShuffle.setToolTipText("");
        btnShuffle.setName(""); // NOI18N
        btnShuffle.setOpaque(true);
        btnShuffle.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/shuffle.png"))); // NOI18N
        btnShuffle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShuffleActionPerformed(evt);
            }
        });
        panel2buttons.add(btnShuffle, java.awt.BorderLayout.SOUTH);
        btnShuffle.getAccessibleContext().setAccessibleName("");

        panel3buttons.add(panel2buttons, java.awt.BorderLayout.CENTER);

        panelButtons.add(panel3buttons, java.awt.BorderLayout.CENTER);

        panelPlayer.add(panelButtons, java.awt.BorderLayout.CENTER);

        panelMain.add(panelPlayer, java.awt.BorderLayout.WEST);

        getContentPane().add(panelMain, java.awt.BorderLayout.CENTER);

        jMenuBar.setComponentPopupMenu(jPopupMenu1);

        menuFile.setText("File");
        menuFile.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        menuFile.add(jSeparator1);

        menuitemExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/exit.png"))); // NOI18N
        menuitemExit.setText("Exit");
        menuitemExit.setPreferredSize(new java.awt.Dimension(150, 22));
        menuitemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemExitActionPerformed(evt);
            }
        });
        menuFile.add(menuitemExit);

        jMenuBar.add(menuFile);

        manuPLayList.setText("PlayList");

        menuitemOpenPlayList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/open-icon.png"))); // NOI18N
        menuitemOpenPlayList.setText("Open");
        menuitemOpenPlayList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemOpenPlayListActionPerformed(evt);
            }
        });
        manuPLayList.add(menuitemOpenPlayList);

        menuitemSavePlayList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save_16.png"))); // NOI18N
        menuitemSavePlayList.setText("Save");
        menuitemSavePlayList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuitemSavePlayListActionPerformed(evt);
            }
        });
        manuPLayList.add(menuitemSavePlayList);
        manuPLayList.add(jSeparator2);

        menuitemMultipleSelection.setSelected(true);
        menuitemMultipleSelection.setText("Multiple song selection mode ");
        manuPLayList.add(menuitemMultipleSelection);

        jMenuBar.add(manuPLayList);

        menuDesign.setText("Design");

        menuSkin.setText("Skin");
        menuSkin.setHideActionText(true);
        menuSkin.setPreferredSize(new java.awt.Dimension(150, 22));

        menuItemSkin1.setText("skin1");
        menuItemSkin1.setToolTipText("Выбрать скин");
        menuItemSkin1.setActionCommand("");
        menuItemSkin1.setAutoscrolls(true);
        menuItemSkin1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        menuItemSkin1.setPreferredSize(new java.awt.Dimension(150, 22));
        menuItemSkin1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSkin1ActionPerformed(evt);
            }
        });
        menuSkin.add(menuItemSkin1);

        menuItemSkin2.setText("skin2");
        menuItemSkin2.setPreferredSize(new java.awt.Dimension(150, 22));
        menuItemSkin2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemSkin2ActionPerformed(evt);
            }
        });
        menuSkin.add(menuItemSkin2);

        menuDesign.add(menuSkin);

        menuitemIsResizable.setSelected(true);
        menuitemIsResizable.setText("Window Resizable");
        menuDesign.add(menuitemIsResizable);

        jMenuBar.add(menuDesign);

        setJMenuBar(jMenuBar);

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPlayPauseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPlayPauseMouseClicked

        playSong();
    }//GEN-LAST:event_btnPlayPauseMouseClicked

    private void menuItemSkin1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSkin1ActionPerformed
        LookAndFeelChooser.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel", this);
    }//GEN-LAST:event_menuItemSkin1ActionPerformed

    private void menuItemSkin2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSkin2ActionPerformed
        LookAndFeelChooser.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel", this);
    }//GEN-LAST:event_menuItemSkin2ActionPerformed

    private void menuitemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemExitActionPerformed
        this.dispose();
    }//GEN-LAST:event_menuitemExitActionPerformed

    private void btnSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectAllActionPerformed
        playList.selectAll();
    }//GEN-LAST:event_btnSelectAllActionPerformed

    private void btnShuffleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShuffleActionPerformed
       
        JToggleButton jtglbtnSuffle = null;
        if (evt.getSource() instanceof JToggleButton) {
            jtglbtnSuffle = (JToggleButton) evt.getSource();
        }

        switch ((String) (jtglbtnSuffle.getText())) {

            case "shuffle off":
                jtglbtnSuffle.setText("shuffle on");
                break;
            case "shuffle on":
                jtglbtnSuffle.setText("shuffle off");
                break;
        }
    }//GEN-LAST:event_btnShuffleActionPerformed

    private void btnPlayPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayPauseActionPerformed


    }//GEN-LAST:event_btnPlayPauseActionPerformed

    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
        try {
            basicPlayer.stop();
            btnPlayPause.setText("play");
            state = "STOPPED";
        } catch (BasicPlayerException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnStopActionPerformed

    private void btnVolumeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolumeActionPerformed

//        System.out.println("basicPlayer.getGainValue() = " + basicPlayer.getGainValue());
//
//        if (basicPlayer.getGainValue() != -80.0f) {
//            currentVolume = basicPlayer.getGainValue();
//
//            try {
//                //
//                basicPlayer.setGain(0.0);
//            } catch (BasicPlayerException ex) {
//                Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//            try {
//                basicPlayer.setGain(1.0);
//            } catch (BasicPlayerException ex) {
//                Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }

    }//GEN-LAST:event_btnVolumeActionPerformed

    private void btnRemoveSongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveSongActionPerformed
        playList.deleteSelected();
    }//GEN-LAST:event_btnRemoveSongActionPerformed

    private void btnAddSongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSongActionPerformed

        jFileChooser.setMultiSelectionEnabled(true);
        FileUtils.addFileFilter(jFileChooser, MP3_FILE_EXTENSION, MP3_FILES_FILTER_DIALOG_DESCRIPTION);

        int result = jFileChooser.showDialog(this, "выбрать");
        if (result == JFileChooser.APPROVE_OPTION) {
            File[] files = jFileChooser.getSelectedFiles();
            for (File f : files) {
                playList.add(new PlayListItem(f.getName(), f.getPath()));
            }
        }
    }//GEN-LAST:event_btnAddSongActionPerformed

    private void menuitemSavePlayListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemSavePlayListActionPerformed

        jFileChooser.setMultiSelectionEnabled(false);
        FileUtils.addFileFilter(jFileChooser, PLAY_LIST_EXTENSION, PLS_PLAYLIST_FILTER_DIALOG_DESCRIPTION);

        int result = jFileChooser.showSaveDialog(this);

        if (result == JFileChooser.APPROVE_OPTION) {

            File f = jFileChooser.getSelectedFile();

            try {
                FileUtils.serializePlayList(playList, f.getPath());
            } catch (IOException ex) {
                Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            JOptionPane op = new JOptionPane("Error writing the file.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_menuitemSavePlayListActionPerformed

    private void menuitemOpenPlayListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemOpenPlayListActionPerformed
        jFileChooser.setMultiSelectionEnabled(false);
        FileUtils.addFileFilter(jFileChooser, "pls", "pls playlist");
        int result = jFileChooser.showSaveDialog(this);
        File f = jFileChooser.getSelectedFile();
        try {
            PlayList playListJListImpl = (PlayListJListImpl) (FileUtils.deserializePlayList(playList, f));

            this.playList = playListJListImpl;
            this.repaint();
            jScrollPane2.setViewportView(((JList) this.playList));
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_menuitemOpenPlayListActionPerformed

    private void menuItemOpenListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemOpenListActionPerformed
        menuitemOpenPlayListActionPerformed(evt);
    }//GEN-LAST:event_menuItemOpenListActionPerformed

    private void menuItemSaveListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSaveListActionPerformed
        menuitemSavePlayListActionPerformed(evt);
    }//GEN-LAST:event_menuItemSaveListActionPerformed

    private void menuItemPopAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemPopAddActionPerformed
        btnAddSongActionPerformed(evt);
    }//GEN-LAST:event_menuItemPopAddActionPerformed

    private void menuItemPopDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemPopDelActionPerformed
        btnRemoveSongActionPerformed(evt);
    }//GEN-LAST:event_menuItemPopDelActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        searchSong();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void searchSong() {
        switch (this.jTextFieldSearch.getText()) {
            
            case "": {
                
                break;
            }
            default: {
                playList.searchItems(this.jTextFieldSearch.getText());
            }
        }
    }

    private void sliderVolumeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderVolumeStateChanged
        
        
        setVolume(sliderVolume.getValue(), sliderVolume.getMaximum());
        if (currentVolume == 0) {
            btnVolume.setSelected(true);
        } else {
            btnVolume.setSelected(false);
        }

    }//GEN-LAST:event_sliderVolumeStateChanged

    private void sliderSongStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderSongStateChanged
        
        //System.out.println("sliderSong.getValue() = " + sliderSong.getValue());
        //System.out.println("sliderSong.getMaximum() = " + sliderSong.getMaximum());
        //long bytes = ((long)sliderSong.getValue())*( duration / (long)sliderSong.getMaximum() );     

        posValue =  (double) sliderSong.getValue() / sliderSong.getMaximum() ; // * 1.0 / 1000;
       this.jump(posValue);
        
       //jump(bytes);

    }//GEN-LAST:event_sliderSongStateChanged

    private void btnAddSong1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSong1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddSong1ActionPerformed

    private void btnRemoveSong1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveSong1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRemoveSong1ActionPerformed

    private void btnSelectAll1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectAll1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSelectAll1ActionPerformed

    private void btnSearch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearch1ActionPerformed

    private void sliderSong1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderSong1StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_sliderSong1StateChanged

    private void btnVolume1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolume1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVolume1ActionPerformed

    private void sliderVolume1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderVolume1StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_sliderVolume1StateChanged

    private void btnStop1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStop1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnStop1ActionPerformed

    private void btnPlayPause1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPlayPause1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPlayPause1MouseClicked

    private void btnPlayPause1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayPause1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPlayPause1ActionPerformed

    private void btnShuffle1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShuffle1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnShuffle1ActionPerformed

    private void menuitemExit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemExit1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuitemExit1ActionPerformed

    private void menuitemOpenPlayList1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemOpenPlayList1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuitemOpenPlayList1ActionPerformed

    private void menuitemSavePlayList1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuitemSavePlayList1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuitemSavePlayList1ActionPerformed

    private void menuItemSkin3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSkin3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuItemSkin3ActionPerformed

    private void menuItemSkin4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemSkin4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuItemSkin4ActionPerformed

    private void sliderSongMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSongMousePressed
        moveAutomatic = false;
    }//GEN-LAST:event_sliderSongMousePressed

    private void sliderSongMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderSongMouseReleased
        //if (sliderSong.getValueIsAdjusting() == false) {
            posValue = sliderSong.getValue() * 1.0 / sliderSong.getMaximum();
            this.jump(posValue);
        try {
            basicPlayer.seek(500000);
            // }
        } catch (BasicPlayerException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        moveAutomatic = true;
    }//GEN-LAST:event_sliderSongMouseReleased

    private void playSong() {

        PlayListItem currentItem = playList.getCurrentItem();
        if (currentItem == null) {
            return;
        }

        try {
            switch (state.toUpperCase()) {

                case "STOPPED": {
                    btnPlayPause.setText("pause");

                    File f = playList.getCurrentItem().getFile();
                    if (f.isFile()) {
                        
                        basicPlayer.open(new URL("file:///" + f.getPath()));
                      
                        basicPlayer.seek(500000);
                        basicPlayer.play();
                        basicPlayer.seek(10000);
                        setVolume(sliderVolume.getValue(), sliderVolume.getMaximum());
                    }
                    state = "PLAYING";
                    break;
                }

                case "PLAYING": {
                    btnPlayPause.setText("play");
                    basicPlayer.pause();// p.pause();
                    state = "PAUSED";
                    break;
                }

                case "PAUSED": {
                    btnPlayPause.setText("pause");
                    basicPlayer.resume();
                    state = "PLAYING";
                    break;
                }
            }
        } catch (BasicPlayerException | MalformedURLException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    private void setVolume(int currentValue, int maximumValue) {
        try {
            this.currentVolume = currentValue;
            if (currentValue == 0) {
                basicPlayer.setGain(0.0);
            } else {
                basicPlayer.setGain((double) sliderVolume.getValue() / (double) sliderVolume.getMaximum());
            }
        } catch (BasicPlayerException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddSong;
    private javax.swing.JButton btnAddSong1;
    private javax.swing.JButton btnNextSong;
    private javax.swing.JButton btnNextSong1;
    private javax.swing.JButton btnPlayPause;
    private javax.swing.JButton btnPlayPause1;
    private javax.swing.JButton btnPrevSong;
    private javax.swing.JButton btnPrevSong1;
    private javax.swing.JButton btnRemoveSong;
    private javax.swing.JButton btnRemoveSong1;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSearch1;
    private javax.swing.JButton btnSelectAll;
    private javax.swing.JButton btnSelectAll1;
    private javax.swing.JToggleButton btnShuffle;
    private javax.swing.JToggleButton btnShuffle1;
    private javax.swing.JButton btnStop;
    private javax.swing.JButton btnStop1;
    private javax.swing.JToggleButton btnVolume;
    private javax.swing.JToggleButton btnVolume1;
    private javax.swing.JFileChooser jFileChooser;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelButtons1;
    private javax.swing.JPanel jPanelSearch;
    private javax.swing.JPanel jPanelSearch1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JTextField jTextFieldSearch;
    private javax.swing.JTextField jTextFieldSearch1;
    private javax.swing.JMenu manuPLayList;
    private javax.swing.JMenu manuPLayList1;
    private javax.swing.JMenu menuDesign;
    private javax.swing.JMenu menuDesign1;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuFile1;
    private javax.swing.JMenuItem menuItemOpenList;
    private javax.swing.JMenuItem menuItemPopAdd;
    private javax.swing.JMenuItem menuItemPopDel;
    private javax.swing.JMenuItem menuItemSaveList;
    private javax.swing.JMenuItem menuItemSkin1;
    private javax.swing.JMenuItem menuItemSkin2;
    private javax.swing.JMenuItem menuItemSkin3;
    private javax.swing.JMenuItem menuItemSkin4;
    private javax.swing.JMenu menuSkin;
    private javax.swing.JMenu menuSkin1;
    private javax.swing.JMenuItem menuitemExit;
    private javax.swing.JMenuItem menuitemExit1;
    private javax.swing.JCheckBoxMenuItem menuitemIsResizable;
    private javax.swing.JCheckBoxMenuItem menuitemIsResizable1;
    private javax.swing.JCheckBoxMenuItem menuitemMultipleSelection;
    private javax.swing.JCheckBoxMenuItem menuitemMultipleSelection1;
    private javax.swing.JMenuItem menuitemOpenPlayList;
    private javax.swing.JMenuItem menuitemOpenPlayList1;
    private javax.swing.JMenuItem menuitemSavePlayList;
    private javax.swing.JMenuItem menuitemSavePlayList1;
    private javax.swing.JPanel panel2buttons;
    private javax.swing.JPanel panel2buttons1;
    private javax.swing.JPanel panel3buttons;
    private javax.swing.JPanel panel3buttons1;
    private javax.swing.JPanel panelButtons;
    private javax.swing.JPanel panelButtons1;
    private javax.swing.JPanel panelList;
    private javax.swing.JPanel panelList1;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelMain1;
    private javax.swing.JPanel panelPlayer;
    private javax.swing.JPanel panelPlayer1;
    private javax.swing.JPanel panelVolume;
    private javax.swing.JPanel panelVolume1;
    private javax.swing.JSlider sliderSong;
    private javax.swing.JSlider sliderSong1;
    private javax.swing.JSlider sliderVolume;
    private javax.swing.JSlider sliderVolume1;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables


    @Override
    public void opened(Object o, Map map) {
        
        System.out.println("o = " + o);
        System.out.println("map = " + map);
       
        URL u = (URL) o;       
        
        File f = new File(u.getFile());
        bytesLen = (f).length(); 

       
//        еще один вариант определения mp3 тегов
//        AudioFileFormat aff = null;
//        try {
//            aff = AudioSystem.getAudioFileFormat(new File(o.toString()));
//        } catch (UnsupportedAudioFileException ex) {
//            Logger.getLogger(MP3PlayerGui.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(MP3PlayerGui.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        // определить длину песни и размер файла
        //map.get(o)
        
        //duration = (long) Math.round((((Long) map.get("duration")).longValue()) / 1000000);
        //bytesLen = (int) Math.round(((Integer) map.get("mp3.length.bytes")).intValue());

        // если есть mp3 тег для имени - берем его, если нет - вытаскиваем название из имени файла
//        String songName = map.get("title").toString()  ;//!= null ? map.get("title").toString() : FileUtils.getFileNameWithoutExtension(new File(o.toString()).getName());
//
//        // если длинное название - укоротить его
//        if (songName.length() > 30) {
//            songName = songName.substring(0, 30) + "...";
//        }
//        
//        System.out.println("songName = " + songName);
    }

    @Override
    public void progress(int i, long l, byte[] bytes, Map map) {
      
    }

    @Override
    public void stateUpdated(BasicPlayerEvent bpe) {
        
    }

    @Override
    public void setController(BasicController bc) {
        
    }
    
    
    public void jump(double controlPosition) {
        
        try {
            long skipBytes = (long) Math.round(( bytesLen) * controlPosition);
            basicPlayer.seek(skipBytes);
 
           // basicPlayer.setGain(currentVolume);// устанавливаем уровень звука
        } catch (BasicPlayerException ex) {
            Logger.getLogger(PlayerGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
           
   
    }
}
