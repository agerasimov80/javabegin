
package start;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;


public class Controller implements BasicController {
    
   
   BasicPlayer basicPlayer; 

    public Controller(BasicPlayer basicPlayer) {
        this.basicPlayer = basicPlayer;
    }

   
   
    @Override
    public void open(InputStream in) throws BasicPlayerException {
      basicPlayer.open(in);
    }

    @Override
    public void open(File file) throws BasicPlayerException {
       basicPlayer.open(file);
    }

    @Override
    public void open(URL url) throws BasicPlayerException {
        
    }

    @Override
    public long seek(long l) throws BasicPlayerException {
       
        return 0;
    }

    @Override
    public void play() throws BasicPlayerException {
       
    }

    @Override
    public void stop() throws BasicPlayerException {
        
    }

    @Override
    public void pause() throws BasicPlayerException {
        
    }

    @Override
    public void resume() throws BasicPlayerException {
        
    }

    @Override
    public void setPan(double d) throws BasicPlayerException {
        
    }

    @Override
    public void setGain(double d) throws BasicPlayerException {
        
    }
   
   
   
    
    
    
}
