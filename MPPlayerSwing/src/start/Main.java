package start;

import components.PlayerGUI;
import components.PlayList;
import components.PlayListJListImpl;
import javazoom.jlgui.basicplayer.BasicPlayer;

public class Main {
  
    private static PlayerGUI mainForm;
    
    private static PlayList playList;
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
             for (javax.swing.UIManager.LookAndFeelInfo iinfo : javax.swing.UIManager.getInstalledLookAndFeels()) {
                 System.out.println(iinfo.getName());
                }   
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PlayerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
      
        
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                
                // создаем PlayerGUI
                try {
                    
                    mainForm =new PlayerGUI();
                    BasicPlayer BasicPlayer = mainForm.getBasicPlayer();
                    playList = new PlayListJListImpl (300, 600, BasicPlayer);                       
                    playList.setPopUpMenu(mainForm.getjPopupMenu1()); 
                    mainForm.setPlayList(playList);
                    
                }
                catch (Exception e ){
                   if (e.getMessage().equals("PlayList is not a Component.")) 
                        System.out.println("PlayList is not a Component.");;
                        return;
                }
                
                // создаем PlayList

            }

           
        });
        
    }
    
    
}
