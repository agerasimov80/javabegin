package start;

import controllers.MainController;
import interfaces.PhoneBook;
import interfaces.impls.CollectionPhoneBook;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import objects.Lang;
import utils.LocaleManager;

import java.io.IOException;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

public class Main extends Application implements Observer {


    public static final String RESOURCES_FXML_MAIN = "../fxml/mainForm.fxml";
    public static final String RESOURCES_BUNDLES_LOCALE = "bundles.Locale";
    public static final String STYLES = "styles/my.css";
    private static final String locale = "en";
    private Parent fxmlMain;
    Stage primaryStage;
    MainController mainController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        createGUI(LocaleManager.RU_LOCALE);

    }

    private VBox loadFXML(Locale locale) {

        //create new FXMLLoader class
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource(RESOURCES_FXML_MAIN));
        fxmlLoader.setResources(ResourceBundle.getBundle(RESOURCES_BUNDLES_LOCALE, locale));

        VBox node = null;
        try {
            node = (VBox) fxmlLoader.load();

            mainController = fxmlLoader.getController();
            mainController.addObserver(this);
            primaryStage.setTitle(fxmlLoader.getResources().getString("name"));

        } catch (IOException e) {
            e.printStackTrace();
        }


        return node;
    }


    private void createGUI(Locale locale) {

        fxmlMain = loadFXML(locale);

        mainController.setMainStage(primaryStage);
        Scene scene = new Scene(fxmlMain, 480, 480);
        scene.getStylesheets().add(0, STYLES);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void update(Observable o, Object arg) {
        Lang lang = (Lang) arg;
        // создаем новое дерево компонентов с нужной локалью
        VBox newNode = loadFXML(lang.getLocale());
        // заменяем одно дерево на другое для родительского элемента fxmlMain типа VBox
        ((VBox) fxmlMain).getChildren().setAll(newNode.getChildren());

    }
}
