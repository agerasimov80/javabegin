package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import objects.Person;
import utils.DialogManager;

import java.net.URL;
import java.security.PrivateKey;
import java.util.ResourceBundle;


/**
 * Created by Alex on 10.05.2015.
 */
public class EditController implements Initializable{

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnDel;
    @FXML
    private TextField txtFio;
    @FXML
    private TextField txtPhone;
    @FXML
    private TextField txtAddress;

    @FXML
    private Label lblName;
    @FXML
    private Label lblPhone;
    @FXML
    private Label lblAddress;


    private ResourceBundle resources;

    private Person person;


    public void setPerson(Person person) {
        this.person = person;
        this.txtFio.setText(person.getFio());
        this.txtPhone.setText(person.getPhone());
    }

    public Person getPerson() {
        return person;
    }


    public void actionClose(ActionEvent actionEvent) {

        Node source = (Node) actionEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public void actionSave (ActionEvent actionEvent) {

        if (!checkValues())
            return;

        this.person.setFio(this.txtFio.getText());
        this.person.setPhone(this.txtPhone.getText());

        actionClose (actionEvent);
    }


    private boolean checkValues () {
        if (this.txtFio.getText().trim().length()==0 || this.txtPhone.getText().trim().length() ==0)
        {
            DialogManager.showErrorDialog(resources.getString("error"),resources.getString("fill_fields"));
            return false;
        }
        return true;
    }


    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;
        lblAddress.setText(resources.getString("address"));
        lblName.setText(resources.getString("fio"));
        lblPhone.setText(resources.getString("telephone"));
        btnCancel.setText(resources.getString("cancel"));
        btnDel.setText(resources.getString("delete"));
        btnSave.setText(resources.getString("save"));

    }


}
