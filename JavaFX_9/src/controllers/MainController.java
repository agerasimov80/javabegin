package controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import interfaces.impls.CollectionPhoneBook;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import objects.Lang;
import objects.Person;
import org.controlsfx.control.textfield.CustomTextField;
import org.controlsfx.control.textfield.TextFields;
import utils.DialogManager;
import utils.LocaleManager;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.ResourceBundle;

public class MainController extends Observable implements Initializable {


    private CollectionPhoneBook collectionPhoneBook = new CollectionPhoneBook();

    @FXML
    private CustomTextField txtSearch;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnEdit;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnDel;
    @FXML
    private TableView table;
    @FXML
    private TableColumn<Person, String> columnFio;
    @FXML
    private TableColumn<Person, String> columnPhone;
    @FXML
    private TableColumn<Person, String> columnAddress;
    @FXML
    private Label lblCount;
    @FXML
    public ComboBox comboLocales;

    private final static String RU_CODE = "ru";
    private final static String EN_CODE = "en";

    private ResourceBundle resourceBundle;
    private Parent fxmlEdit;
    private FXMLLoader fxmlLoader = new FXMLLoader();
    private EditController editController;
    private Stage editDialogStage;
    private Stage mainStage;
    private ObservableList<Person> personObservableListBackUp = FXCollections.observableArrayList();


    private void updateLblCount() {

        lblCount.setText(resourceBundle.getString("count") + ": " + collectionPhoneBook.getPersonList().size());
    }

    private void showDialog(Event actionEvent) {

        if (editDialogStage == null) {
            editDialogStage = new Stage();
            editDialogStage.setTitle(resourceBundle.getString("editForm"));
            editDialogStage.setResizable(false);
            editDialogStage.setScene(new Scene(fxmlEdit, 450, 250));
            editDialogStage.initModality(Modality.WINDOW_MODAL);
            editDialogStage.initOwner(((Node) (actionEvent.getSource())).getScene().getWindow());
        }

        editDialogStage.showAndWait();
    }


    public void onButtonPressed(ActionEvent actionEvent) {

        Object source = actionEvent.getSource();
        if (!(source instanceof Button))
            return;

        Person personSelected = (Person) table.getSelectionModel().getSelectedItem();
        Button buttonClicked = (Button) actionEvent.getSource();
        switch (buttonClicked.getId()) {

            case "btnAdd":

                editController.setPerson(new Person("", "", "", ""));
                showDialog(actionEvent);
                if (!editController.getPerson().getFio().equals(""))
                    collectionPhoneBook.addPerson(editController.getPerson());
                break;

            case "btnEdit":
                if (!personIsSelected(personSelected))
                    return;

                editController.setPerson(personSelected);
                showDialog(actionEvent);
                break;

            case "btnDel":
                if (!personIsSelected(personSelected))
                    return;

                collectionPhoneBook.delPerson((Person) table.getSelectionModel().getSelectedItem());
                break;

            case "btnSearch":
                collectionPhoneBook.getPersonList().clear();


                for (Person p : personObservableListBackUp)
                    if (p.getFio().toLowerCase().contains(txtSearch.getText().toLowerCase()) ||
                            p.getPhone().toLowerCase().contains(txtSearch.getText().toLowerCase()))
                        collectionPhoneBook.getPersonList().add(p);
                break;
        }
    }


    private boolean personIsSelected(Person personSelected) {
        if (personSelected == null) {
            DialogManager.showErrorDialog(resourceBundle.getString("error"), resourceBundle.getString("select_person"));
            return false;
        }

        return true;
    }


    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }


    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.resourceBundle = resources;

        columnFio.setText(resourceBundle.getString("fio"));
        columnPhone.setText(resourceBundle.getString("telephone"));
        columnAddress.setText(resourceBundle.getString("address"));
        btnAdd.setText(resourceBundle.getString("add"));
        btnDel.setText(resourceBundle.getString("delete"));
        btnEdit.setText(resourceBundle.getString("edit"));
        btnSearch.setText(resourceBundle.getString("search"));
        txtSearch.setText("fill-in data");

        columnFio.setCellValueFactory(new PropertyValueFactory<Person, String>("fio"));
        columnPhone.setCellValueFactory(new PropertyValueFactory<Person, String>("phone"));
        columnAddress.setCellValueFactory(new PropertyValueFactory<Person, String>("address"));

        initializeListeners();
        setupClearButtonField(txtSearch);
        fillTable();
        fillLangComboBox();

        loadFXML();
        editController = fxmlLoader.getController();

    }

    private void fillLangComboBox() {
        Lang langRU = new Lang(RU_CODE, resourceBundle.getString("ru"), LocaleManager.RU_LOCALE, 0);
        Lang langEN = new Lang(EN_CODE, resourceBundle.getString("en"), LocaleManager.EN_LOCALE, 1);

        comboLocales.getItems().add(langRU);
        comboLocales.getItems().add(langEN);

        if (LocaleManager.getCurrentLang() == null){
            comboLocales.getSelectionModel().select(0);
        }
        else {
            comboLocales.getSelectionModel().select(LocaleManager.getCurrentLang().getIndex());
        }
    }


    private void fillTable() {
        collectionPhoneBook.fillTestData();
        personObservableListBackUp.addAll(collectionPhoneBook.getPersonList());
        table.setItems(collectionPhoneBook.getPersonList());
    }


    private void setupClearButtonField(CustomTextField customTextField) {
        try {
            Method m = TextFields.class.getDeclaredMethod("setupClearButtonField", TextField.class, ObjectProperty.class);
            m.setAccessible(true);
            try {
                m.invoke(null, customTextField, customTextField.rightProperty());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }


    }



    private void loadFXML() {
        fxmlLoader.setLocation(getClass().getResource("../fxml/editForm.fxml"));
        fxmlLoader.setResources(resourceBundle);
        try {
            fxmlEdit = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializeListeners() {

        collectionPhoneBook.getPersonList().addListener(new ListChangeListener<Person>() {
            @Override
            public void onChanged(Change<? extends Person> c) {
                updateLblCount();
            }
        });


        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // Object source = event.getSource();

                if (event.getButton() == (MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 1) {
                        btnEdit.setDisable(false);
                        btnDel.setDisable(false);
                    }

                    if (event.getClickCount() == 2) {

                        Person personSelected = (Person) table.getSelectionModel().getSelectedItem();
                        editController.setPerson(personSelected);
                        showDialog(event);
                    }

                }
            }
        });

        //  слушает изменение языка
        comboLocales.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Lang selectedLang = (Lang) comboLocales.getSelectionModel().getSelectedItem();
                LocaleManager.setCurrentLang(selectedLang);

                // уведомить всех слушателей об изменении
                setChanged();
                notifyObservers (selectedLang);
            }
        });


    }

}

