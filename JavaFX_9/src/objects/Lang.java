package objects;

import java.util.Locale;

/**
 * Created by gerasal3 on 20.06.2015.
 */
public class Lang {

    private int index;
    private String name;
    private String code;
    private Locale locale;


    public Lang(String code, String name, Locale locale, int index) {
        this.setCode(code);
        this.setName(name);
        this.setLocale(locale);
        this.setIndex(index);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return name;
    }
}
