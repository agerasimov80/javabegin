package objects;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Alex on 10.05.2015.
 */
public class Person {

    private SimpleStringProperty fio = new SimpleStringProperty("");
    private SimpleStringProperty phone= new SimpleStringProperty("");
    private SimpleStringProperty age= new SimpleStringProperty("");
    private SimpleStringProperty address= new SimpleStringProperty("");

    public SimpleStringProperty fioProperty() {
        return fio;
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public SimpleStringProperty ageProperty() {
        return age;
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public Person(String fio, String phone, String age, String address) {
        this.fio = new SimpleStringProperty(fio);
        this.phone = new SimpleStringProperty(phone);
        this.age = new SimpleStringProperty(age);
        this.address = new SimpleStringProperty(address);
    }

    public String getFio() {
        return fio.get();
    }

    public void setFio(String fio) {
        this.fio.set(fio);
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getAge() {
        return age.get();
    }

    public void setAge(String age) {
        this.age.set(age);
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    @Override
    public String toString() {
        return this.getFio() + " (" + this.age + ") phone: " + this.getPhone() + " address: " + this.getAddress();
    }


}
