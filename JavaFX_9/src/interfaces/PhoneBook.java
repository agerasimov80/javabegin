package interfaces;

import objects.Person;

/**
 * Created by Alex on 10.05.2015.
 */
public interface PhoneBook {

    void addPerson(Person person);

    void delPerson(Person person);

    void editPerson(Person person);


}
