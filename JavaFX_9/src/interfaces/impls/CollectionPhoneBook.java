package interfaces.impls;

import interfaces.PhoneBook;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import objects.Person;

import java.util.Random;

/**
 * Created by Alex on 10.05.2015.
 */
public class CollectionPhoneBook implements PhoneBook {

    ObservableList<Person> personList = FXCollections.observableArrayList();

    public ObservableList<Person> getPersonList() {
        return personList;
    }


    @Override
    public void addPerson(Person person) {
        personList.add(person);
    }

    @Override
    public void delPerson(Person person) {
        personList.remove(person);
    }

    @Override
    public void editPerson(Person person) {
        System.out.println("This need to be implemented");
    }


    public void print () {

        int counter =0;
        for (Person person : personList)
        {
            System.out.print(counter++);
            System.out.println(" " + person);
        }


    }

    public void fillTestData () {

        personList.add(new Person ("Vasiliy Ivanov", "7911123456" , "20" , "Moscow, Solyanka 13" ));
        personList.add(new Person ("Ivan Sidorov", "79234567" , "25" , "Moscow, Solyanka 14" ));
        personList.add(new Person ("Ivan Verevkin", "79634423167" , "30" , "Moscow, Solyanka 15" ));
        personList.add(new Person ("Petr Verevkin", "79634423347" , "32" , "Moscow, Solyanka 15" ));
        personList.add(new Person ("Alexander Morozov", "792356767857" , "56" , "Moscow, Solyanka 17" ));
        personList.add(new Person ("Alexey Suvorov", "7923121267857" , "40" , "Moscow, Solyanka 18" ));



/*
        // creating 20 Persons
        for (int i = 0; i < 20; i++) {

            String name = "";
            for (int j = 0; j < 10; j++)
                name += (char) (65 + new Random().nextInt(90 - 65));

            String address = "";
            for (int j = 0; j < 10; j++)
                address += (char) (65 + new Random().nextInt(90 - 65));

            address += " ";

            for (int j = 0; j < 2; j++)
                address += (char) (48 + new Random().nextInt(57 - 48));

            String phone= "+";
            for (int j = 0; j < 10; j++)
                phone += (char) (48 + new Random().nextInt(57 - 48));


            String age = String.valueOf(new Random().nextInt(100));


            Person person = new Person(name, phone, age, address);

            personList.add(person);*/

    }
}
