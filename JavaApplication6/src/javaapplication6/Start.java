package javaapplication6;


public class Start {

    public static void main(String[] args) {
            Publisher p = new Publisher();
            p.addListener(new Subscriber());
            
            p.notifySubscribers("s");
        
    }
    
}
