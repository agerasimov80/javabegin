
package javaapplication6;

import java.util.ArrayList;

public class Publisher {
    
    ArrayList<Listener> listeners = new ArrayList<>();
    
    public void addListener (Listener listener){     
        listeners.add(listener);
    };
    
    public void notifySubscribers (String message){
        for (Listener listener : listeners )
            listener.eventHandler(message);
    }
  
}
