
package javaapplication6;

public class Subscriber implements Listener {

    @Override
    public void eventHandler(String string) {
        System.out.println("From subscriber string = " + string);
    }
    
}
