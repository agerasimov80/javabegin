package company.stores;

import company.Company;
import company.Category;
import person.Owner;
import person.AbstractPerson;
import person.Staff.AbstractEmployee;
import room.Room;

import java.util.ArrayList;
import java.util.List;
//import company.stores.Type;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class Store extends Company {


    private Type type;
    private List<Room> roomList;


    //  the only constructor is avaliable as there should be no possibility to create a company without obligatory parameters
    public Store(String companyName, Owner owner, AbstractEmployee contactPerson, Type type) {
        super(companyName, owner, contactPerson, Category.Store);
        this.type=type;

        roomList= new ArrayList<Room>();
    }

    public void addRoom (Room room) {
        roomList.add(room);
    }

    public Type getType() {
        return type;
    }
}
