package company.stores;

import person.Owner;
import person.AbstractPerson;
import person.Staff.AbstractEmployee;

/**
 * Created by Alex on 19.04.2015.
 */
public class BookStore extends Store {

    public BookStore(String companyName, Owner owner, AbstractEmployee contactPerson) {
        super(companyName, owner, contactPerson, Type.BookStore);
    }

}
