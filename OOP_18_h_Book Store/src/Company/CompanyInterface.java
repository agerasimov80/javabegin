package company;

import person.AbstractPerson;
import person.Owner;
import person.Staff.AbstractEmployee;

/**
 * Created by Alex on 22.04.2015.
 */
public interface CompanyInterface {

    public String getCompanyName ();
    public Owner getOwner ();
    public Category getCategory ();


    public void setContactPerson (AbstractEmployee contactPerson);
    public AbstractEmployee getContactPerson ();

}
