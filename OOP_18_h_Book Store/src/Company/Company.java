package company;

import person.AbstractPerson;
import person.Owner;
import person.Staff.AbstractEmployee;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class Company implements CompanyInterface {

    private String companyName;
    private Owner owner;
    private AbstractEmployee contactPerson;
    private Category category;

    private List<AbstractEmployee> employeeList;


    public Company(String companyName, Owner owner, AbstractEmployee contactPerson,
                   Category category) {

        this.companyName = companyName;
        this.owner = owner;
        this.setContactPerson(contactPerson);
        this.category = category;

        employeeList = new ArrayList<AbstractEmployee>();
    }

    @Override
    public Owner getOwner() {
        return owner;
    }

    @Override
    public String getCompanyName() {
        return companyName;
    }

    public AbstractEmployee getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(AbstractEmployee contactPerson) {
        this.contactPerson = contactPerson;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    public void changeOwner(Owner owner) {
        this.owner = owner;

    }
}
