package company.vendors;

import company.Company;
import person.Owner;
import person.AbstractPerson;
import company.Category;
import person.Staff.AbstractEmployee;

/**
 * Created by Alex on 19.04.2015.
 */
public class Vendor extends Company {

    public Vendor(String companyName, Owner owner, AbstractEmployee contactPerson) {
        super(companyName, owner, contactPerson, Category.Vendor);
    }

    public void deliverProducts () {

    };
}
