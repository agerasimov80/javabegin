package room;

/**
 * Created by Alex on 22.04.2015.
 */
public enum RoomType {
    SienceFiction,
    Thriller,
    Bestseller,
    Programming,
    Drama
}
