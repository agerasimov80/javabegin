package room;

import person.AbstractPerson;
import person.Customers.AbstractCustomer;
import person.Staff.Assistant;
import products.goods.ProductAbstract;

import java.beans.PersistenceDelegate;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 21.04.2015.
 */
public class Room {

    private ArrayList<AbstractPerson> assistantList;
    private ArrayList<ProductAbstract> productList;
    private ArrayList<AbstractCustomer> customerList;

    RoomType roomType;

    public Room(RoomType roomType) {
        this.roomType = roomType;
        customerList = new ArrayList<AbstractCustomer>();
        assistantList = new ArrayList<AbstractPerson>();
        productList = new ArrayList<ProductAbstract>();
    }

    public void addAssistents(AbstractPerson assistent) {
        this.assistantList.add(assistent);

        if ( assistent instanceof Assistant)
            ((Assistant) assistent).setRoom(this);
    }

    public void addProducts(ProductAbstract product) {
        this.productList.add(product);
    }

    public void addCustomer(AbstractCustomer customer) {
        this.customerList.add(customer);
    }

}
