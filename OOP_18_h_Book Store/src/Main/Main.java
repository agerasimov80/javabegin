package main;

import company.Company;
import company.stores.BookStore;
import company.stores.Store;
import person.AbstractPerson;
import person.Customers.AbstractCustomer;
import person.Customers.CustomerGroup;
import person.Customers.CustomerVIP;
import person.Owner;
import person.Sex;
import person.Staff.AbstractEmployee;
import person.Staff.Assistant;
import person.Staff.Cashier;
import person.Staff.Manager;
import products.goods.books.Book;
import products.goods.ProductAbstract;
import room.Room;
import room.RoomType;

/**
 * Created by Alex on 20.04.2015.
 */
public class Main {

    public static void main(String[] args) {


        AbstractPerson owner = new Owner(50, Sex.male, "Alexander", "Gerasimov");
        AbstractEmployee manager = new Manager(40, Sex.male, "Alexey", "Smirnov", 1000, 50_000);

        Company bookStore = new BookStore("Книжный магазин", (Owner) owner, manager);

//        Employees
        AbstractEmployee assistant1 = new Assistant(25, Sex.male, "Alexander", "Rybin", 2000, 25_000);
        AbstractEmployee assistant2 = new Assistant(26, Sex.male, "Dmitry", "Veselov", 2000, 25_000);
        AbstractEmployee cashier = new Cashier(26, Sex.male, "Dmitry", "Veselov", 2000, 25_000);


//        Products
        ProductAbstract book1 = new Book(500, 11_001, "War and Peace", "L.Tolstoy", "Publisher1", 10_000);

//        Department "ScienceFiction"
//        Adding products & staff
        Room scienceFictionRoom = new Room(RoomType.SienceFiction);
        ((Store) bookStore).addRoom(scienceFictionRoom);

        scienceFictionRoom.addAssistents(assistant1);
        scienceFictionRoom.addAssistents(assistant2);

        ((Manager)manager).assignToRoom(assistant1,scienceFictionRoom);
        ((Manager)manager).assignToRoom(assistant2,scienceFictionRoom);

        scienceFictionRoom.addProducts(book1);


    }
}
