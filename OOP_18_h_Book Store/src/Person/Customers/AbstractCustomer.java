package person.Customers;

import person.AbstractPerson;
import person.Complainer;
import person.Sex;
import person.Staff.Assistant;
import products.goods.ProductAbstract;

import java.util.ArrayList;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class AbstractCustomer extends AbstractPerson implements Complainer {

    ArrayList<ProductAbstract> basket;
    ArrayList<ProductAbstract> wishList;
    CustomerGroup customerGroup;

    public AbstractCustomer(int age, Sex sex, String firstName, String lastName, CustomerGroup customerGroup) {
        super(age, sex, firstName, lastName);
        //this.discount = discount;
        this.customerGroup = customerGroup;
    }

    public void complain() {
        System.out.println("Customer complaining to manager");
    }

    public void requestProduct(ArrayList<ProductAbstract> wishList, Assistant assistant) {
        basket = assistant.serveCustomer(wishList);
    }

    public void returnProduct(ProductAbstract product, int quantity) {
        product.increaseStock(quantity);
    }

    public void buy(ProductAbstract product, int quantity) {
        product.decreaseStock(quantity);
        System.out.println("Customer is buying product " + product.getName());
    }
}
