package person.Customers;

import person.Sex;

/**
 * Created by Alex on 19.04.2015.
 */
public class CustomerRegular extends AbstractCustomer {

    private int discount;

    public CustomerRegular(int age, Sex sex, String firstName, String lastName, CustomerGroup customerGroup, int discount) {
        super(age, sex, firstName, lastName, customerGroup);
        this.discount = discount;
    }


}
