package person.Customers;

import person.Sex;
import products.goods.ProductAbstract;

/**
 * Created by Alex on 19.04.2015.
 */
public class CustomerVIP extends AbstractCustomer {
    private int discount;

    public CustomerVIP(int age, Sex sex, String firstName, String lastName, CustomerGroup customerGroup, int discount) {
        super(age, sex, firstName, lastName, customerGroup);
        this.discount = discount;
    }


    public void buyWithDiscount (ProductAbstract product) {

    }

}
