package person.Customers;

/**
 * Created by Alex on 19.04.2015.
 */
public enum CustomerGroup {
    age16_22,
    age23_27,
    age28_35,
    age36_45,
    age46_55,
    age56_65,
    age66_100
}
