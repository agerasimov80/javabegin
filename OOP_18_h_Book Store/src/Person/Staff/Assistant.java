package person.Staff;

import person.Sex;
import products.goods.ProductAbstract;
import room.Room;

import java.util.ArrayList;


/**
 * Created by Alex on 19.04.2015.
 */
public class Assistant extends AbstractEmployee {


    private Room room;
    private Manager manager;
    ArrayList<ProductAbstract> avaliableList;

    public Assistant(int age, Sex sex, String firstName, String lastName, int personalId, int salary) {
        super(age, sex, firstName, lastName, personalId,  salary,Position.Assistant);
    }


    public ArrayList<ProductAbstract> serveCustomer(ArrayList<ProductAbstract> wishList) {
        avaliableList = new ArrayList<ProductAbstract>();
        this.checkStock(wishList);
        return avaliableList;
    }

    ;

    private void checkStock(ArrayList<ProductAbstract> list) {

        avaliableList.add(list.get(1));
    }

    ;

    @Override
    public void complain() {
        // complaining to manager
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

}
