package person.Staff;

import person.AbstractPerson;
import person.Sex;
import room.Room;

/**
 * Created by Alex on 19.04.2015.
 */
public class Manager extends AbstractEmployee {

    public Manager(int age, Sex sex, String firstName, String lastName, int personalId, int salary) {
        super(age, sex, firstName, lastName, personalId, salary, Position.Manager);
    }

    public void assignToRoom (AbstractEmployee employee, Room room)
    {
        room.addAssistents(employee);
    }

    void hire(AbstractPerson person, Position position) {

//        switch(position) {
//            case Manager:
//                return new Manager(person.getAge(),person.getSex(),person.getFirstName(),person.getLastName(), 1000, 25_000);
//            case Assistant:
//                return new Assistant(person.getAge(),person.getSex(),person.getFirstName(),person.getLastName(), 1000, 25_000);
//            case Cashier:
//                return new Cashier(person.getAge(),person.getSex(),person.getFirstName(),person.getLastName(), 1000, 25_000);
//            default:
//                return null;
//        }


    }

    public void fire(AbstractEmployee employee) {

    }

    public void proceedComplain() {

    }

}
