package person.Staff;

import person.AbstractPerson;
import person.Complainer;
import person.Sex;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class AbstractEmployee extends AbstractPerson implements Complainer {


    private int personalId;
    private int salary;
    private Position position;
    private boolean isAvaliable;


    public AbstractEmployee(int age, Sex sex, String firstName,
                            String lastName, int personalId,
                            int salary, Position position) {
        super(age, sex, firstName, lastName);
        this.personalId = personalId;
        this.position = position;
        this.salary = salary;
    }

    @Override
    public void complain() {

        System.out.print("Complaining to manager");

    }
}
