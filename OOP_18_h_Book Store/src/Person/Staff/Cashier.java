package person.Staff;

import person.Sex;

/**
 * Created by Alex on 19.04.2015.
 */
public class Cashier extends AbstractEmployee {

    private static final String position = "cashier";

    public Cashier(int age, Sex sex, String firstName, String lastName, int personalId, int salary) {
        super(age, sex, firstName, lastName, personalId,  salary, Position.Cashier);
    }

    public void proceedPayment() {

    }

    ;
}
