package person;

/**
 * Created by Alex on 22.04.2015.
 */
public class Attendee extends AbstractPerson {

    public Attendee(int age, Sex sex, String firstName, String lastName) {
        super(age, sex, firstName, lastName);
    }
}
