package person;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class AbstractPerson {

    private int age;
    private Sex sex;
    private String firstName;
    private String lastName;

    public AbstractPerson(int age, Sex sex, String firstName, String lastName) {
        this.age = age;
        this.sex = sex;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public Sex getSex() {
        return sex;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

}
