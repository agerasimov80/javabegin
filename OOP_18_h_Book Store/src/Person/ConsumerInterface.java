package person;

import products.ConsumableInterface;

/**
 * Created by Alex on 22.04.2015.
 */
public interface ConsumerInterface {
    void consume (ConsumableInterface product, int amount);
}
