package products;

/**
 * Created by Alex on 22.04.2015.
 */
public interface DeliverableInterface {
    void deliver (int quantity);
}
