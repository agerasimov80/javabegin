package products;

/**
 * Created by Alex on 22.04.2015.
 */
public interface StockableInterface {
    void increaseStock (int stock);
    void decreaseStock (int stock);
    int getStock ();
    void setStock(int stock);

}
