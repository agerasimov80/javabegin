package products.services;

import products.ConsumableInterface;
import products.DeliverableInterface;
import products.ValuableInterface;

/**
 * Created by Alex on 22.04.2015.
 */
public interface ServiceInterface extends DeliverableInterface, ValuableInterface, ConsumableInterface {



}
