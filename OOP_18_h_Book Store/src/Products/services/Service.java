package products.services;

import products.DeliverableInterface;
import products.ValuableInterface;

/**
 * Created by Alex on 22.04.2015.
 */
public abstract class Service implements DeliverableInterface, ValuableInterface {

    private double price;
    private String name;
    private String description;


    public Service(double price, String name, String description) {
        this.price = price;
        this.name = name;
        this.description = description;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void deliver(int quantity) {
        System.out.println("Not yet implemented");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

}
