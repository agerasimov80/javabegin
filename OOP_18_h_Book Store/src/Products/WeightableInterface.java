package products;

/**
 * Created by Alex on 22.04.2015.
 */
public interface WeightableInterface extends DeliverableInterface {

    double getWeight ();
    void setWeight (double weight);
}
