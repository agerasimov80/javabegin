package products.goods.accessories;

import products.goods.ProductAbstract;

/**
 * Created by Alex on 19.04.2015.
 */
public class Accessories extends ProductAbstract {

    public Accessories(double price, int serialNumber, String name, String type) {
        super(price,  serialNumber, name);
        this.type = type;
    }

    private String type;

}
