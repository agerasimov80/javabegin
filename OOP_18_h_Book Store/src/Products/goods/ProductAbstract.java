package products.goods;

import products.*;

/**
 * Created by Alex on 19.04.2015.
 */
public abstract class ProductAbstract implements DeliverableInterface,
        ValuableInterface, ReturnableInterface, StockableInterface {

    private double price;
    //    private double weight;
    private int serialNumber;
    private String name;
    private int stock;

    public ProductAbstract(double price, int serialNumber, String name) {
        this.price = price;
        //this.weight = weight;
        this.serialNumber = serialNumber;
        this.name = name;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }


    public String getName() {
        return name;
    }

    @Override
    public int getStock() {
        return stock;
    }

    @Override
    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public void increaseStock(int amount) {
        this.stock += amount;
    }

    @Override
    public void decreaseStock(int amount) {
        this.stock -= amount;
    }

    @Override
    public void deliver(int quantity) {
        this.decreaseStock(quantity);
    }

    @Override
    public void returnBack(int quantity) {
        this.increaseStock(quantity);
    }
}
