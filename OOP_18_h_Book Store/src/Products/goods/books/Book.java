package products.goods.books;

import products.goods.ProductAbstract;

/**
 * Created by Alex on 19.04.2015.
 */
public class Book extends ProductAbstract implements BookInterface{

    private String author;
    private String publisher;
    private int pages;

    public Book(double price,  int serialNumber, String name, String author, String publisher, int pages) {
        super(price, serialNumber, name);
        this.author = author;
        this.publisher = publisher;
        this.pages = pages;
    }


    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getPages() {
        return pages;
    }


}
