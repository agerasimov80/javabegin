package products.goods.books;

/**
 * Created by Alex on 24.04.2015.
 */
public interface BookInterface {

    String getAuthor ();
    String getPublisher ();
    int getPages ();
}
