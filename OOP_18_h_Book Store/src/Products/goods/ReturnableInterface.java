package products.goods;

/**
 * Created by Alex on 22.04.2015.
 */
public interface ReturnableInterface {
    void returnBack(int quantity);
}
