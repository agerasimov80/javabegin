package products;

/**
 * Created by Alex on 22.04.2015.
 */
public interface ValuableInterface {
    double getPrice ();
    void setPrice(double price);
}
